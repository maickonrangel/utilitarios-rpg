<?php

class Mensagens_Helper {

	function __construct(){
		$this->messages = $this->set_messages();
	}

	public function set_messages(){
		$_MESSAGES = [
			'saiu' 						=> 'Você foi deslogado com sucesso!',
			'login-limite-estourado'	=> 'Sua conta permite um número limitado de acesso e este limite foi atingido! Caso já possua uma conta ativa marque a caixa de seleção para forçar o login.',
			'usuario-inativo'			=> 'Sua conta precisa ser ativada ainda.',
			'usuario-errado'			=> 'Login ou senha estão incorretos!',
			'conta-criada'				=> 'Sua conta foi criada com sucesso!',
			'conta-nao-criada' 			=> 'Um erro ocorreu e sua conta não pode ser criada. Entre em contato com a dministrador pelo email <b>helprpg.br@gmail.com</b>.',
			'usuario-ja-existe'			=> 'Este usuário já existe no sistema.',
			'pagina-restrita'			=> 'Você não tem acesso a esta página.',
			'dados-atualizados'			=> 'Os dados foram atualizados com sucesso.',
			'dados-nao-atualizados'		=> 'Os dados não foram atualizados com sucesso.',
			'id-invalido'				=> 'Código de usuário inválido.',
			'contador-invalido'			=> 'Sua conta só permite um único acesso.',
			'resetado'					=> 'Tente fazer login novamente.'
		];

		return $_MESSAGES;
	}	
}