<?php

class Alert_Helper {

	public function update_msg($status_var, $opcao1, $opcao2){
		global $_QUERY;
		if (isset($_QUERY[$status_var]) && $_QUERY[$status_var] == $opcao1) {
			echo '<div style="background-color:green;color:#fff; padding:10px;margin:0px;margin-bottom:5px;">Dados atualizado com sucesso!</div>';
		} elseif (isset($_QUERY[$status_var]) && $_QUERY[$status_var] == $opcao2) {
			echo '<div style="background-color:red;color:#fff; padding:10px;margin:0px;margin-bottom:5px;">Dados não foram atualizado!</div>';
		}
	}

	public function create_msg($status_var, $opcao1, $opcao2){
		global $_QUERY;
		if (isset($_QUERY[$status_var]) && $_QUERY[$status_var] == $opcao1) {
			echo '<div style="background-color:green;color:#fff; padding:10px;margin:0px;margin-bottom:5px;">Dados salvos com sucesso!</div>';
		} elseif (isset($_QUERY[$status_var]) && $_QUERY[$status_var] == $opcao2) {
			echo '<div style="background-color:red;color:#fff; padding:10px;margin:0px;margin-bottom:5px;">Dados não foram salvos!</div>';
		}
	}

	public function delete_msg($status_var, $opcao1, $opcao2){
		global $_QUERY;
		if (isset($_QUERY[$status_var]) && $_QUERY[$status_var] == $opcao1) {
			echo '<div style="background-color:green;color:#fff; padding:10px;margin:0px;margin-bottom:5px;">Dados foram deletado com sucesso!</div>';
		} elseif (isset($_QUERY[$status_var]) && $_QUERY[$status_var] == $opcao2) {
			echo '<div style="background-color:red;color:#fff; padding:10px;margin:0px;margin-bottom:5px;">Dados não foram deletados!</div>';
		}
	}
}