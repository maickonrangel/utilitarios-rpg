<?php

class Arquivos_Helper {

	function __construct(){
		$this->tipos_form = $this->label_tipos();
		$this->tipos_midia = $this->label_documentos();
	}

	public function label_tipos(){
		$_TIPOS = [
			'aventuras'		=> 'Aventuras',
			'personagens'	=> 'Personagens',
			'artigos'		=> 'Artigos',
			'canais'		=> 'Canais Youtube',
			'contos'		=> 'Contos',
			'fanpages'		=> 'Fanpages',
			'pastas'		=> 'Pasta de arquivos',
			'historias'		=> 'Histórias',
			'imagens'		=> 'Imagens Descritivas',
			'itens'			=> 'Itens Gerais',
			'livros'		=> 'Livros & Suplementos',
			'musicas'		=> 'Músicas Ambiente',
			'programas'		=> 'Programas',
			'revistas'		=> 'Revistas',
			'sites'			=> 'Sites',
			'videos'		=> 'Vídeos',
		];

		return $_TIPOS;
	}

	public function label_documentos(){
		$_TIPOS = [
			'apk'		=> 'APLICATIVO',
			'audio'		=> 'AUDIO',
			'dos'		=> 'DOS',
			'doc'		=> 'DOC',
			'dir'		=> 'PASTAS',
			'exe'		=> 'EXE',
			'excel'		=> 'EXCEL',
			'flash'		=> 'FLASH',
			'html'		=> 'HTML',
			'img'		=> 'IMAGEM',
			'pdf'		=> 'PDF',
			'txt'		=> 'TXT',
			'video'		=> 'VÍDEO',
			'word'		=> 'WORD',
			'zip'		=> 'ZIP',
		];

		return $_TIPOS;
	}

	public function rschar($string){
	    $string = trim(html_entity_decode($string));
		//tirando os acentos
		$string= preg_replace('![áàãâä]+!u','a',$string);
		$string= preg_replace('![éèêë]+!u','e',$string);
		$string= preg_replace('![íìîï]+!u','i',$string);
		$string= preg_replace('![óòõôö]+!u','o',$string);
		$string= preg_replace('![úùûü]+!u','u',$string);
		//parte que tira o cedilha e o ñ
		$string= preg_replace('![ç]+!u','c',$string);
		$string= preg_replace('![ñ]+!u','n',$string);
		//tirando outros caracteres invalidos
		$string= preg_replace('[^a-z0-9\-]','-',$string);
		//trocando duplo espaço (hifen) por 1 hifen só
		$string = str_replace('--','-',$string);

		return $string;
	}

	public function contador($pagina, $categoria, $contador, $pesquisa = null){
		global $_QUERY;
		if ($contador > 0) {
			$pag_ante = $_QUERY['pagina']-1;
			$pag_prox = $_QUERY['pagina']+1;
			if ($pag_ante <= 0) {
				$pag_ante = 1;
			}

			if ($pag_prox > $contador) {
				$pag_prox = $contador;
			}

			if ($pesquisa != null) {
				$url_ante = URL_BASE . "aventurateca/{$pagina}/{$categoria}/{$pesquisa}?pagina={$pag_ante}"; 
				$url_prox = URL_BASE . "aventurateca/{$pagina}/{$categoria}/{$pesquisa}?pagina={$pag_prox}"; 
				$url_atual = URL_BASE . "aventurateca/{$pagina}/{$categoria}/{$pesquisa}?pagina=";
			} else {
				$url_ante = URL_BASE . "aventurateca/{$pagina}/{$categoria}?pagina={$pag_ante}"; 
				$url_prox = URL_BASE . "aventurateca/{$pagina}/{$categoria}?pagina={$pag_prox}"; 
				$url_atual = URL_BASE . "aventurateca/{$pagina}/{$categoria}?pagina="; 
			}
			$final = $_QUERY['pagina']+5;

			echo '<div class="contador" id="contador">';
			echo '<div class="indices">';
			echo '<a href="'.$url_ante.'#prev" id="prec"><div class="indice">&#10229;</div></a>';
			for ($i=$_QUERY['pagina']; $i <= $contador; $i++) {
				echo '<a href="'.$url_atual . $i.'#index-link"><div class="indice">'.$i.'</div></a>';
				if ($i == $final):
					break;
				endif;
			}
			echo '<a href="'.$url_prox.'#next" id="next"><div class="indice">&#10230;</div></a>';
			echo '</div>';
			echo '</div>';
		}
	}

	function url_exists($url) {
	    $ch = curl_init($url);
	    curl_setopt($ch, CURLOPT_NOBODY, true);
	    curl_exec($ch);
	    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	    curl_close($ch);

	    return ($code == 200);
	}

	public function select_classes(){
		return explode("\n",file_get_contents(PATH_BASE.'config/locale/pt-br/classes.txt'));
	}
}