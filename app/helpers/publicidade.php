<?php

class Publicidade_Helper {

	function __construct(){
		$this->ads_fluid = '
		<div class="label-layout publicidade-layout">
		    <h2 class="title emfont">
		        <a class="label-name" href="#" title="Utilitários">Publicidade</a>
		        <div class="clear"></div>
		    </h2>
		    <div class="clear"></div>
		    <div class="content">
		     	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
				<ins class="adsbygoogle"
				     style="display:block"
				     data-ad-format="fluid"
				     data-ad-layout-key="-fe+67+3j-k5+oo"
				     data-ad-client="ca-pub-3010334569259161"
				     data-ad-slot="2718555897"></ins>
				<script>
				     (adsbygoogle = window.adsbygoogle || []).push({});
				</script>
		        <div class="clear"></div>
		    </div>
		</div>';

		$this->ads_responsive = '
			<h2 class="title">Publicidade</h2>
			<div class="item item-other">
				<div class="inner">
					<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
						<ins class="adsbygoogle"
						     style="display:block"
						     data-ad-client="ca-pub-3010334569259161"
						     data-ad-slot="7650926539"
						     data-ad-format="auto"></ins>
						<script>
						(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
					<!-- ads_help_responsivo_01 -->
				</div>
			</div>';

		$this->ads_pure_responsive = '
			<div class="box-publicidade" style="height:auto!important;">
			<div style="margin:0 auto; width: 90%;">
	     			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<!-- ads_help_responsivo_01 -->
					<ins class="adsbygoogle"
					     style="display:block"
					     data-ad-client="ca-pub-3010334569259161"
					     data-ad-slot="7650926539"
					     data-ad-format="auto"
					     data-full-width-responsive="true"></ins>
					<script>
					(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
	 			</div>
	 		</div>';

	 	$this->ads_preview_page = '
			<div class="publicidade">
				<div style="margin:0 auto; width: 90%;">
	     			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<!-- ads_help_responsivo_01 -->
					<ins class="adsbygoogle"
					     style="display:block"
					     data-ad-client="ca-pub-3010334569259161"
					     data-ad-slot="7650926539"
					     data-ad-format="auto"
					     data-full-width-responsive="true"></ins>
					<script>
					(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
     			</div>
			</div>';
	}

	public function ads_fluid($forced = false){
		if ($forced == true) {
			echo $this->ads_fluid;
		} else {
			if (!isset($_SESSION['login']) && !isset($_SESSION['usuarios_logados'])) {
				echo $this->ads_fluid;
			} else if ($_SESSION['plano'] == 7) {
				echo $this->ads_fluid;
			}
		}
	}

	public function ads_responsive($forced = false){
		if ($forced == true) {
			echo $this->ads_responsive;
		} else {
			if (!isset($_SESSION['login']) && !isset($_SESSION['usuarios_logados'])) {
				echo $this->ads_responsive;
			} else if ($_SESSION['plano'] == 7) {
				echo $this->ads_responsive;
			}	
		}
	}

	public function ads_pure_responsive($forced = false){
		if ($forced == true) {
			echo $this->ads_pure_responsive;
		} else {
			if (!isset($_SESSION['login']) && !isset($_SESSION['usuarios_logados'])) {
				echo $this->ads_pure_responsive;
			} else if ($_SESSION['plano'] == 7) {
				echo $this->ads_pure_responsive;
			}
		}
	}

	public function ads_preview_page($forced = false){
		if ($forced == true) {
			echo $this->ads_preview_page;
		} else {
			if (!isset($_SESSION['login']) && !isset($_SESSION['usuarios_logados'])) {
				echo $this->ads_preview_page;
			} else if ($_SESSION['plano'] == 7) {
				echo $this->ads_preview_page;
			}
		}
	}
}