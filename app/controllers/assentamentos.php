<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2017
	Controller: assentamentos
*/

class Assentamentos_Controller extends Controller_Core {
	function __construct(){
		parent::__construct();
		// setanto os meta dados
		$this->meta_title = 'Gerador de assentamentos';
		$this->meta_description = 'Ferramenta online que gera assentamentos para RPG de mesa.';
		$this->meta_keywords = 'RPG, assentamentos, D&D 5e, D&D, RPG de mesa, utilitários';

		// [Voce pode passar arquivos css para a pagina do seu controller apenas 
		// informando o array como parametro de $this->set_base_css()]

		// chamando css em assets/css
		$this->css_files = $this->set_base_css(['init']);
		
		// chamando css interno dentro da view e concatenando ao css_files
		// $this->css_files .= $this->set_css(['index','home']);
		
		// [Voce pode passar arquivos javascript para ser chamado na view deste  
		// controller apenas passando um array com os nomes dos arquivos sem a 
		// extençao no array em $this->set_base_js]

		// chamada de arquivos js dentro de assets
		$this->js_files = $this->set_base_js(['index']);
		// chamada de arquivos js dentro da veiw 
		// $this->js_files .= $this->set_js(['index','teste']);
	}

	public function index(){
		$url = new Init_Model;
		$tipos = ['Assentamento','Construcoes','Encontro aleatório urbano'];
		require_once $this->render('index');
	}
	
	public function sortear($type){
		$url = new Init_Model;
		$tipos = ['Assentamento','Construcoes','Encontro aleatório urbano'];
		if (!in_array($type, $tipos)) {
			$this->redirect('error');
		} else {
			$assentamento = new Assentamentos_Model;
			if ($type == 'Assentamento') {
				$assentamento_sorteio = [
					'Relações raciais' => $assentamento->relacoes_raciais(),
					'Posição do governante' => $assentamento->posicao_do_governante(),
					'Traços notáveis' => $assentamento->tracos_notaveis(),
					'Conhecido por...' => $assentamento->conhecido_por(),
					'Calamidade atual' => $assentamento->calamidade_atual()
				];
			} elseif ($type == 'Construcoes') {
				$nome_taverna = $assentamento->primeiro_nome_taverna().' '.$assentamento->segundo_nome_taverna();
				$assentamento_sorteio = [
					'Tipo de construção' => $assentamento->tipo_de_construcao(),
					'Tipo de residência' => $assentamento->residencia(),
					'Tipo de construção religiosa' => $assentamento->construcao_religiosa(),
					'Característica da Taverna' => $assentamento->taverna(),
					'Nome da taverna' => $nome_taverna,
					'Tipo de armazém' => $assentamento->armazem(),
					'Tipo de loja' => $assentamento->loja()
				];
			} elseif ($type == 'Encontro aleatório urbano') {
				$assentamento_sorteio = $assentamento->encontros(4);
				$assentamento_sorteio['Sugestão de encontro 1'] = $assentamento_sorteio[0];
				$assentamento_sorteio['Sugestão de encontro 2'] = $assentamento_sorteio[1];
				$assentamento_sorteio['Sugestão de encontro 3'] = $assentamento_sorteio[2];
				$assentamento_sorteio['Sugestão de encontro 4'] = $assentamento_sorteio[3];
				unset($assentamento_sorteio[0]);
				unset($assentamento_sorteio[1]);
				unset($assentamento_sorteio[2]);
				unset($assentamento_sorteio[3]);
			}
			
			require_once $this->render('sortear');
		}
	}
}