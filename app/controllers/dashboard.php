<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2018
	Controller: dashboard
*/

class Dashboard_Controller extends Controller_Core {
	function __construct(){
		$this->checar_sessao();
		parent::__construct();
		// setanto os meta dados
		$this->meta_title = 'Dashboard Utilitários RPG';
		$this->meta_description = 'Confira suas informações aqui.';
		$this->meta_keywords = 'Dashboard, paiel';

		// [Voce pode passar arquivos css para a pagina do seu controller apenas 
		// informando o array como parametro de $this->set_base_css()]

		// chamando css em assets/css
		$this->css_files = $this->set_base_css(['init','dashboard','jquery.dataTables.min']);
		
		// chamando css interno dentro da view e concatenando ao css_files
		// $this->css_files .= $this->set_css(['index','home']);
		
		// [Voce pode passar arquivos javascript para ser chamado na view deste  
		// controller apenas passando um array com os nomes dos arquivos sem a 
		// extençao no array em $this->set_base_js]

		// chamada de arquivos js dentro de assets
		$this->js_files = $this->set_base_js(['jquery.min','jquery.dataTables.min','dashboard','ckeditor/ckeditor']);
		// chamada de arquivos js dentro da veiw 
		// $this->js_files .= $this->set_js(['index','teste']);

		$usuario = new Usuarios_Model;
		$this->user = $usuario->find_filter('*',"email = '{$_SESSION['login']}'");
		$this->checar_contador();

		$this->display = (new Mensagens_Helper)->set_messages();
		$this->menu = [
			[
				'url' 	=> URL_BASE.'dashboard',
				'nome'	=> 'Home'
			],

			[
				'url' 	=> URL_BASE.'dashboard/msg',
				'nome'	=> 'Mensagens'
			],

			[
				'url' 	=> URL_BASE,
				'nome'	=> 'Utilitários'
			],

			[
				'url' 	=> URL_BASE.'utilitarios-especiais',
				'nome'	=> 'Utilitários Especiais'
			],

			[
				'url' 	=> URL_BASE.'home-cadastro',
				'nome'	=> 'Adicionar Arquivo'
			],

			[
				'url' 	=> 'http://www.helprpg.com.br/2017/08/como-se-joga-rpg.html',
				'nome'	=> 'O que é RPG?'
			],

			[
				'url' 	=> URL_BASE.'sair',
				'nome'	=> 'Sair'
			],
		];

		$this->planos = [
			1	=> 'Único Mensal      - R$ 2,00',
			2 	=> 'Único Semestral   - R$ 11,40',
			3 	=> 'Único Anual       - R$ 21,60',
			4 	=> 'Grupo Mensal      - R$ 10,00',
			5 	=> 'Grupo Semestral   - R$ 57,00',
			6 	=> 'Grupo Anual       - R$ 108,00',
			7 	=> 'Free - Usuários que cadastram'
		];
	}

	public function index(){
		global $_QUERY;
		$mensagem = new Mensagens_Model;
		$mensagens = $mensagem->find_all();
		require_once $this->render('index', false);
	}
	
	public function checar_sessao(){
		if (isset($_SESSION['login']) and isset($_SESSION['plano']) and isset($_SESSION['cadastrado'])) {
			$usuario = new Usuarios_Model;
			$user = $usuario->find_filter('*',"email = '{$_SESSION['login']}'");
			if (empty($user[0]->email) || $user[0]->status != 'ativo') {
				$this->redirect();
			}
		} else {
			$this->redirect();
		}
	}

	public function checar_contador(){
		if ($this->user[0]->contador == 0) {
			@session_start();
			session_destroy();
			$this->redirect('?status=contador-invalido');
		}
	}

	public function is_admin(){
		if ($this->user[0]->nivel != 21) {
			$this->redirect('dashboard');
		}
	}

	public function dashboard_admin(){
		$this->is_admin();
		$usuario = new Usuarios_Model;
		$model = new Dashboard_Model();
		$usuarios = $usuario->find_filter('*',"nivel = '1'");
		require_once $this->render('admin', false);
	}

	public function msg(){
		$usuario = new Usuarios_Model;
		$usuarios = $usuario->find_filter('*',"email = '{$_SESSION['login']}'");
		$mensagem = new Mensagens_Model;
		$mensagens = $mensagem->find_all();
		if ($usuarios[0]->nivel == 21 && $usuarios[0]->id == 1) {
			require_once $this->render('msg', false);
		} else {
			require_once $this->render('user-msg', false);
		}
	}

	public function visualizar_mensagem($id = ''){
		$mensagem = new Mensagens_Model;
		$mensagens = $mensagem->find($id);
		if(isset($mensagens[0]->id)) {
			require_once $this->render('show-msg', false);
		} else {
			$this->redirect('dashboard?status=mensagem-nao-encontrada');
		}
			
	}

	public function nova_msg($params = null){
		$this->is_admin();
		$mensagens = false;
		if (is_array($params)) {
			$usuario = new Usuarios_Model;
			$usuario_id = $usuario->find_filter('id',"email = '{$_SESSION['login']}'");
			
			$mensagem = new Mensagens_Model;
			$id = $params[1];
			$mensagens = $mensagem->find($id);
		}

		require_once $this->render('nova-msg', false);
	}

	public function salvar_msg(){
		$this->is_admin();
		$mensagem = new Mensagens_Model;
		if ($_REQUEST){
			unset($_REQUEST['url']);
			unset($_REQUEST['salvar']);
			if ($mensagem->save()) {
				$this->redirect('dashboard/msg?status=msg-salva');
			} else {
				$this->redirect('dashboard/dashboard_admin?status=msg-nao-salva');
			}
		} else {
			$this->redirect('dashboard/dashboard_admin?status=envio-invalido');
		}
	} 

	public function atualizar_msg(){
		$this->is_admin();
		$mensagem = new Mensagens_Model;
	
		if ($_REQUEST){
			unset($_REQUEST['url']);
			unset($_REQUEST['salvar']);
			if ($mensagem->update()) {
				$this->redirect('dashboard/msg?status=msg-atualizada');
			} else {
				$this->redirect('dashboard/msg?status=msg-nao-salva');
			}
		} else {
			$this->redirect('dashboard/dashboard_admin?status=msg-nao-atualizada');
		}
	}

	public function ativar_usuario($id){
		$this->is_admin();
		date_default_timezone_set('America/Sao_Paulo');
    	$data = date('d/m/Y H:i', time());
		$usuario = new Usuarios_Model;

		$update = ['id'=>$id,'status'=>'ativo','data_ativacao'=>$data];
		$usuario->__update('usuarios',$update);

		$this->redirect('dashboard/dashboard_admin?status=usuario-ativado');
	}

	public function desativar_usuario($id){
		$this->is_admin();
		$usuario = new Usuarios_Model;

		$update = ['id'=>$id,'status'=>0,'data_ativacao'=>'-'];
		$usuario->__update('usuarios',$update);

		$this->redirect('dashboard/dashboard_admin?status=usuario-desativado');
	}

	public function deletar_usuario($id){
		$this->is_admin();
		$usuario = new Usuarios_Model;
		if ($usuario->delete($id)) {
			$this->redirect('dashboard/dashboard_admin?status=usuario-deletado');
		} else {
			$this->redirect('dashboard/dashboard_admin?status=arquivo-nao-encontrado');
		}
	}

	public function atualizar_senha(){
		if ($_REQUEST && isset($_REQUEST['url'])) {
			unset($_REQUEST['url']);
			unset($_REQUEST['salvar']);
			unset($_REQUEST['senha_antiga']);
			unset($_REQUEST['confirma']);

			$usuario = new Usuarios_Model;
			$usuarios = $usuario->find($_SESSION['id']);
			if ($usuarios[0]->nivel == 21) {
				$_REQUEST['senha'] = password_hash($_REQUEST['senha'], PASSWORD_DEFAULT);
				if ($usuario->update()) {
					$this->redirect('dashboard/alterar_dados/'.$_REQUEST['id'].'?status=dados-atualizados');
				} else {
					$this->redirect('dashboard/alterar_dados/'.$_REQUEST['id'].'?status=dados-nao-atualizados');
				}
			} else {
				if ($_SESSION['id'] == $_REQUEST['id']) {
					$_REQUEST['senha'] = password_hash($_REQUEST['senha'], PASSWORD_DEFAULT);
					if ($usuario->update()) {
						$this->redirect('dashboard/alterar_dados?status=dados-atualizados');
					} else {
						$this->redirect('dashboard/alterar_dados?status=dados-nao-atualizados');
					}
				} else {
					$this->redirect('dashboard/alterar_dados?status=id-invalido');
				}
			}
		}
		require_once $this->render('alterar-dados', false);
	}

	public function atualizar_dados(){
		if ($_REQUEST && isset($_REQUEST['url'])) {
			unset($_REQUEST['url']);
			unset($_REQUEST['salvar']);
			$usuario = new Usuarios_Model;
			$usuarios = $usuario->find($_SESSION['id']);
			if ($usuarios[0]->nivel == 21) {
				if ($usuario->update()) {
					$usuarios[0]->nick = $_REQUEST['nick'];
					$usuarios[0]->email = $_REQUEST['email'];
					$usuarios[0]->plano = $_REQUEST['plano'];
					$this->redirect('dashboard/alterar_dados/'.$_REQUEST['id'].'?status=dados-atualizados');
				} else {
					$this->redirect('dashboard/alterar_dados/'.$_REQUEST['id'].'?status=dados-nao-atualizados');
				}
			} else {
				if ($_SESSION['id'] == $_REQUEST['id']) {
					if ($usuario->update()) {
						$_SESSION['nick'] = $_REQUEST['nick'];
						$_SESSION['login'] = $_REQUEST['email'];
						$this->redirect('dashboard/alterar_dados?status=dados-atualizados');
					} else {
						$this->redirect('dashboard/alterar_dados?status=dados-nao-atualizados');
					}
				} else {
					$this->redirect('dashboard/alterar_dados?status=id-invalido');
				}
			}
		}
		require_once $this->render('alterar-dados', false);
	}

	public function alterar_dados($id = null){
		global $_QUERY;
		if ($this->user[0]->nivel == 21 && $id != null) {
			$usuario = new Usuarios_Model;
			$usuarios = $usuario->find($id);
			require_once $this->render('alterar-dados-admin', false);
		} else {
			require_once $this->render('alterar-dados', false);
		}
	}

	public function ajax_check_senha(){
		if ($_REQUEST) {
			$usuario = new Usuarios_Model;
			$usuarios = $usuario->find($_SESSION['id']);
			if (password_verify($_REQUEST['senha_antiga'], $usuarios[0]->senha)) {
				echo 'true';
			} else {
				echo 'false';
			}
		}
	}

	public function cadastrar_arquivos(){
		$helper = new Arquivos_Helper;
		$alert = new Alert_Helper;
		require_once $this->render('cadastrar-arquivos', false);
	}

	public function home_cadastro(){
		global $_QUERY;
		$helper = new Arquivos_Helper;
		$arquivo = new Arquivos_Model;
		if (isset($_QUERY['categoria']) && $_QUERY['categoria'] != 'todos') {
			$arquivos = $arquivo->find_filter('*',"tipo_geral='{$_QUERY['categoria']}'");
			if (empty($arquivos[0]->id)) {
				$arquivos = [];
			}
		} else {
			$arquivos = $arquivo->find_all();
		}
		require_once $this->render('home-cadastro', false);
	}

	public function salvar_arquivo(){
		$_REQUEST['fk_usuarios'] = $_SESSION['id'];
		if (isset($_REQUEST)) {
			$helper = new Arquivos_Helper;
			$arquivos = new Arquivos_Model;
			$_REQUEST['palavras_chave'] = $helper->rschar($_REQUEST['palavras_chave']);
			if ($arquivos->save()) {
				$this->redirect('home-cadastro?status=salvo');
			} else {
				$this->redirect('home-cadastro?status=nao-salvo');
			}
		} else {
			$this->redirect('error');
		}
	}

	public function editar_arquivo($id = null){
		global $_QUERY;
		$helper = new Arquivos_Helper;
		$alert = new Alert_Helper;
		if ($id != null) {
			$arquivo = new Arquivos_Model;
			$editar_arquivo = $arquivo->find($id);
			if ($editar_arquivo[0]->usuarios->id != $_SESSION['id']) {
				$this->redirect('home-cadastro');
			}
		} else {
			$this->redirect('error');
		}
		require_once $this->render('editar-arquivos', false);
	}

	public function atualizar_arquivo($id = null){
		if (isset($_REQUEST) && $id != null) {
			unset($_REQUEST['url']);
			$arquivos = new arquivos_Model;
			$helper = new Arquivos_Helper;
			$_REQUEST['palavras_chave'] = $helper->rschar($_REQUEST['palavras_chave']);
			if ($_REQUEST['fk_usuarios'] != $_SESSION['id']) {
				$this->redirect('home-cadastro?status=sem-premissao');
			} else {
				if ($arquivos->update()) {
					$this->redirect('dashboard/editar_arquivo/'.$id.'?status=atualizado');
				} else {
					$this->redirect('dashboard/editar_arquivo/'.$id.'?status=nao-atualizado');
				}
			}
		} else {
			$this->redirect('error');
		}	
	}
}