<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2017
	Controller: nomes
*/

class Nomes_Controller extends Controller_Core {
	function __construct(){
		parent::__construct();
		// setanto os meta dados
		$this->meta_title = 'Gerador de nomes - Help RPG';
		$this->meta_description = 'Utilitário para RPG que gera nomes para personagens.';
		$this->meta_keywords = 'nomes,  nomes de lugares, nome de classes, nomes de raças, nomes culturais, outros nomes, nomes de nave, nomes de super heróis';

		// [Voce pode passar arquivos css para a pagina do seu controller apenas 
		// informando o array como parametro de $this->set_base_css()]

		// chamando css em assets/css
		$this->css_files = $this->set_base_css(['init']);
		
		// chamando css interno dentro da view e concatenando ao css_files
		// $this->css_files .= $this->set_css(['index','home']);
		
		// [Voce pode passar arquivos javascript para ser chamado na view deste  
		// controller apenas passando um array com os nomes dos arquivos sem a 
		// extençao no array em $this->set_base_js]

		// chamada de arquivos js dentro de assets
		$this->js_files = $this->set_base_js([]);
		// chamada de arquivos js dentro da veiw 
		// $this->js_files .= $this->set_js(['index','teste']);
	}

	public function index(){
		$publicidade = new Publicidade_Helper;
		$url = new Init_Model;
		$nome = new Nomes_Model;
		require_once $this->render('index');
	}
	
	public function sortear($type = ''){
		$url = new Init_Model;
		$publicidade = new Publicidade_Helper;
		$nome = new Nomes_Model;
		if (!in_array($type, $nome->names)) {
			$this->redirect('error');
		} else {
			$file_name = strtolower(str_replace(' ', '_', "{$type}.txt"));
			$sorteio = new Raffleitemfile_Core($nome->file_path.$file_name, 10);
			require_once $this->render('sortear');
		}
	}
}