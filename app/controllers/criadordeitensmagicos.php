<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2017
	Controller: criadordeitensmagicos
*/

class Criadordeitensmagicos_Controller extends Controller_Core {
	
	function __construct(){
		parent::__construct();
		// setanto os meta dados
		$this->meta_title = 'Criador de itens mágicos';
		$this->meta_description = 'Criador de itens mágicos para RPG de mesa feito com base no livro de criação de itens mágicos da Tormenta RPG.';
		$this->meta_keywords = 'RPG, itens mágicos, Gerdor de itens mágicos, D&D, RPG de mesa, utilitários';

		// [Voce pode passar arquivos css para a pagina do seu controller apenas 
		// informando o array como parametro de $this->set_base_css()]

		// chamando css em assets/css
		$this->css_files = $this->set_base_css(['init']);
		
		// chamando css interno dentro da view e concatenando ao css_files
		// $this->css_files .= $this->set_css(['index','home']);
		
		// [Voce pode passar arquivos javascript para ser chamado na view deste  
		// controller apenas passando um array com os nomes dos arquivos sem a 
		// extençao no array em $this->set_base_js]

		// chamada de arquivos js dentro de assets
		$this->js_files = $this->set_base_js(['index']);
		// chamada de arquivos js dentro da veiw 
		// $this->js_files .= $this->set_js(['index','teste']);
	}

	public function index(){
		$publicidade = new Publicidade_Helper;
		$url = new Init_Model;
		$item = new Criadordeitensmagicos_Model;
		require_once $this->render('index');
	}
}