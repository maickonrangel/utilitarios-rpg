<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2018
	Controller: login
*/

class Login_Controller extends Controller_Core {
	function __construct(){
		parent::__construct();
		// setanto os meta dados
		$this->meta_title = 'Tela de Login';
		$this->meta_description = 'Login do Help RPG.';
		$this->meta_keywords = 'Login, entrar, Help RPG';

		// [Voce pode passar arquivos css para a pagina do seu controller apenas 
		// informando o array como parametro de $this->set_base_css()]

		// chamando css em assets/css
		$this->css_files = $this->set_base_css(['init']);
		
		// chamando css interno dentro da view e concatenando ao css_files
		// $this->css_files .= $this->set_css(['index','home']);
		
		// [Voce pode passar arquivos javascript para ser chamado na view deste  
		// controller apenas passando um array com os nomes dos arquivos sem a 
		// extençao no array em $this->set_base_js]

		// chamada de arquivos js dentro de assets
		$this->js_files = $this->set_base_js(['index']);
		// chamada de arquivos js dentro da veiw 
		// $this->js_files .= $this->set_js(['index','teste']);

	}
	
	public function index() {
		if (isset($_REQUEST)) {
			$usuario = new Usuarios_Model;
			$user = $usuario->find_filter('*',"email = '{$_REQUEST['email']}'");
			if (!empty($user[0]->email) && password_verify($_REQUEST['senha'], $user[0]->senha)
				&& $user[0]->status == 'ativo' && $user[0]->contador == 0) {

				$_SESSION['id'] 		= $user[0]->id;
				$_SESSION['login'] 		= $_REQUEST['email'];
				$_SESSION['nick'] 		= $user[0]->nick;
				$_SESSION['plano'] 		= $user[0]->plano;
				$_SESSION['cadastrado'] = $user[0]->created_at;
				$_SESSION['liberado'] 	= $user[0]->data_ativacao;

				$update = ['id'=>$user[0]->id,'contador'=> 1];
				$usuario->__update('usuarios',$update);

				if ($user[0]->nivel == 1) {
					$this->redirect('dashboard');
				} elseif ($user[0]->nivel == 21) {
					$this->redirect('dashboard/dashboard_admin');
				} else {
					$this->redirect('?status=usuario-errado');
				}
			} else if (!empty($user[0]->email) && password_verify($_REQUEST['senha'], $user[0]->senha)
				&& $user[0]->status == 'ativo' && $user[0]->contador == 1) {

				if(isset($_REQUEST['ativo']) && $_REQUEST['ativo'] == 'on') {
					$update = ['id'=>$user[0]->id,'contador'=> 0];
					$usuario->__update('usuarios',$update);
					$this->redirect('?status=resetado');
					exit();
				} 
				$this->redirect('?status=contador-invalido');
			} else {
				if (!empty($user[0]->email) && password_verify($_REQUEST['senha'], $user[0]->senha) && $user[0]->status == 0) {
					$this->redirect('?status=usuario-inativo');
				} else {
					$this->redirect('?status=usuario-errado');
				}
			}
		} else {
			$this->redirect('?status=usuario-errado');
		}
	}

	public function criar_conta(){
		if(isset($_REQUEST)) {
			unset($_REQUEST['url']);
			unset($_REQUEST['confirma']);
			unset($_REQUEST['Criar_conta']);
			$_REQUEST['senha'] = password_hash($_REQUEST['senha'], PASSWORD_DEFAULT);
			$usuario = new Usuarios_Model;
			$um_usuario = $usuario->find_filter('email',"email = {$_REQUEST['email']}");
			if (empty($um_usuario[0]->email)) {
				if ($usuario->save()) {
					$this->redirect('?status=conta-criada');
				} else {
					$this->redirect('?status=conta-nao-criada');
				}
			} else {
				$this->redirect('?status=usuario-ja-existe');
			}
		} else {
			$this->redirect();
		}
	}

	public function sair(){
		@session_start();
		session_destroy();
		$usuario = new Usuarios_Model;	
		$user = $usuario->find_filter('*',"email = '{$_SESSION['login']}'");
		$update = ['id'=>$user[0]->id,'contador'=> 0];
		$usuario->__update('usuarios',$update);
		$this->redirect('?status=saiu');
	}
}