<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2017
	Controller: aventurateca
*/

class Aventurateca_Controller extends Controller_Core {
	function __construct(){
		parent::__construct();
		// setanto os meta dados
		$this->meta_title = 'Aventurateca';
		$this->meta_description = 'Faça o download de arquivos de RPG com seus amigos';
		$this->meta_keywords = 'RPG, Utilitários, Aventuras, download';

		$this->css_files = $this->set_base_css(['init','dashboard','jquery.dataTables.min']);
		
		$this->js_files = $this->set_base_js(['jquery.min','jquery.dataTables.min','dashboard']);
	}

	public function index(){
		$url = new Init_Model;
		$arquivo = new Arquivos_Model;
		$helper = new Arquivos_Helper;
		$publicidade = new Publicidade_Helper;
		$aventurateca = new Aventurateca_Model;
		$arquivos = new stdClass();
		foreach ($helper->tipos_form as $key => $value) {
			$arquivos->$key = $arquivo->__sql("SELECT arquivos.*,usuarios.nick, (select COUNT(*) AS contador FROM arquivos WHERE tipo_geral='aventuras') as cont FROM arquivos INNER JOIN usuarios ON arquivos.fk_usuarios = usuarios.id WHERE tipo_geral='{$key}' ORDER BY id DESC LIMIT 0,5");
		}
		require_once $this->render('index', false);
	}

	public function load_recents(){
		$url = new Init_Model;
		$arquivo = new Arquivos_Model;
		$aventurateca = new Aventurateca_Model;
		$qntd = 3;
		$inicio = $qntd * ($_REQUEST['page']-1);
		$arquivos = $arquivo->__sql("SELECT arquivos.*,usuarios.nick FROM arquivos INNER JOIN usuarios ON arquivos.fk_usuarios = usuarios.id ORDER BY id DESC LIMIT {$inicio}, {$qntd}");
		foreach ($arquivos as $key => $value) {
			$value->cadastrado = (new DateTime($value->cadastrado))->format('d/m/Y H:i:s');
			$value->palavras_chave = $aventurateca->linkar_hashtag("#" . str_replace(", ", " #", $value->palavras_chave));
			$value->url_documento = $url->ads_link($value->url_documento);
		}
		echo json_encode($arquivos);
	}
	
	public function lista(){
		$url = new Init_Model;
		$arquivo = new Arquivos_Model;
		$aventurateca = new Aventurateca_Model;
		$helper = new Arquivos_Helper;
		$publicidade = new Publicidade_Helper;
		$qntd = 10;
		$inicio = $qntd * 0;
		$arquivos = $arquivo->__sql("SELECT arquivos.*,usuarios.nick FROM arquivos INNER JOIN usuarios ON arquivos.fk_usuarios = usuarios.id ORDER BY id DESC LIMIT {$inicio}, {$qntd}");

		require_once $this->render('lista', false);
	}

	public function preview($id = null){
		if (!is_numeric($id)) {
			$this->redirect('erro');
		} else {
			$url = new Init_Model;
			$arquivo = new Arquivos_Model;
			$publicidade = new Publicidade_Helper;
			$preview = $arquivo->__sql("SELECT arquivos.*,usuarios.nick FROM arquivos INNER JOIN usuarios ON arquivos.fk_usuarios = usuarios.id WHERE arquivos.id={$id}");
			if (empty($preview)) {
				$this->redirect('erro');
			} else {
				$preview = $preview[0];
				if (empty($preview->url_imagem)) {
					$preview->url_imagem = URL_BASE . '/app/assets/img/logo-principal.png';
				}
				require_once $this->render('preview', false);
			}
		}
	}

	public function tabelas($categoria = 'todos'){
		$url = new Init_Model;
		$arquivo = new Arquivos_Model;
		$helper = new Arquivos_Helper;
		$publicidade = new Publicidade_Helper;
		if ($categoria != 'todos' && array_key_exists($categoria, $helper->tipos_form)) {
			$arquivos = $arquivo->find_filter('*',"tipo_geral='{$categoria}'");
			if (empty($arquivos[0]->id)) {
				$arquivos = [];
			}
		} elseif ($categoria == 'todos') {
			$arquivos = $arquivo->find_all();
			if (empty($arquivos[0]->id)) {
				$arquivos = [];
			}
		} else {
			$this->redirect('erro');
		}

		require_once $this->render('tabelas', false);
	}

	public function padrao($categoria = 'todos'){
		global $_QUERY;
		if (isset($_QUERY['pagina']) && is_numeric($_QUERY['pagina']) && intval($_QUERY['pagina']) > 0) {
			$qntd = 100;
			$inicio = $qntd * ($_QUERY['pagina']-1);
		} else {
			$_QUERY['pagina'] = 1;
			$qntd = 100;
			$inicio = 0;
		}

		$url = new Init_Model;
		$arquivo = new Arquivos_Model;
		$helper = new Arquivos_Helper;
		$publicidade = new Publicidade_Helper;
		$aventurateca = new Aventurateca_Model;
		$contador = $arquivo->__count('arquivos',"tipo_geral='{$categoria}'");
		$contador = $contador/$qntd;
		$contador = intval($contador);
		$pagina = 'padrao';
		$pesquisa = null;

		if ($categoria != 'todos' && array_key_exists($categoria, $helper->tipos_form)) {
			$sql = "SELECT arquivos.*,usuarios.nick FROM arquivos INNER JOIN usuarios ON arquivos.fk_usuarios = usuarios.id WHERE arquivos.tipo_geral='{$categoria}' LIMIT {$inicio},{$qntd}";
			$arquivos = $arquivo->__sql($sql);
			if (empty($arquivos[0]->id)) {
				$arquivos = [];
			}
		} elseif ($categoria == 'todos') {
			$sql = "SELECT arquivos.*,usuarios.nick FROM arquivos INNER JOIN usuarios ON arquivos.fk_usuarios = usuarios.id LIMIT {$inicio},{$qntd}";
			$arquivos = $arquivo->__sql($sql);
			if (empty($arquivos[0]->id)) {
				$arquivos = [];
			}
		} else {
			$this->redirect('erro');
		}

		require_once $this->render('pesquisar', false);
	}


	public function pesquisar($pesquisa = null){
	
		if (is_array($pesquisa)) {
			$_REQUEST['categoria'] = $pesquisa[0];
			$_REQUEST['pesquisar'] = $pesquisa[1];
			$_REQUEST['sistema_rpg'] = $pesquisa[2];
		} else if (isset($_REQUEST['categoria']) && isset($_REQUEST['pesquisar']) && isset($_REQUEST['sistema_rpg'])) {
		} else {
			$_REQUEST['categoria'] = $pesquisa;
			$_REQUEST['pesquisar'] = '';
			$_REQUEST['sistema_rpg'] = '';
		}
		global $_QUERY;
		if (isset($_QUERY['pagina']) && is_numeric($_QUERY['pagina']) && intval($_QUERY['pagina']) > 0) {
			$qntd = 100;
			$inicio = $qntd * ($_QUERY['pagina']-1);
		} else {
			$_QUERY['pagina'] = 1;
			$qntd = 100;
			$inicio = 0;
		}

		$url = new Init_Model;
		$helper = new Arquivos_Helper;
		$publicidade = new Publicidade_Helper;
		$arquivo = new Arquivos_Model;
		$aventurateca = new Aventurateca_Model;

		$categoria = $_REQUEST['categoria'];
		$pesquisa = $_REQUEST['pesquisar'];

		$sql = "SELECT arquivos.*,usuarios.nick FROM arquivos INNER JOIN usuarios ON arquivos.fk_usuarios = usuarios.id ";
		$sql_count = "SELECT COUNT(arquivos.id) AS total FROM arquivos ";
		
		$pesquisa = $aventurateca->str_like_analyser($_REQUEST['pesquisar']);
		$sistema_rpg = '';
		$sistema_rpg_todos = '';
		if (!empty($_REQUEST['sistema_rpg'])) {
			$sistema_rpg = "AND sistema_rpg = '{$_REQUEST['sistema_rpg']}'";
			$sistema_rpg_todos = " sistema_rpg = '{$_REQUEST['sistema_rpg']}' AND ";
		}

		if ($_REQUEST['categoria'] != 'todos') {
			$sql .= " WHERE tipo_geral = '{$_REQUEST['categoria']}' {$sistema_rpg} ";
			if ($pesquisa != '') {
				$sql .= " AND (tipo_geral LIKE '%{$pesquisa}%'";
				$sql .= " OR tipo_documento LIKE '%{$pesquisa}%'";
				$sql .= " OR nome LIKE '%{$pesquisa}%'";
				$sql .= " OR palavras_chave LIKE '%{$pesquisa}%'";
				$sql .= " OR sistema_rpg LIKE '%{$pesquisa}%')";
			}
			$sql_count .= " WHERE tipo_geral = '{$_REQUEST['categoria']}' {$sistema_rpg}";
			if ($pesquisa != '') {
				$sql_count .= " AND (tipo_geral LIKE '%{$pesquisa}%'";
				$sql_count .= " OR tipo_documento LIKE '%{$pesquisa}%'";
				$sql_count .= " OR nome LIKE '%{$pesquisa}%'";
				$sql_count .= " OR palavras_chave LIKE '%{$pesquisa}%'";
				$sql_count .= " OR sistema_rpg LIKE '%{$pesquisa}%')";
			}
		} else {
			$sql .= " WHERE {$sistema_rpg_todos} (tipo_geral LIKE '%{$pesquisa}%'";
			$sql .= " OR tipo_documento LIKE '%{$pesquisa}%'";
			$sql .= " OR nome LIKE '%{$pesquisa}%'";
			$sql .= " OR palavras_chave LIKE '%{$pesquisa}%'";
			$sql .= " OR sistema_rpg LIKE '%{$pesquisa}%')";

			$sql_count .= " WHERE {$sistema_rpg_todos} (tipo_geral LIKE '%{$pesquisa}%'";
			$sql_count .= " OR tipo_documento LIKE '%{$pesquisa}%'";
			$sql_count .= " OR nome LIKE '%{$pesquisa}%'";
			$sql_count .= " OR palavras_chave LIKE '%{$pesquisa}%'";
			$sql_count .= " OR sistema_rpg LIKE '%{$pesquisa}%')";
			
		}
		$sql .= "LIMIT {$inicio},{$qntd}";
		$arquivos = $arquivo->__sql($sql);
		$contador = $arquivo->__sql($sql_count);
		if ($contador[0]->total == 0) {
			$resposta = 'Ninguém cadastrou isso ainda. Quer ser o primeiro a cadastrar?<br>Crie uma conta para cadastrar novos aquivos!';
		}
		$contador = $contador[0]->total/$qntd;
		$contador = intval($contador);
		$pagina = 'pesquisar';

		require_once $this->render('pesquisar', false);
	}

	public function pesquisar_hashtag($palavra = null){
		if ($palavra != null) {
			global $_QUERY;
			if (isset($_QUERY['pagina']) && is_numeric($_QUERY['pagina']) && intval($_QUERY['pagina']) > 0) {
				$qntd = 100;
				$inicio = $qntd * ($_QUERY['pagina']-1);
			} else {
				$_QUERY['pagina'] = 1;
				$qntd = 100;
				$inicio = 0;
			}
			$url = new Init_Model;
			$helper = new Arquivos_Helper;
			$arquivo = new Arquivos_Model;
			$publicidade = new Publicidade_Helper;
			$aventurateca = new Aventurateca_Model;
			
			$categoria = $palavra;
			$pesquisa = null;
			$pagina = 'pesquisar_hashtag';
			$contador = $arquivo->__count('arquivos',"palavras_chave LIKE '%{$palavra}%'");
			
			$contador = $contador/$qntd;
			$contador = intval($contador);

			$sql = "SELECT arquivos.*,usuarios.nick FROM arquivos INNER JOIN usuarios ON arquivos.fk_usuarios = usuarios.id ";
			$sql .= " WHERE palavras_chave LIKE '%{$palavra}%' LIMIT {$inicio},{$qntd}";
			$arquivos = $arquivo->__sql($sql);
		} else {
			$arquivos = [];
		}
		require_once $this->render('pesquisar', false);
	}

	public function pesquisar_midia($palavra = null){
		if ($palavra != null) {
			global $_QUERY;
			if (isset($_QUERY['pagina']) && is_numeric($_QUERY['pagina']) && intval($_QUERY['pagina']) > 0) {
				$qntd = 100;
				$inicio = $qntd * ($_QUERY['pagina']-1);
			} else {
				$_QUERY['pagina'] = 1;
				$qntd = 100;
				$inicio = 0;
			}

			$url = new Init_Model;
			$helper = new Arquivos_Helper;
			$publicidade = new Publicidade_Helper;
			$arquivo = new Arquivos_Model;
			$aventurateca = new Aventurateca_Model;
			
			$categoria = $palavra;
			$pesquisa = null;
			$pagina = 'pesquisar_hashtag';
			$contador = $arquivo->__count('arquivos',"tipo_documento LIKE '%{$palavra}%'");
			
			$contador = $contador/$qntd;
			$contador = intval($contador);

			$sql = "SELECT arquivos.*,usuarios.nick FROM arquivos INNER JOIN usuarios ON arquivos.fk_usuarios = usuarios.id ";
			$sql .= " WHERE tipo_documento LIKE '%{$palavra}%' LIMIT {$inicio},{$qntd}";
			$arquivos = $arquivo->__sql($sql);
		} else {
			$arquivos = [];
		}
		require_once $this->render('pesquisar', false);
	}

	public function pesquisar_categoria($palavra = null){
		if ($palavra != null) {
			global $_QUERY;
			if (isset($_QUERY['pagina']) && is_numeric($_QUERY['pagina']) && intval($_QUERY['pagina']) > 0) {
				$qntd = 100;
				$inicio = $qntd * ($_QUERY['pagina']-1);
			} else {
				$_QUERY['pagina'] = 1;
				$qntd = 100;
				$inicio = 0;
			}

			$url = new Init_Model;
			$helper = new Arquivos_Helper;
			$publicidade = new Publicidade_Helper;
			$arquivo = new Arquivos_Model;
			$aventurateca = new Aventurateca_Model;
			
			$categoria = $palavra;
			$pesquisa = null;
			$pagina = 'pesquisar_hashtag';
			$contador = $arquivo->__count('arquivos',"tipo_geral LIKE '%{$palavra}%'");
			
			$contador = $contador/$qntd;
			$contador = intval($contador);

			$sql = "SELECT arquivos.*,usuarios.nick FROM arquivos INNER JOIN usuarios ON arquivos.fk_usuarios = usuarios.id ";
			$sql .= " WHERE tipo_geral LIKE '%{$palavra}%' LIMIT {$inicio},{$qntd}";
			$arquivos = $arquivo->__sql($sql);
		} else {
			$arquivos = [];
		}
		require_once $this->render('pesquisar', false);
	}

	public function pesquisar_sistema($palavra = null){
		if ($palavra != null) {
			global $_QUERY;
			if (isset($_QUERY['pagina']) && is_numeric($_QUERY['pagina']) && intval($_QUERY['pagina']) > 0) {
				$qntd = 100;
				$inicio = $qntd * ($_QUERY['pagina']-1);
			} else {
				$_QUERY['pagina'] = 1;
				$qntd = 100;
				$inicio = 0;
			}

			$url = new Init_Model;
			$helper = new Arquivos_Helper;
			$publicidade = new Publicidade_Helper;
			$arquivo = new Arquivos_Model;
			$aventurateca = new Aventurateca_Model;
			
			$categoria = $palavra;
			$pesquisa = null;
			$pagina = 'pesquisar_hashtag';
			$contador = $arquivo->__count('arquivos',"sistema_rpg LIKE '%{$palavra}%'");
			
			$contador = $contador/$qntd;
			$contador = intval($contador);

			$sql = "SELECT arquivos.*,usuarios.nick FROM arquivos INNER JOIN usuarios ON arquivos.fk_usuarios = usuarios.id ";
			$sql .= " WHERE sistema_rpg LIKE '%{$palavra}%' LIMIT {$inicio},{$qntd}";

			$arquivos = $arquivo->__sql($sql);
		} else {
			$arquivos = [];
		}
		require_once $this->render('pesquisar', false);
	}

	public function galeria($pesquisa = 'imagens'){
		if (is_array($pesquisa)) {
			$_REQUEST['categoria'] = $pesquisa[0];
			$_REQUEST['pesquisar'] = $pesquisa[1];
		} else if (isset($_REQUEST['categoria']) && isset($_REQUEST['pesquisar'])) {
		} else {
			$_REQUEST['categoria'] = $pesquisa;
			$_REQUEST['pesquisar'] = '';
		}

		global $_QUERY;

		if (isset($_QUERY['pagina']) && is_numeric($_QUERY['pagina']) && intval($_QUERY['pagina']) > 0) {
			$qntd = 100;
			$inicio = $qntd * ($_QUERY['pagina']-1);
		} else {
			$_QUERY['pagina'] = 1;
			$qntd = 100;
			$inicio = 0;
		}

		$categoria = $_REQUEST['categoria'];
		$pesquisa = $_REQUEST['pesquisar'];

		$helper = new Arquivos_Helper;
		$publicidade = new Publicidade_Helper;
		$arquivo = new Arquivos_Model;
		$aventurateca = new Aventurateca_Model;
		$sql = "SELECT arquivos.*,usuarios.nick FROM arquivos INNER JOIN usuarios ON arquivos.fk_usuarios = usuarios.id ";
		$sql_count = "SELECT COUNT(arquivos.id) AS total FROM arquivos ";
		
		$pesquisa = $aventurateca->str_like_analyser($_REQUEST['pesquisar']);

		if ($_REQUEST['categoria'] != 'todos') {
			$sql .= " WHERE tipo_geral = '{$categoria}'";
			if ($pesquisa != '') {
				$sql .= "AND (tipo_geral LIKE '%{$pesquisa}%'";
				$sql .= " OR tipo_documento LIKE '%{$pesquisa}%'";
				$sql .= " OR nome LIKE '%{$pesquisa}%'";
				$sql .= " OR palavras_chave LIKE '%{$pesquisa}%'";
				$sql .= " OR sistema_rpg LIKE '%{$pesquisa}%')";
			}

			$sql_count .= " WHERE tipo_geral = '{$categoria}'";
			if ($pesquisa != '') {
				$sql_count .= " AND (tipo_geral LIKE '%{$pesquisa}%'";
				$sql_count .= " OR tipo_documento LIKE '%{$pesquisa}%'";
				$sql_count .= " OR nome LIKE '%{$pesquisa}%'";
				$sql_count .= " OR palavras_chave LIKE '%{$pesquisa}%'";
				$sql_count .= " OR sistema_rpg LIKE '%{$pesquisa}%')";
			}
		} else {
			$sql .= " WHERE tipo_geral LIKE '%{$pesquisa}%'";
			$sql .= " OR tipo_documento LIKE '%{$pesquisa}%'";
			$sql .= " OR nome LIKE '%{$pesquisa}%'";
			$sql .= " OR palavras_chave LIKE '%{$pesquisa}%'";
			$sql .= " OR sistema_rpg LIKE '%{$pesquisa}%'";

			$sql_count .= " WHERE tipo_geral LIKE '%{$pesquisa}%'";
			$sql_count .= " OR tipo_documento LIKE '%{$pesquisa}%'";
			$sql_count .= " OR nome LIKE '%{$pesquisa}%'";
			$sql_count .= " OR palavras_chave LIKE '%{$pesquisa}%'";
			$sql_count .= " OR sistema_rpg LIKE '%{$pesquisa}%'";
			
		}
		
		$sql .= "LIMIT {$inicio},{$qntd}";
		$arquivos = $arquivo->__sql($sql);
		$contador = $arquivo->__sql($sql_count);
		$contador = $contador[0]->total/$qntd;
		$contador = intval($contador);
		$pagina = 'galeria';

		require_once $this->render('galeria', false);
	}
}