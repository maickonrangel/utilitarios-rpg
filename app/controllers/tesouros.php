<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2017
	Controller: tesouros
*/

class Tesouros_Controller extends Controller_Core {
	function __construct(){
		parent::__construct();
		// setanto os meta dados
		$this->meta_title = 'Gerador de Tesouros RPG';
		$this->meta_description = 'Ferramenta online que gera tesouros para aventuras de RPG de mesa.';
		$this->meta_keywords = 'RPG, RPG de mesa, Tesouros, D&D 5e, D&D 3.5';

		// [Voce pode passar arquivos css para a pagina do seu controller apenas 
		// informando o array como parametro de $this->set_base_css()]

		// chamando css em assets/css
		$this->css_files = $this->set_base_css(['init']);
		
		// chamando css interno dentro da view e concatenando ao css_files
		// $this->css_files .= $this->set_css(['index','home']);
		
		// [Voce pode passar arquivos javascript para ser chamado na view deste  
		// controller apenas passando um array com os nomes dos arquivos sem a 
		// extençao no array em $this->set_base_js]

		// chamada de arquivos js dentro de assets
		$this->js_files = $this->set_base_js([]);
		// chamada de arquivos js dentro da veiw 
		// $this->js_files .= $this->set_js(['index','teste']);
	}

	public function index(){
		$publicidade = new Publicidade_Helper;
		$url = new Init_Model;
		$tesouros = [
			'Tesouro Individual: Desafio ND 0-4',
			'Tesouro Individual: Desafio ND 5-10',
			'Tesouro Individual: Desafio ND 11-16',
			'Tesouro Individual: Desafio ND 17 ou maior',
			'Objetos de arte',
			'Gemas',
			'Pilha de tesouros: Desafio ND 0-4',
			'Pilha de tesouros: Desafio ND 5-10',
			'Pilha de tesouros: Desafio ND 11-16',
			'Pilha de tesouros: Desafio ND 17 ou maior'
		];
		require_once $this->render('index');
	}
	
	public function sortear($type){
		$url = new Init_Model;
		$publicidade = new Publicidade_Helper;
		$tesouro = new Tesouros_Model;
		$tipos_de_tesouro = [
				'Tesouro Individual: Desafio ND 0-4' => '0_4',
				'Tesouro Individual: Desafio ND 5-10' => '5_10',
				'Tesouro Individual: Desafio ND 11-16' => '11_16',
				'Tesouro Individual: Desafio ND 17 ou maior' => '17_maior'
			];
		if (isset($tipos_de_tesouro[$type])) {
			for ($i=0; $i < 4; $i++) { 
				$tesouros[] = $tesouro->tesouro_individual($tipos_de_tesouro[$type]);
			}
		} elseif($type == 'Gemas') {
			$tesouros = $tesouro->gemas();
		} elseif ($type == 'Objetos de arte') {
			$tesouros = $tesouro->objetos_de_arte();
		} else {
			$tipos_de_pilha = [
				'Pilha de tesouros: Desafio ND 0-4' => '0_4',
				'Pilha de tesouros: Desafio ND 5-10' => '5_10',
				'Pilha de tesouros: Desafio ND 11-16' => '11_16',
				'Pilha de tesouros: Desafio ND 17 ou maior' => '17_maior'
			];
			for ($i=0; $i < 4; $i++) { 
				$tesouros[] = $tesouro->pilha_de_tesouros($tipos_de_pilha[$type]);
			}
		}
		require_once $this->render('sortear');
	}
}