<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2017
	Controller: init
*/

class Init_Controller extends Controller_Core {
	function __construct(){
		parent::__construct();
		// setanto os meta dados
		$this->meta_title = 'Utilitários - Help RPG';
		$this->meta_description = 'Utilitários para RPG de mesa desenvolvido pelo Help RPG.';
		$this->meta_keywords = 'Help RPG, RPG, RPG de mesa, Utilitários';

		// [Voce pode passar arquivos css para a pagina do seu controller apenas 
		// informando o array como parametro de $this->set_base_css()]

		// chamando css em assets/css
		$this->css_files = $this->set_base_css(['init']);
		
		// chamando css interno dentro da view e concatenando ao css_files
		// $this->css_files .= $this->set_css(['index','home']);
		
		// [Voce pode passar arquivos javascript para ser chamado na view deste  
		// controller apenas passando um array com os nomes dos arquivos sem a 
		// extençao no array em $this->set_base_js]

		// chamada de arquivos js dentro de assets
		$this->js_files = $this->set_base_js(['jquery']);
		// chamada de arquivos js dentro da veiw 
		// $this->js_files .= $this->set_js(['index','teste']);

		$this->utilitarios_terceiros = [
			'Gerador-de-cidades-medievais-com-imagem' 	=> [
				'nome' 	=> 'Gerador de cidades medievais em imagem',
				'url' 	=> 'https://watabou.itch.io/medieval-fantasy-city-generator'
			],
			'Gerador-de-cidades-medievais-em-3d'	=> [
				'nome' 	=> 'Gerador de cidades medievais em 3D <b>(você pode andar pela cidade! TOP)</b>',
				'url' 	=> 'https://watabou.itch.io/toy-town'
			],
			'Gerador-de-mapas-mundo-medieval'	=> [
				'nome' 	=> 'Gerador de mapas mundo medieval <b>(Inkarnate)</b>',
				'url' 	=> 'https://inkarnate.com'
			],
			'Criar-mapas-para-tavernas-e-masmorras'	=> [
				'nome' 	=> 'Criar mapas para tavernas e masmorras <b>(Pyromancers)</b>',
				'url' 	=> 'http://pyromancers.com/dungeon-painter-online/'
			],
			'Visualizador-de-cena-para-RPG'	=> [
				'nome' 	=> 'Visualizador de cena para RPG  <b>(Pyromancers)</b>',
				'url' 	=> 'http://pyromancers.com/rpg-forum-scene-viewer/'
			],
			'Gerador-de-mapas-para-masmorra'	=> [
				'nome' 	=> 'Gerador de mapas para masmorras  <b>(Gozzys)</b>',
				'url' 	=> 'http://www.gozzys.com/dungeon-maps'
			],
			'Gerador-de-mapas-para-cavernas'	=> [
				'nome' 	=> 'Gerador de mapas para cavernas  <b>(Gozzys)</b>',
				'url' 	=> 'http://www.gozzys.com/cave-maps'
			],
			'Gerador-de-mapas-para-regioes-selvagens'	=> [
				'nome' 	=> 'Gerador de mapas para regiões selvanegs  <b>(Gozzys)</b>',
				'url' 	=> 'http://www.gozzys.com/wilderness-maps'
			],
			'Galeria-de-mapas'	=> [
				'nome' 	=> 'Galeria de mapas <b>(Gozzys)</b>',
				'url' 	=> 'http://www.gozzys.com/gallery'
			],
			'Gerador-de-masmorras-do-donjon'	=> [
				'nome' 	=> 'Gerador de masmorras <b>(Donjon)</b>',
				'url' 	=> 'http://donjon.bin.sh/d20/dungeon/index.cgi'
			],
			'Gerador-de-aventuras-usando-cartas-de-magic'	=> [
				'nome' 	=> 'Gerador de aventuras usando cartas de Magic',
				'url' 	=> 'http://gerador.ultrav.com.br/'
			],
			'Criar-fichas-para-o-um-anel-RPG'	=> [
				'nome' 	=> 'Criar ficha para <b>(O um anel RPG)</b>',
				'url' 	=> 'http://azrapse.es/tor/sheet.html'
			],
			'Gerador-de-mapas-do-daves-mapper'	=> [
				'nome' 	=> 'Gerador de mapas <b>(Daves Mapper)</b>',
				'url' 	=> 'http://davesmapper.com/'
			],
			'Catalogo-de-mapas-do-paratime'	=> [
				'nome' 	=> 'Catálogo de mapas <b>(Paratime)</b>',
				'url' 	=> 'http://www.paratime.ca/cartography/index.html'
			],
			'Gerador-de-mapas-de-cidade-inkwellideas'	=> [
				'nome' 	=> 'Gerador de mapas de cidade <b>(Inkwellideas)</b>',
				'url' 	=> 'http://inkwellideas.com/free-tools/random-city-map-generator/'
			],
			'Gerador-de-mapas-de-vilarejo-inkwellideas'	=> [
				'nome' 	=> 'Gerador de mapas de vilarejos <b>(Inkwellideas)</b>',
				'url' 	=> 'http://www.inkwellideas.com/roleplaying_tools/random_village/index.jsp'
			],
			'Gerador-de-mapas-de-masmorras-inkwellideas'	=> [
				'nome' 	=> 'Gerador de mapas de masmorras <b>(Inkwellideas)</b>',
				'url' 	=> 'http://www.inkwellideas.com/roleplaying_tools/random_dungeon/'
			],
			'Gerador-de-mapas-de-estalagem-inkwellideas'	=> [
				'nome' 	=> 'Gerador de mapas de estalagem <b>(Inkwellideas)</b>',
				'url' 	=> 'http://inkwellideas.com/free-tools/random-inn-generator-floorplan/?submit=submit'
			],
			'Planilha-de-personagem-para-Dungeons-and-Dragons-3.5'	=> [
				'nome' 	=> 'Planilha de Personagem D&D 3.5 <b>(Mfribeiro)</b>',
				'url' 	=> 'https://chrome.google.com/webstore/detail/planilha-de-personagem-dd/ijmoclncnlplomapiccaifhpjnolbfch?hl=pt-BR'
			],
			'Gerador-de-fichas-para-Dungeons-and-Dragons-5e'	=> [
				'nome' 	=> 'Gerador de fichas para D&D 5e <b>(Rpgtinker)</b>',
				'url' 	=> 'https://rpgtinker.com/'
			],
			'Ferramenta-de-audio-para-jogos-de-RPG'	=> [
				'nome' 	=> 'Ferramenta de áudio para RPG <b>(Tabletopaudio)</b>',
				'url' 	=> 'https://tabletopaudio.com/'
			],
			'Editor-de-mapas-2D-para-RPG'	=> [
				'nome' 	=> 'Ferramenta de criação de mapas para RPG <b>(ANAmap)</b>',
				'url' 	=> 'http://deepnight.net/tools/tabletop-rpg-map-editor/'
			],
			'Gerador-de-mapa-poligonal'	=> [
				'nome' 	=> 'Gerador de mapa poligonal <b>(Redblobgames)</b>',
				'url' 	=> 'https://www.redblobgames.com/maps/mapgen2/'
			],
			'Ficha-epica-web'	=> [
				'nome' 	=> 'Ficha Épica - criação de personagens do Épico RPG (Versão web)</b>',
				'url' 	=> 'https://ficha.epicorpg.com.br/?fbclid=IwAR2XxZM08jRFlqXIaEY-8IbIVYGIS4pggjjNPaRUgVAcMj2EsyEEvLgTBw4'
			],
			'Ficha-epica-mobile'	=> [
				'nome' 	=> 'Ficha Épica - criação de personagens do Épico RPG (Versão mobile App)</b>',
				'url' 	=> 'https://play.google.com/store/apps/details?id=br.com.epicorpg.ficha&fbclid=IwAR23FyVpvehzmBthXjb5Xw1A-NmwmYUs2RmXOawYYS2bPkcQ2XJBbpGho2Q'
			]

		];
	}

	public function index(){
		$url = new Init_Model;
		global $_QUERY;
		$display = new Mensagens_Helper;
		$publicidade = new Publicidade_Helper;
		$modules = [
			'Fluxovida-Old-Dragon' 	=> 'Old Dragon Fluxovida - Help RPG',
			'Cards-para-RPG' 	=> 'Gerador de cartas para RPG - Help RPG',
			'Monstros-para-Tormenta-RPG' 	=> 'Gerador de monstros para Tormenta RPG - Help RPG',
			'Salvar-fichas-de-RPG-online' 	=> 'Cadastrar Fichas de RPG - Help RPG',
			'Rolar-dados-para-RPG' 	=> 'Rolagem de Dados - Help RPG',
			'Gerador-de-Doencas-para-NPC-ou-PJ' 	=> 'Gerador de Doenças P/ NPC ou PJ - Help RPG',
			'Gerador-de-Missoes-Para-Mercenarios' 	=> 'Gerador de Missões P/ Mercenários - Help RPG',
			'Gerador-de-Fichas-Dungeons-and-Dragons-3.5'	=> 'Gerador de Fichas D&D 3.5 - Help RPG',
			'Gerador-de-Fichas-para-Tormenta-RPG'	=> 'Gerador de Fichas para Tormenta RPG - Help RPG',
			'Gerador-de-Fichas-para-Vampiro'	=> 'Gerador de Fichas para Vampiro: A Mascara - Help RPG',
			'Gerador-de-tavernas-para-RPG' 	=> 'Gerador de tavernas - Help RPG',
			'Gerador-de-itens-para-Tormenta-RPG' 	=> 'Gerador de itens para Tormenta RPG - Help RPG',
			'Gerador-de-encontros-aleatorios-para-RPG' 	=> 'Gerador de encontros aleatórios - Help RPG',
			'Gerador-de-descricao-de-personagens-para-RPG' => 'Gerador de descrição - Help RPG',
			'Frases-epicas-para-RPG'	=> 'Frases épicas - Help RPG',
			'Gerador-de-Deturpacoes-magicas-para-RPG'	=> 'Gerador de Deturpações mágicas - Help RPG',
			'Gerador-de-Corrupcoes-para-RPG'	=> 'Gerador de Corrupções - Help RPG',
			'Gerador-de-sorte-para-RPG'	=> 'Gerador de sorte - Help RPG',
			'Gerador-de-nome-de-espadas-para-RPG' => 'Gerador de nome de espadas - Help RPG',
			'Gerador-de-nomes-para-RPG' => 'Gerador de nomes - Help RPG',
			'Gerador-de-aventuras-para-RPG' => 'Gerador de aventuras -Help RPG',
			'Gerador-de-masmorras-para-RPG' => 'Gerador de masmorras - Help RPG',
			'Gerador-de-cidades-para-RPG' => 'Gerador de cidades - Help RPG',
			'Gerador-de-itens-para-RPG' => 'Gerador de itens - Help RPG',
			'Gerador-de-tempo-climatico-para-RPG' => 'Gerador de tempo climático - Help RPG',
			'Geradores-do-Livro-do-mestre-de-Dungeons-and-Dragons-5e' => 'Geradores do Livro do mestre D&D 5e - Help RPG',
			'Geradores-assentamentos-para-Dungeons-and-Dragons-5e' => 'Geradores assentamentos D&D 5e - Help RPG',
			'Gerador-de-encontros-de-monstros-por-ambiente-para-Dungeons-and-Dragons-5e' => 'Gerador de encontros de monstros por ambiente D&D 5e - Help RPG',
			'Gerador-de-tesouros-para-Dungeons-and-Dragons-5e' => 'Gerador de tesouros D&D 5e - Help RPG',
			'Gerador-de-PdMs-para-Dungeons-and-Dragons-5e' => 'Gerador de PdMs D&D 5e - Help RPG'
		];
		require_once $this->render('index');
	}

	public function utilitariosterceiros(){
		$url = new Init_Model;
		$publicidade = new Publicidade_Helper;
		global $_QUERY;
		$display = new Mensagens_Helper;

		require_once $this->render('utilitarios-de-terceiros');
	}

	public function redirect_site($url = ''){
		if (empty($url)) {
			$this->redirect();
		} else {
			$url = $this->utilitarios_terceiros[$url]['url'];
			header("Location: {$url}");
		}
	}
}