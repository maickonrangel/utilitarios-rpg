<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2017
	Controller: especial
*/

class Especial_Controller extends Controller_Core {
	function __construct(){
		parent::__construct();
		// setanto os meta dados
		$this->meta_title = 'Página Especial';
		$this->meta_description = 'Faça login rápido com limite de tempo.';
		$this->meta_keywords = 'Help RPG, Utilitários RPG, Especial, Login';
		// [Voce pode passar arquivos css para a pagina do seu controller apenas 
		// informando o array como parametro de $this->set_base_css()]

		// chamando css em assets/css
		$this->css_files = $this->set_base_css(['init','dashboard']);
		
		// chamando css interno dentro da view e concatenando ao css_files
		// $this->css_files .= $this->set_css(['index','home']);
		
		// [Voce pode passar arquivos javascript para ser chamado na view deste  
		// controller apenas passando um array com os nomes dos arquivos sem a 
		// extençao no array em $this->set_base_js]

		// chamada de arquivos js dentro de assets
		$this->js_files = $this->set_base_js(['jquery']);
		// chamada de arquivos js dentro da veiw 
		// $this->js_files .= $this->set_js(['index','teste']);
	}

	public function index(){
		$url = new Init_Model;
		$publicidade = new Publicidade_Helper;
		require_once $this->render('index', false);
	}
	
	public function links(){
		$url = new Init_Model;
		$publicidade = new Publicidade_Helper;
		require_once $this->render("links", false);
	}

	public function login(){
		if (isset($_REQUEST)) {
			$time = strtotime(date("Y-m-d H:i:s"));
			$endTime = date("Y-m-d H:i:s", strtotime('+60 minutes', $time));
			session_start();
			$_SESSION['login'] = $_REQUEST['email'];
			$_SESSION['plano'] = 1;
			$_SESSION['usuarios_logados'] = 1;
			$_SESSION['expira'] = $endTime;
			//Produco 	-  http://zipansion.com/35yP7
			//Local 	- http://zipansion.com/35tUi
			$this->redirect('http://zipansion.com/35yP7', false);
		} else {
			$this->error();
		}
	}
}