<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2017
	Controller: aventuras
*/

class Aventuras_Controller extends Controller_Core {
	function __construct(){
		parent::__construct();
		// setanto os meta dados
		$this->meta_title = 'Utilitário gerador de aventuras para RPG';
		$this->meta_description = 'Ferramenta online que gera aventuras prontas para RPG.';
		$this->meta_keywords = 'RPG, Utilitários, aventuras de RPG, Aventuras';

		// [Voce pode passar arquivos css para a pagina do seu controller apenas 
		// informando o array como parametro de $this->set_base_css()]

		// chamando css em assets/css
		$this->css_files = $this->set_base_css(['init']);
		
		// chamando css interno dentro da view e concatenando ao css_files
		// $this->css_files .= $this->set_css(['index','home']);
		
		// [Voce pode passar arquivos javascript para ser chamado na view deste  
		// controller apenas passando um array com os nomes dos arquivos sem a 
		// extençao no array em $this->set_base_js]

		// chamada de arquivos js dentro de assets
		$this->js_files = $this->set_base_js([]);
		// chamada de arquivos js dentro da veiw 
		// $this->js_files .= $this->set_js(['index','teste']);
	}

	public function index(){
		$publicidade = new Publicidade_Helper;
		$url = new Init_Model;
		$adv = ['medieval','starwars','apocaliptica'];
		require_once $this->render('index');
	}
	
	public function sortear($type){
		$url = new Init_Model;
		$publicidade = new Publicidade_Helper;	
		$adv = ['medieval','starwars','apocaliptica'];
		if (!in_array($type, $adv)) {
			$this->redirect('error');
		} else {
			$aventura = new Aventuras_Model;
			$aventura->generate_adventure("{$type}");

			$aventuras = [
				'Antagonista' => $aventura->antagonista->getRaffleItens(),
				'Coadjuvante' => $aventura->coadjuvante->getRaffleItens(),
				'Complicacao' => $aventura->complicacao->getRaffleItens(),
				'Localidade' => $aventura->localidade->getRaffleItens(),
				'Objetivo' => $aventura->objetivo->getRaffleItens(),
				'Recompensa' => $aventura->recompensa->getRaffleItens(),
			];
			require_once $this->render('sortear');
		}
	}

}