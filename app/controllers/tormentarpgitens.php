<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2017
	Controller: tormentarpgitens
*/

class Tormentarpgitens_Controller extends Controller_Core {
	function __construct(){
		parent::__construct();
		// setanto os meta dados
		$this->meta_title = 'Gerador de Itens para Tormenta RPG';
		$this->meta_description = 'Gerador de itens mágicos para tormenta RPG.';
		$this->meta_keywords = 'RPG, Utilitários, RPG de mesa, Tormenta RPG, Itens mágicos';

		// [Voce pode passar arquivos css para a pagina do seu controller apenas 
		// informando o array como parametro de $this->set_base_css()]

		// chamando css em assets/css
		$this->css_files = $this->set_base_css(['init']);
		
		// chamando css interno dentro da view e concatenando ao css_files
		// $this->css_files .= $this->set_css(['index','home']);
		
		// [Voce pode passar arquivos javascript para ser chamado na view deste  
		// controller apenas passando um array com os nomes dos arquivos sem a 
		// extençao no array em $this->set_base_js]

		// chamada de arquivos js dentro de assets
		$this->js_files = $this->set_base_js(['index','teste']);
		// chamada de arquivos js dentro da veiw 
		// $this->js_files .= $this->set_js(['index','teste']);
	}

	public function index(){
		$publicidade = new Publicidade_Helper;
		$url = new Init_Model;
		$item = new Tormentarpgitens_Model;
		require_once $this->render('index');
	}

	public function sortear($type){
		$url = new Init_Model;
		$publicidade = new Publicidade_Helper;
		$item = new Tormentarpgitens_Model;
		if (isset($item->itens[$type])) {
			$titulo = $item->itens[$type];
			$itens = [];
			if ($type == 'armaduras' || $type == 'armas' || $type == 'escudos') {
				$itens = $item->sortear_item($type, 2);
			} elseif ($type == 'todos_os_pergaminhos') {
				$itens = $item->sortear_todos_pergaminhos();
			} elseif ($type == 'todos_os_itens') {
				$itens = $item->sortear_todos_itens();
			} elseif ($type == 'todos_acessorio') {
				$itens = $item->sortear_todos_acessorios();
			} elseif ($type == 'armas_magicas' || $type == 'armas_magicas_de_ataque_a_distancia' || $type == 'escudos_magicos' || $type == 'armaduras_magicas') {
				$tipo_de_item = [
					'armas_magicas' => ['armas_comuns','poderes_de_armas_corpo_a_corpo'],
					'armas_magicas_de_ataque_a_distancia' => ['armas_a_distancia_comuns','poderes_de_armas_de_ataque_a_distancia'],
					'escudos_magicos' => ['escudos_comuns','poderes_de_escudos'],
					'armaduras_magicas' => ['armaduras_comuns','poderes_armaduras']
				];

				$nome = $tipo_de_item[$type];
				$hab = $tipo_de_item[$type];
				$itens = $item->itens_magicos($nome[0], $hab[1]);
			} elseif ($type == 'gemas' || $type == 'obras_de_arte') {
				$itens = $item->objetos($type);
			} else {
				$itens = $item->sortear_item($type, 4);
			}

			require_once $this->render('sortear');
		} else {
			$this->redirect('error');
		}
	}
	
}