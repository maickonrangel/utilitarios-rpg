<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2017
	Controller: itens
*/

class Itens_Controller extends Controller_Core {
	function __construct(){
		parent::__construct();
		// setanto os meta dados
		$this->meta_title = 'Gerador de itens';
		$this->meta_description = 'Gerador de itens mágicos para RPG de mesa.';
		$this->meta_keywords = 'RPG, Itens, RPG de mesa, Itens mágicos, D&D, D&D 5e, Utilitários';

		// [Voce pode passar arquivos css para a pagina do seu controller apenas 
		// informando o array como parametro de $this->set_base_css()]

		// chamando css em assets/css
		$this->css_files = $this->set_base_css(['init']);
		
		// chamando css interno dentro da view e concatenando ao css_files
		// $this->css_files .= $this->set_css(['index','home']);
		
		// [Voce pode passar arquivos javascript para ser chamado na view deste  
		// controller apenas passando um array com os nomes dos arquivos sem a 
		// extençao no array em $this->set_base_js]

		// chamada de arquivos js dentro de assets
		$this->js_files = $this->set_base_js(['index','teste']);
		// chamada de arquivos js dentro da veiw 
		// $this->js_files .= $this->set_js(['index','teste']);
	}

	public function index(){
		$url = new Init_Model;
		$publicidade = new Publicidade_Helper;
		$opcoes = [
			'caracteristicas_especiais',
			'item_inteligente',
			'aneis_magicos',
			'armaduras',
			'armaduras_magicas',
			'armas_comuns',
			'armas_magicas',
			'bugigangas',
			'habilidade_magica',
			'itens_adversos',
			'itens_estupidos',
			'itens_magicos_ded5e_comuns',
			'itens_magicos_ded5e_incomun',
			'itens_magicos_ded5e_lendarios',
			'itens_magicos_ded5e_muito_raros',
			'itens_magicos_ded5e_raros',
			'itens_magicos_ded5e_tabela_a',
			'itens_magicos_ded5e_tabela_b',
			'itens_magicos_ded5e_tabela_c',
			'itens_magicos_ded5e_tabela_d',
			'itens_magicos_ded5e_tabela_e',
			'itens_magicos_ded5e_tabela_f',
			'itens_magicos_ded5e_tabela_g',
			'itens_magicos_ded5e_tabela_h',
			'itens_magicos_ded5e_tabela_i'
		];

		require_once $this->render('index');
	}
	
	public function sortear($type){
		$url = new Init_Model;
		$publicidade = new Publicidade_Helper;
		$item = new Itens_Model;
		if ($type == 'caracteristicas_especiais') {
			$itens = $item->caracteristicas_especiais();
		} elseif ($type == 'item_inteligente') {
			$itens = $item->item_inteligente();
		} else {
			$itens = $item->$type(5);
		}
		require_once $this->render('sortear');
	}
}