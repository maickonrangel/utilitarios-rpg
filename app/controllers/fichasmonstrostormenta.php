<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2017
	Controller: fichasmonstrostormenta
*/

class Fichasmonstrostormenta_Controller extends Controller_Core {
	function __construct(){
		parent::__construct();
		// setanto os meta dados
		$this->meta_title = 'Fichas de fichas de monstros - Tormenta RPG';
		$this->meta_description = 'Gerador de fichas de monstros para Tormenta RPG.';
		$this->meta_keywords = 'Utilitários, Gerador, Fichas, Monstros, Tormenta RPG, RPG de mesa';

		// [Voce pode passar arquivos css para a pagina do seu controller apenas 
		// informando o array como parametro de $this->set_base_css()]

		// chamando css em assets/css
		$this->css_files = $this->set_base_css(['init']);
		
		// chamando css interno dentro da view e concatenando ao css_files
		// $this->css_files .= $this->set_css(['index','home']);
		
		// [Voce pode passar arquivos javascript para ser chamado na view deste  
		// controller apenas passando um array com os nomes dos arquivos sem a 
		// extençao no array em $this->set_base_js]

		// chamada de arquivos js dentro de assets
		$this->js_files = $this->set_base_js(['jquery','html2canvas.min','export']);
		// chamada de arquivos js dentro da veiw 
		// $this->js_files .= $this->set_js(['index','teste']);
	}

	public function index(){
		$publicidade = new Publicidade_Helper;
		$url = new Init_Model;
		if (isset($_REQUEST['forma']) || isset($_REQUEST['tamanho']) || isset($_REQUEST['tipo']) || isset($_REQUEST['nivel'])) {
			if ($_REQUEST['forma'] == '') {
				$_REQUEST['forma'] = null;
			}

			if ($_REQUEST['tamanho'] == '') {
				$_REQUEST['tamanho'] = null;
			}

			if ($_REQUEST['tipo'] == '') {
				$_REQUEST['tipo'] = null;
			}

			if ($_REQUEST['nivel'] == '') {
				$_REQUEST['nivel'] = null;
			}
			$monstro = new Fichasmonstrostormenta_Model($_REQUEST['nivel'], $_REQUEST['tipo'], $_REQUEST['tamanho'], $_REQUEST['forma']);
		} else{
			$monstro = new Fichasmonstrostormenta_Model;
		}
		require_once $this->render('index');
	}
	
}