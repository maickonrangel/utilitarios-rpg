<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2017
	Controller: fichasvampiro
*/

class Fichasvampiro_Controller extends Controller_Core {

	function __construct(){
		parent::__construct();
		// setanto os meta dados
		$this->meta_title = 'Gerador de Fichas de Vampiro';
		$this->meta_description = 'Ferramenta online que gera fichas para vampiro a mascara.';
		$this->meta_keywords = 'RPG, vampiros, vampiro a mascara, RPG de mesa, utilitários, Ferramentas';

		// [Voce pode passar arquivos css para a pagina do seu controller apenas 
		// informando o array como parametro de $this->set_base_css()]

		// chamando css em assets/css
		$this->css_files = $this->set_base_css(['init']);
		
		// chamando css interno dentro da view e concatenando ao css_files
		// $this->css_files .= $this->set_css(['index','home']);
		
		// [Voce pode passar arquivos javascript para ser chamado na view deste  
		// controller apenas passando um array com os nomes dos arquivos sem a 
		// extençao no array em $this->set_base_js]

		// chamada de arquivos js dentro de assets
		$this->js_files = $this->set_base_js(['jquery','html2canvas.min','card','export']);
		// chamada de arquivos js dentro da veiw 
		// $this->js_files .= $this->set_js(['index','teste']);
	}

	public function index(){
		$publicidade = new Publicidade_Helper;
		$url = new Init_Model;
		if (isset($_REQUEST['geracao']) || isset($_REQUEST['cla']) || isset($_REQUEST['natureza']) || isset($_REQUEST['arquetipo'])) {
			if ($_REQUEST['geracao'] == '') {
				$_REQUEST['geracao'] = null;
			}

			if ($_REQUEST['cla'] == '') {
				$_REQUEST['cla'] = null;
			}

			if ($_REQUEST['natureza'] == '') {
				$_REQUEST['natureza'] = null;
			}

			if ($_REQUEST['arquetipo'] == '') {
				$_REQUEST['arquetipo'] = null;
			}

			$ficha = new Fichasvampiro_Model($_REQUEST['geracao'], $_REQUEST['cla'], $_REQUEST['natureza'], $_REQUEST['arquetipo']);
		} else{
			$ficha = new Fichasvampiro_Model(null, null, null, null);
		}
		$arq_list = $ficha->get_arquetipo();
		$nat_list = $ficha->get_natureza();
		$cla_list = $ficha->get_cla();
		require_once $this->render('index');
	}
}
