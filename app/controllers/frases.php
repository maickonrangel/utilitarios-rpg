<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2017
	Controller: frases
*/

class Frases_Controller extends Controller_Core {
	function __construct(){
		parent::__construct();
		// setanto os meta dados
		$this->meta_title = 'Gerador de Frases';
		$this->meta_description = 'Ferramenta online que sorteia frases incríveis para personagens de RPG de mesa.';
		$this->meta_keywords = 'Frases, RPG, Gerador de frases, RPG de mesa, Frases famosas, frases de guerreiro, frases de mago, frases de samurai, frases de druida, frases de mago';

		// [Voce pode passar arquivos css para a pagina do seu controller apenas 
		// informando o array como parametro de $this->set_base_css()]

		// chamando css em assets/css
		$this->css_files = $this->set_base_css(['init']);
		
		// chamando css interno dentro da view e concatenando ao css_files
		// $this->css_files .= $this->set_css(['index','home']);
		
		// [Voce pode passar arquivos javascript para ser chamado na view deste  
		// controller apenas passando um array com os nomes dos arquivos sem a 
		// extençao no array em $this->set_base_js]

		// chamada de arquivos js dentro de assets
		$this->js_files = $this->set_base_js(['index','teste']);
		// chamada de arquivos js dentro da veiw 
		// $this->js_files .= $this->set_js(['index','teste']);
	}

	public function index(){
		$url = new Init_Model;
		$publicidade = new Publicidade_Helper;
		$frases = [
			'druidas' => 'Druidas',
			'epicas' => 'Frases épicas',
			'famosas' => 'Frases famosas',
			'guerreiros' => 'Frases de guerreiro',
			'magos' => 'Frases de mago',
			'paladinos' => 'Frases de paladino',
			'samurais' => 'Frases de samurai',
			'bardo' => 'Frases de bardo',
			'clerigo' => 'Frases de clérigo',
			'ladino' => 'Frases de ladino',
		];

		require_once $this->render('index');
	}
	
	public function sortear($type){
		$url = new Init_Model;
		$publicidade = new Publicidade_Helper;
		$frases = ['Druidas','Frases épicas','Frases famosas','Frases de guerreiro','Frases de mago','Frases de paladino','Frases de samurai','Frases de bardo','Frases de clérigo','Frases de ladino'];
		
		if (!in_array($type, $frases)) {
			$this->redirect('error');
		} else {
			$frases = new Frases_Model;
			$frases_sorteadas = null;
			switch ($type) {
				case 'Druidas': $frases_sorteadas = $frases->druidas(10);
				break;

				case 'Frases épicas': $frases_sorteadas = $frases->frases_epicas(10);
				break;

				case 'Frases famosas': $frases_sorteadas = $frases->frases_famosas(10);
				break;

				case 'Frases de guerreiro': $frases_sorteadas = $frases->guerreiro(10);
				break;

				case 'Frases de mago': $frases_sorteadas = $frases->magos(10);
				break;

				case 'Frases de paladino': $frases_sorteadas = $frases->paladinos(10);
				break;
				
				case 'Frases de samurai': $frases_sorteadas = $frases->samurai(10);
				break;

				case 'Frases de bardo': $frases_sorteadas = $frases->bardo(10);
				break;

				case 'Frases de clérigo': $frases_sorteadas = $frases->clerigo(10);
				break;
				
				case 'Frases de ladino': $frases_sorteadas = $frases->ladino(10);
				break;
			}	
			require_once $this->render('sortear');
		}
	}
}