<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2017
	Controller: monstros
*/

class Monstros_Controller extends Controller_Core {
	function __construct(){
		parent::__construct();
		// setanto os meta dados
		$this->meta_title = 'Gerador de monstros';
		$this->meta_description = 'Gerador de monstros por ambiente para D&D 5e.';
		$this->meta_keywords = 'RPG, Gerador de monstros, RPG de mesa, D&D 5e';

		// [Voce pode passar arquivos css para a pagina do seu controller apenas 
		// informando o array como parametro de $this->set_base_css()]

		// chamando css em assets/css
		$this->css_files = $this->set_base_css(['init']);
		
		// chamando css interno dentro da view e concatenando ao css_files
		// $this->css_files .= $this->set_css(['index','home']);
		
		// [Voce pode passar arquivos javascript para ser chamado na view deste  
		// controller apenas passando um array com os nomes dos arquivos sem a 
		// extençao no array em $this->set_base_js]

		// chamada de arquivos js dentro de assets
		$this->js_files = $this->set_base_js(['index','teste']);
		// chamada de arquivos js dentro da veiw 
		// $this->js_files .= $this->set_js(['index','teste']);
	}

	public function index(){
		$url = new Init_Model;
		$publicidade = new Publicidade_Helper;
		$monstro = new Monstros_Model;
		$monstro->sortear('artico');
		$ambientes = [
			'artico',
			'colina',
			'costa',
			'deserto',
			'floresta',
			'montanha',
			'pantano',
			'planice',
			'subaquatico',
			'subterraneo',
			'urbano'
		];

		if (isset($_REQUEST['ambiente']) and $_REQUEST['ambiente'] != '') {
			$ambiente = $_REQUEST['ambiente'];
		} else {
			$escolhido = array_rand($ambientes);
			$ambiente = $ambientes[$escolhido];
		}

		$monstros_sorteados = $monstro->$ambiente($ambiente, 6);

		require_once $this->render('index');
	}
	
}