<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2017
	Controller: sorte
*/

class Sorte_Controller extends Controller_Core {
	function __construct(){
		parent::__construct();
		// setanto os meta dados
		$this->meta_title = 'Gerador de Sorte';
		$this->meta_description = 'Ferramenta online que simula a sorte de um personagem de RPG de mesa.';
		$this->meta_keywords = 'RPG, RPG de mesa, Gerador de sorte';

		// [Voce pode passar arquivos css para a pagina do seu controller apenas 
		// informando o array como parametro de $this->set_base_css()]

		// chamando css em assets/css
		$this->css_files = $this->set_base_css(['init']);
		
		// chamando css interno dentro da view e concatenando ao css_files
		// $this->css_files .= $this->set_css(['index','home']);
		
		// [Voce pode passar arquivos javascript para ser chamado na view deste  
		// controller apenas passando um array com os nomes dos arquivos sem a 
		// extençao no array em $this->set_base_js]

		// chamada de arquivos js dentro de assets
		$this->js_files = $this->set_base_js(['index','teste']);
		// chamada de arquivos js dentro da veiw 
		// $this->js_files .= $this->set_js(['index','teste']);
	}

	public function index(){
		$publicidade = new Publicidade_Helper;
		$url = new Init_Model;
		$sorte = new Sorte_Model;
		$acontecimento = $sorte->acontecimento(3);
		$afetado = $sorte->afetado(3);

		$sorte_gerada = [
			'Quem poderá ser afetado' => $afetado,
			'O que pode acontecer' => $acontecimento
		];
		
		require_once $this->render('index');
	}
	
}