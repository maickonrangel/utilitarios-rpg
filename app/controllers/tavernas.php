<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2017
	Controller: tavernas
*/

class Tavernas_Controller extends Controller_Core {
	function __construct(){
		parent::__construct();
		// setanto os meta dados
		$this->meta_title = 'Gerador de tavernas';
		$this->meta_description = 'Ferramenta online que gera tavernas para jogos de RPG de mesa.';
		$this->meta_keywords = 'RPG, Tavernas, Gerador de tavernas, RPG de mesa';

		// [Voce pode passar arquivos css para a pagina do seu controller apenas 
		// informando o array como parametro de $this->set_base_css()]

		// chamando css em assets/css
		$this->css_files = $this->set_base_css(['init']);
		
		// chamando css interno dentro da view e concatenando ao css_files
		// $this->css_files .= $this->set_css(['index','home']);
		
		// [Voce pode passar arquivos javascript para ser chamado na view deste  
		// controller apenas passando um array com os nomes dos arquivos sem a 
		// extençao no array em $this->set_base_js]

		// chamada de arquivos js dentro de assets
		$this->js_files = $this->set_base_js(['index','teste']);
		// chamada de arquivos js dentro da veiw 
		// $this->js_files .= $this->set_js(['index','teste']);
	}

	public function index(){
		$publicidade = new Publicidade_Helper;
		$url = new Init_Model;
		$tavernas = [
			'medieval' => 'Medieval',
			'boteco' => 'Bar de boteco',
			'restaurante' => 'Restaurante moderno',
			'pub' => 'Pub'
		];
		
		require_once $this->render('index');
	}
	
	public function sortear($type){
		$publicidade = new Publicidade_Helper;
		$url = new Init_Model;
		$tavernas = ['Medieval','Bar de boteco','Restaurante moderno','Pub'];

		if (!in_array($type, $tavernas)) {
			$this->redirect('error');
		} else {
			if ($type == 'Medieval') {
				$taverna = new Tavernas_Model;
				$raca = $taverna->taverna_raca_taverneiro();
				$idade = $taverna->taverna_idade_taverneiro($raca);
				$sexo = ($taverna->sexo == 1)?'Feminino': 'Masculinmo';
				$taverna_dados = [
					'Nome' => $taverna->taverna_nome(),
					'Nome do taverneiro(a)' => $taverna->taverna_nome_taverneiro(),
					'Idade do taverneiro(a)' => $idade,
					'Sexo do taverneiro(a)' => $sexo,
					'Tempo de profissão do Taverneiro(a)' => $taverna->taverna_tempo_de_profissao($idade),
					'Aparência do taverneiro(a)' => $taverna->taverna_aparencia_taverneiro(mt_rand(3,5)),
					'Raça do Taverneiro(a)' => $raca,
					'Atrações do dia' => $taverna->taverna_atracao_medieval(mt_rand(1,3)),
					'Bebidas a venda' => $taverna->taverna_bebidas(mt_rand(3,6)),
					'Em caso de confusão pegue o que está perto' => $taverna->taverna_objetos_de_briga(mt_rand(1,3))
				];

			} elseif ($type == 'Bar de boteco') {
				$taverna = new Tavernas_Model;
				$sexo = ($taverna->sexo == 1)?'Feminino': 'Masculinmo';
				$taverna_dados = [
					'Nome' => $taverna->taverna_bar_nome(),
					'Nome do dono(a)' => $taverna->taverna_nome_taverneiro(),
					'Sexo do dono(a)' => $sexo,
					'Personalidade do dono(a)' => $taverna->taverna_personalidade_taverneiro(mt_rand(2,5)),
					'Atrações do dia' => $taverna->taverna_atracao_medieval(mt_rand(1,3)),
					'Bebidas a venda' => $taverna->taverna_bebidas_bar_ou_restaurante(mt_rand(3,6)),
					'Cervejas a venda' => $taverna->taverna_cervejas(mt_rand(3,6)),
					'Porções a venda' => $taverna->taverna_porcoes(mt_rand(3,6)),
					'Petiscos a venda' => $taverna->taverna_petiscos(mt_rand(3,6)),
					'Atrações do dia' => $taverna->taverna_artista(mt_rand(3,6))
				];
			} elseif ($type == 'Restaurante moderno') {
				$taverna = new Tavernas_Model;
				$sexo = ($taverna->sexo == 1)?'Feminino': 'Masculinmo';
				$taverna_dados = [
					'Nome' => $taverna->taverna_restaurante_nome(),
					'Nome do dono(a)' => $taverna->taverna_nome_taverneiro(),
					'Sexo do dono(a)' => $sexo,
					'Personalidade dos garçons(a)' => $taverna->taverna_garcon(mt_rand(2,5)),
					'Personalidade do dono(a)' => $taverna->taverna_personalidade_taverneiro(mt_rand(2,5)),
					'Sobremesas do dia' => $taverna->taverna_sobremesas(mt_rand(2,5)),
					'Pratos do dia' => $taverna->taverna_pratos(mt_rand(2,5)),
					'Pizzas do dia' => $taverna->taverna_restaurante_pizzas(mt_rand(2,5)),
					'Sucos' => $taverna->taverna_sucos(mt_rand(2,5)),
					'Atrações do dia' => $taverna->taverna_atracao_medieval(mt_rand(1,3)),
					'Bebidas a venda' => $taverna->taverna_bebidas_bar_ou_restaurante(mt_rand(3,6)),
					'Cervejas a venda' => $taverna->taverna_cervejas(mt_rand(3,6)),
					'Bebidas não alcólicas' => $taverna->taverna_bebidas_simples(mt_rand(3,6)),
					'Porções a venda' => $taverna->taverna_porcoes(mt_rand(3,6)),
					'Petiscos a venda' => $taverna->taverna_petiscos(mt_rand(3,6)),
					'Atrações do dia' => $taverna->taverna_atracao_conteporanea(mt_rand(3,6))
				];
			} elseif ($type == 'Pub') {
				$taverna = new Tavernas_Model;
				$sexo = ($taverna->sexo == 1)?'Feminino': 'Masculinmo';
				$taverna_dados = [
					'Nome' => $taverna->taverna_pub_nome(),
					'Nome do dono(a)' => $taverna->taverna_nome_taverneiro(),
					'Personalidade do dono(a)' => $taverna->taverna_personalidade_taverneiro(mt_rand(2,5)),
					'Sexo do dono(a)' => $sexo,
					'Atrações do dia' => $taverna->taverna_artista(mt_rand(1,3)),
					'Estilos musicais do dia' => $taverna->taverna_atracao_conteporanea(mt_rand(2,5)),
					'Bebidas a venda' => $taverna->taverna_bebidas_bar_ou_restaurante(mt_rand(3,8)),
					'Cervejas a venda' => $taverna->taverna_cervejas(mt_rand(3,8)),
					'Porções a venda' => $taverna->taverna_porcoes(mt_rand(3,6)),
					'Petiscos a venda' => $taverna->taverna_petiscos(mt_rand(3,6)),
					'Sucos a venda' => $taverna->taverna_sucos(mt_rand(3,6)),
					'Bebidas não alcólicas' => $taverna->taverna_bebidas_simples(mt_rand(3,6)),
				];
			}
			
			require_once $this->render('sortear');
		}
	}
}