<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2017
	Controller: personalidades
*/

class Personalidades_Controller extends Controller_Core {
	function __construct(){
		parent::__construct();
		// setanto os meta dados
		$this->meta_title = 'Gerador de personalidades para RPG de mesa.';
		$this->meta_description = 'Esta ferramenta online cria descrições de personagens para RPG de mesa.';
		$this->meta_keywords = 'RPG, personalidades, Personagens de RPG, RPG de mesa';

		// [Voce pode passar arquivos css para a pagina do seu controller apenas 
		// informando o array como parametro de $this->set_base_css()]

		// chamando css em assets/css
		$this->css_files = $this->set_base_css(['init']);
		
		// chamando css interno dentro da view e concatenando ao css_files
		// $this->css_files .= $this->set_css(['index','home']);
		
		// [Voce pode passar arquivos javascript para ser chamado na view deste  
		// controller apenas passando um array com os nomes dos arquivos sem a 
		// extençao no array em $this->set_base_js]

		// chamada de arquivos js dentro de assets
		$this->js_files = $this->set_base_js(['index','teste']);
		// chamada de arquivos js dentro da veiw 
		// $this->js_files .= $this->set_js(['index','teste']);
	}

	public function index(){
		$publicidade = new Publicidade_Helper;
		$url = new Init_Model;
		$geradores = ['aspectos'=>'Gerador de aspectos', 'descricao'=>'Gerador de descrição'];
		require_once $this->render('index');
	}

	public function sortear($type){
		$publicidade = new Publicidade_Helper;
		$url = new Init_Model;
		$geradores = ['Gerador de aspectos', 'Gerador de descrição'];
		if (!in_array($type, $geradores)) {
			$this->redirect('error');
		} else {
			if ($type == 'Gerador de aspectos') {
				$personalidade = new Personalidades_Model;
				$aspectos = [
					'Aspecto negativo' => $personalidade->aspecto_negativo(),
					'Aspecto positivo' => $personalidade->aspecto_positivo(),
					'Aspecto Geral' => $personalidade->aspecto_geral(),
					'Um medo' => $personalidade->aspecto_medo(),
					'Tendência' => $personalidade->aspecto_tendencia(),
					'Uma ideologia' => $personalidade->aspecto_ideologia(),
				];
				require_once $this->render('aspectos');
			} else {
				$personalidade = new Personalidades_Model;
				$descricoes = [
					'Boca' => $personalidade->forma_boca(),
					'Nariz' => $personalidade->forma_nariz(),
					'Olhos' => $personalidade->forma_olhos(),
					'Queixo' => $personalidade->forma_queixo(),
					'Rosto' => $personalidade->forma_rosto(),
					'Sobrancelha' => $personalidade->forma_sobrancelha(),
					'Testa' => $personalidade->forma_testa(),
					'Cor dos olhos' => $personalidade->olhos_cor(),
					'Cor da pele' => $personalidade->pele_cor(),
					'Estilo de cabelo' => $personalidade->cabelos_estilo(),
					'Cor do cabelo' => $personalidade->cabelos_cores()
				];
				require_once $this->render('descricao');
			}
		}
	}
}