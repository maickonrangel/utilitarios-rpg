<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2017
	Controller: formulariofichas
*/

class Formulariofichas_Controller extends Controller_Core {
	function __construct(){
		parent::__construct();
		// setanto os meta dados
		$this->meta_title = 'Fichas de RPG online';
		$this->meta_description = 'Cadastre suas fichas de RPG no Help RPG..';
		$this->meta_keywords = 'Fichas de RPG, Fichas, RPG, RPG de mesa';

		// [Voce pode passar arquivos css para a pagina do seu controller apenas 
		// informando o array como parametro de $this->set_base_css()]

		// chamando css em assets/css
		$this->css_files = $this->set_base_css(['init','ficha']);
		
		// chamando css interno dentro da view e concatenando ao css_files
		// $this->css_files .= $this->set_css(['index','home']);
		
		// [Voce pode passar arquivos javascript para ser chamado na view deste  
		// controller apenas passando um array com os nomes dos arquivos sem a 
		// extençao no array em $this->set_base_js]

		// chamada de arquivos js dentro de assets
		$this->js_files = $this->set_base_js([]);
		// chamada de arquivos js dentro da veiw 
		// $this->js_files .= $this->set_js(['index','teste']);
	}

	public function index(){
		$publicidade = new Publicidade_Helper;
		$url = new Init_Model;
		$ficha = new Formulariofichas_Model;
		require_once $this->render('index');
	}
	

	public function cadastrar($type = ''){
		$publicidade = new Publicidade_Helper;
		$url = new Init_Model;
		global $_QUERY;
		$acao = 'cadastro';
		if ($type == '') {
			$this->redirect('error');
		} else {
			$ficha = new Formulariofichas_Model;
			require_once $this->render('ficha');
		}
	}

	public function salvar(){
		if (isset($_REQUEST)) {
			$ficha = new Formulariofichas_Model;
			$ficha->ficha_to_xml($_REQUEST);
			$this->resgatar([$_REQUEST['chave'], $_REQUEST['sistema']]);
		} else {
			$this->redirect('error');
		}
	}

	public function atualizar(){
		if (isset($_REQUEST)) {
			$ficha = new Formulariofichas_Model;
			$ficha->ficha_to_xml($_REQUEST, 'atualizar');
			$this->resgatar([$_REQUEST['chave'], $_REQUEST['sistema']]);
		} else {
			$this->redirect('error');
		}
	}

	// public function deletar(){
	// 	if (isset($_REQUEST)) {
	// 		$ficha = new Formulariofichas_Model;
	// 		$ficha->deletar_ficha($_REQUEST['chave'], $_REQUEST['sistema']);
	// 	} else {
	// 		$this->redirect('error');
	// 	}
	// }

	public function show(){
		$publicidade = new Publicidade_Helper;
		$url = new Init_Model;
		$ficha = new Formulariofichas_Model;
		require_once $this->render('show');
	}

	public function resgatar($params = ''){
		$ficha = new Formulariofichas_Model;
		$acao = 'atualizar';
		if (isset($_REQUEST) and count($_REQUEST) > 1) {
			$type = $_REQUEST['sistema'];
			$ficha_xml = $ficha->get_ficha($_REQUEST['chave'], $_REQUEST['sistema']);
			if (!is_object($ficha_xml)) {
				$this->redirect('formulariofichas/cadastrar/'.$_REQUEST['sistema'].'?erro=1');
			} else {
				global $_QUERY;
				require_once $this->render('ficha');
			}
		} elseif ($params != '') {
			$chave = $params[0];
			$type = $params[1];
			$ficha_xml = $ficha->get_ficha($chave, $type);
			if (!is_object($ficha_xml)) {
				$this->redirect('formulariofichas/cadastrar/'.$_REQUEST['sistema'].'?erro=1');
			} else {
				global $_QUERY;
				require_once $this->render('ficha');
			}
		} else {
			$this->redirect('formulariofichas/cadastrar/'.$_REQUEST['sistema'].'?erro=1');
		}
	}


}