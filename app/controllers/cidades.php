<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2017
	Controller: cidades
*/

class Cidades_Controller extends Controller_Core {
	function __construct(){
		parent::__construct();
		// setanto os meta dados
		$this->meta_title = 'Gerador de cidades';
		$this->meta_description = 'Ferramenta online que gera descrição de cidades para RPG de mesa.';
		$this->meta_keywords = 'RPG, Cidade, Cidades RPG, RPG de mesa, Utilitários, Gerador de cidades';

		// [Voce pode passar arquivos css para a pagina do seu controller apenas 
		// informando o array como parametro de $this->set_base_css()]

		// chamando css em assets/css
		$this->css_files = $this->set_base_css(['init']);
		
		// chamando css interno dentro da view e concatenando ao css_files
		// $this->css_files .= $this->set_css(['index','home']);
		
		// [Voce pode passar arquivos javascript para ser chamado na view deste  
		// controller apenas passando um array com os nomes dos arquivos sem a 
		// extençao no array em $this->set_base_js]

		// chamada de arquivos js dentro de assets
		$this->js_files = $this->set_base_js(['index','teste']);
		// chamada de arquivos js dentro da veiw 
		// $this->js_files .= $this->set_js(['index','teste']);
	}

	public function index(){
		$publicidade = new Publicidade_Helper;
		$url = new Init_Model;
		$cidade = new Cidades_Model;
		$cidade->poder_central();
		$cidade_sorteada = [
			'Nome' => $cidade->nome(),
			'Tamanho' => $cidade->tamanho,
			'Composicao racial' => $cidade->composicao_racial(),
			'Tendencia do poder central' => $cidade->tendencia_do_poder_central(),
			'Tipo de defesa' => $cidade->tipo_de_defesa(),
			'Inporta' => $cidade->inporta(mt_rand(1,3)),
			'Exporta' => $cidade->exporta(mt_rand(1,3)),
			'Principais fontes de renda' => $cidade->fonte_de_renda(mt_rand(1,3)),
			'Tipo de religião' => $cidade->tipo_de_religiao(),
			'Cultura inclinada para' => $cidade->tipo_de_cultura(),
			'População' => $cidade->populacao(),
			'Limite em PO' => $cidade->limite_po(),
			'Tipo de poder central' => $cidade->tipo_poder_central_escolhido
		];

		require_once $this->render('index');
	}
	
}