<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2017
	Controller: dados
*/

class Dados_Controller extends Controller_Core {
	function __construct(){
		parent::__construct();
		// setanto os meta dados
		$this->meta_title = 'Rolador de dados - utilitários';
		$this->meta_description = 'Rolador de dados - Help RPG';
		$this->meta_keywords = 'RPG, RPG de mesa, rolador de dados, d4, d6, d8, d10, d12, d100, rolar dados, dados.';

		// [Voce pode passar arquivos css para a pagina do seu controller apenas 
		// informando o array como parametro de $this->set_base_css()]

		// chamando css em assets/css
		$this->css_files = $this->set_base_css(['init']);
		
		// chamando css interno dentro da view e concatenando ao css_files
		// $this->css_files .= $this->set_css(['index','home']);
		
		// [Voce pode passar arquivos javascript para ser chamado na view deste  
		// controller apenas passando um array com os nomes dos arquivos sem a 
		// extençao no array em $this->set_base_js]

		// chamada de arquivos js dentro de assets
		$this->js_files = $this->set_base_js(['index']);
		// chamada de arquivos js dentro da veiw 
		// $this->js_files .= $this->set_js(['index','teste']);
	}

	public function index(){
		$publicidade = new Publicidade_Helper;
		$url = new Init_Model;
		$dados_img = ['d4','d6','d8','d10','d12','d20','d100'];
		$dados = new Dados_Model;
		require_once $this->render('index');
	}
	
	public function rolar($dado = ''){
		$publicidade = new Publicidade_Helper;
		$url = new Init_Model;
		$dados_img = ['d4','d6','d8','d10','d12','d20','d100'];
		if (isset($_REQUEST['qtd']) and $dado != '') {
			$resultados = [];
			for ($i=0; $i < $_REQUEST['qtd']; $i++) { 
				$dados = new Dados_Model;
				$resultados[] = $dados->$dado;
			}
			$total = 0;
			$maior = 0;
			$menor = 9999999;
			$media = 0;

			foreach ($resultados as $key => $value) {
				$total += $value;
				if ($value > $maior) {
					$maior = $value;
				}

				if ($value < $menor) {
					$menor = $value;
				}
			}

			$total_menor_desc = ($total-$menor);
			$media = intval($total/$_REQUEST['qtd']);
			require_once $this->render('index');

		} elseif (is_array($dado)){
			$resultados = [];
			$qtd = $dado[0];
			$dado = $dado[1];
			for ($i=0; $i < $qtd; $i++) { 
				$dados = new Dados_Model;
				$resultados[] = $dados->$dado;
			}
			$total = 0;
			$maior = 0;
			$menor = 9999999;
			$media = 0;

			foreach ($resultados as $key => $value) {
				$total += $value;
				if ($value > $maior) {
					$maior = $value;
				}

				if ($value < $menor) {
					$menor = $value;
				}
			}

			$total_menor_desc = ($total-$menor);
			$media = intval($total/$qtd);
			require_once $this->render('index');
		} else {
			$this->redirect('error');
		}
	}
}