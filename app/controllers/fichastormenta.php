<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2017
	Controller: fichastormenta
*/

class Fichastormenta_Controller extends Controller_Core {
	function __construct(){
		parent::__construct();
		// setanto os meta dados
		$this->meta_title = 'Gerador de Fichas';
		$this->meta_description = 'Gerador de fichas para Tormenta RPG.';
		$this->meta_keywords = 'RPG, Utilitários, Gerador de fichas, Tormenta RPG, RPG de mesa, Fichas de RPG';

		// [Voce pode passar arquivos css para a pagina do seu controller apenas 
		// informando o array como parametro de $this->set_base_css()]

		// chamando css em assets/css
		$this->css_files = $this->set_base_css(['init']);
		
		// chamando css interno dentro da view e concatenando ao css_files
		// $this->css_files .= $this->set_css(['index','home']);
		
		// [Voce pode passar arquivos javascript para ser chamado na view deste  
		// controller apenas passando um array com os nomes dos arquivos sem a 
		// extençao no array em $this->set_base_js]

		// chamada de arquivos js dentro de assets
		$this->js_files = $this->set_base_js(['jquery','html2canvas.min','card','export']);
		// chamada de arquivos js dentro da veiw 
		// $this->js_files .= $this->set_js(['index','teste']);
	}

	public function index(){
		$publicidade = new Publicidade_Helper;
		$url = new Init_Model;
		if (isset($_REQUEST['classe']) || isset($_REQUEST['raca']) || isset($_REQUEST['nivel']) || isset($_REQUEST['tipo_rolagem'])) {
			if ($_REQUEST['classe'] == '') {
				$_REQUEST['classe'] = null;
			}

			if ($_REQUEST['raca'] == '') {
				$_REQUEST['raca'] = null;
			}

			if ($_REQUEST['nivel'] == '') {
				$_REQUEST['nivel'] = mt_rand(1,20);
			}

			if ($_REQUEST['tipo_rolagem'] == '') {
				$_REQUEST['tipo_rolagem'] = 'padrao';
			}

			$f = new Fichastormenta_Model($_REQUEST['nivel'], $_REQUEST['classe'], $_REQUEST['raca'], $_REQUEST['tipo_rolagem']);
		} else{
			$f = new Fichastormenta_Model;
		}
		require_once $this->render('index');
	}
	
}