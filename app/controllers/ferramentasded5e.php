<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2017
	Controller: ferramentasded5e
*/

class Ferramentasded5e_Controller extends Controller_Core {

	public $ferramentas;

	function __construct(){
		parent::__construct();
		// setanto os meta dados
		$this->meta_title = 'Utilitários para D&D 5e';
		$this->meta_description = 'Conjunto de utilitários para o sistema Dungeons and Dragons 5.0';
		$this->meta_keywords = 'RPG, D&D 5e, Dungeons and Dragons 5.0, RPG de mesa, Utilitários';

		// [Voce pode passar arquivos css para a pagina do seu controller apenas 
		// informando o array como parametro de $this->set_base_css()]

		// chamando css em assets/css
		$this->css_files = $this->set_base_css(['init']);
		
		// chamando css interno dentro da view e concatenando ao css_files
		// $this->css_files .= $this->set_css(['index','home']);
		
		// [Voce pode passar arquivos javascript para ser chamado na view deste  
		// controller apenas passando um array com os nomes dos arquivos sem a 
		// extençao no array em $this->set_base_js]

		// chamada de arquivos js dentro de assets
		$this->js_files = $this->set_base_js(['index','teste']);
		// chamada de arquivos js dentro da veiw 
		// $this->js_files .= $this->set_js(['index','teste']);
		$this->ferramentas = [
			'acao_de_vilao_baseado_em_eventos',
			'aliados',
			'aventuras_em_eventos_fechados',
			'climax_da_aventura',
			'dilemas_morais',
			'encontros_em_florestas_silvestres',
			'eventos',
			'introducao_de_aventura',
			'missoes_secundarias',
			'objetivos_ambiente_selvagem',
			'objetivos_baseado_em_eventos',
			'objetivos_na_masmorra',
			'outros_objetivos',
			'patronos_da_aventura',
			'viloes_da_aventura',
			'farreando',
			'complicacao_perseguicao_urbana',
			'complicacao_perseguicao_na_natureza',
			'venenos',
			'ferimento_persistente'
		];

		$this->labels = [
			'acao_de_vilao_baseado_em_eventos' => 'Uma ação para o vilão baseado num evento',
			'aliados' => 'Um possível aliado na sua aventura',
			'aventuras_em_eventos_fechados' => 'Uma aventura baseada num evento fechado',
			'climax_da_aventura' => 'Um climax para a sua aventura',
			'dilemas_morais' => 'Um dilema moral',
			'encontros_em_florestas_silvestres' => 'Um encontro dentro de uma floresta silvestre',
			'eventos' => 'Um evento na sua aventura',
			'introducao_de_aventura' => 'Uma introdução para a sua aventura',
			'missoes_secundarias' => 'Uma missão secundária',
			'objetivos_ambiente_selvagem' => 'Um objetivo em um ambiente selvagem',
			'objetivos_baseado_em_eventos' => 'Um objetivo baseado em um evento',
			'objetivos_na_masmorra' => 'Um objetivo para uma masmorra',
			'outros_objetivos' => 'Um objetivo geral',
			'patronos_da_aventura' => 'Um patrono para a aventura',
			'viloes_da_aventura' => 'Um vilão para a aventura',
			'farreando' => 'Você saiu para uma noitada e no final...',
			'complicacao_perseguicao_urbana' => 'Complicacão e perseguicão em ambiente urbano',
			'complicacao_perseguicao_na_natureza' => 'Complicacão e perseguicão em ambiente na natureza',
			'venenos' => 'Um veneno',
			'ferimento_persistente' => 'Opção para ferimentos de longo prazo num personagem',
		];
			
	}

	public function index(){
		$publicidade = new Publicidade_Helper;
		$url = new Init_Model;
		$ferramentas = $this->ferramentas;
		require_once $this->render('index');
	}
	
	public function sortear($type){
		$url = new Init_Model;
		$publicidade = new Publicidade_Helper;
		$ferramentas = $this->ferramentas;
		if (!in_array($type, $ferramentas)) {
			$this->redirect('error');
		} else {
			$ferramentasded5e = new ferramentasded5e_Model;
			$metodo = str_replace(' ', '_', $type);
			$label = $this->labels[$metodo];
			$sorteio = $ferramentasded5e->$metodo();
			require_once $this->render('sortear');
		}
	}
}