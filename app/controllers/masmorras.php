<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2017
	Controller: masmorras
*/

class Masmorras_Controller extends Controller_Core {
	function __construct(){
		parent::__construct();
		// setanto os meta dados
		$this->meta_title = 'Gerador de masmorras';
		$this->meta_description = 'Ferramenta online que ajuda mestres a criarem masmorras para aventuras de RPG de mesa..';
		$this->meta_keywords = 'Masmorras RPG, RPG, RPG de mesa, Masmorras para D&D 5e, D&D';

		// [Voce pode passar arquivos css para a pagina do seu controller apenas 
		// informando o array como parametro de $this->set_base_css()]

		// chamando css em assets/css
		$this->css_files = $this->set_base_css(['init']);
		
		// chamando css interno dentro da view e concatenando ao css_files
		// $this->css_files .= $this->set_css(['index','home']);
		
		// [Voce pode passar arquivos javascript para ser chamado na view deste  
		// controller apenas passando um array com os nomes dos arquivos sem a 
		// extençao no array em $this->set_base_js]

		// chamada de arquivos js dentro de assets
		$this->js_files = $this->set_base_js(['index','teste']);
		// chamada de arquivos js dentro da veiw 
		// $this->js_files .= $this->set_js(['index','teste']);
	}

	public function index(){
		$url = new Init_Model;
		$publicidade = new Publicidade_Helper;
		$masmorra = new Masmorras_Model;
		$masmorra_sorteio = [
			'Classe dos PdMs que estão na masmorra' => $masmorra->classe_do_pdm(mt_rand(1,3)),
			'Cultos e grupos religiosos na masmorra' => $masmorra->cultos_ou_grupos_religiosos_na_masmorra(mt_rand(1,3)),
			'História da masmorra' => $masmorra->historia_da_masmorra(),
			'Criador da masmorra' => $masmorra->criador_da_masmorra(),
			'Localização da masmorra' => $masmorra->localizacao_da_masmorra(),
			'Localização exótica da masmorra' => $masmorra->localizacao_exotica_da_masmorra(),
			'Prpósito da masmorra' => $masmorra->proposito_da_masmorra(),
			'Tendência dos PdMs envolvidos na masmorra' => $masmorra->tendencia_do_pdm(mt_rand(1,3))
		];

		require_once $this->render('index');
	}
	
}