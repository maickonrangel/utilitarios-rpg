<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2017
	Controller: encontros
*/

class Encontros_Controller extends Controller_Core {
	function __construct(){
		parent::__construct();
		// setanto os meta dados
		$this->meta_title = 'Gerador de Encontros Aleatórios';
		$this->meta_description = 'Gerador de encontro aleatório para RPG baseado em tipo de ambiente.';
		$this->meta_keywords = 'RPG, Utilitário, Gerador de Encontros, RPG de mesa, Encontro aleatório RPG';

		// [Voce pode passar arquivos css para a pagina do seu controller apenas 
		// informando o array como parametro de $this->set_base_css()]

		// chamando css em assets/css
		$this->css_files = $this->set_base_css(['init']);
		
		// chamando css interno dentro da view e concatenando ao css_files
		// $this->css_files .= $this->set_css(['index','home']);
		
		// [Voce pode passar arquivos javascript para ser chamado na view deste  
		// controller apenas passando um array com os nomes dos arquivos sem a 
		// extençao no array em $this->set_base_js]

		// chamada de arquivos js dentro de assets
		$this->js_files = $this->set_base_js(['index','teste']);
		// chamada de arquivos js dentro da veiw 
		// $this->js_files .= $this->set_js(['index','teste']);
	}

	public function index(){
		$publicidade = new Publicidade_Helper;
		$url = new Init_Model;
		$encontros = new Encontros_Model;
		if (isset($_REQUEST['ambiente']) and $_REQUEST['ambiente'] != '') {
			$ambiente = $_REQUEST['ambiente'];
		} else {
			$ambiente = array_rand($encontros->ambientes);
		}

		$encontro = $encontros->sortear_encontro($ambiente);
		require_once $this->render('index');
	}
	
}