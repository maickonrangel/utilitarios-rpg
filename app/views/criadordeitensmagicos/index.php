<!--  
	Arquivo gerado pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2017
	View: criadordeitensmagicos
-->
<div class="label-layout">
   	<h2 class="title emfont">
      	<a class="label-name" href="#" title="Criador de itens mágicos para RPG">Criador de itens mágicos</a>
      	<a class="label-target" href="<?php echo URL_BASE; ?>">Voltar</a>
    	<div class="clear"></div>
   	</h2>
   	<div class="clear"></div>
   	<div class="content">
   		<div class="item item-1 item-other">
        	<div class="inner">
            	<?php $item->pocao(); ?>
         	</div>
      	</div>
    	<div class="clear"></div>
	</div>
</div>