<?php

class Tormentarpgitens_Model {

	public $file_path;

	function __construct(){
		$this->file_path = PATH_BASE.'config/locale/pt-br/txt/tormenta_rpg/';
		$this->itens = [
			'itens_diversos' => 'Itens Diversos',
			'gemas' => 'Gemas',
			'obras_de_arte' => 'Obras de Arte',
			'armaduras' => 'Armaduras Específicas',
			'armas' => 'Armas Específicas',
			'cajados' => 'Cajados Mágicos',
			'escudos' => 'Escudos Específicos',
			'armas_magicas' => 'Armas Mágicas',
			'armas_magicas_de_ataque_a_distancia' => 'Armas Mágicas de ataque a distância',
			'escudos_magicos' => 'Escudos Mágicos',
			'armaduras_magicas' => 'Armaduras Mágicas',
			'pocoes' => 'Poções',
			'varinhas' => 'Varinhas Mágicas',
			'acessorio_menor' => 'Acessório Menor',
			'acessorio_medio' => 'Acessório Médio',
			'acessorio_maior' => 'Acessório maior',
			'todos_acessorio' => 'Todos os acessórios (menor, médio e maior)',
			'pergaminho_arcano1' => 'Pergaminho arcano de 1° círculo',
			'pergaminho_arcano2' => 'Pergaminho arcano de 2° círculo',
			'pergaminho_arcano3' => 'Pergaminho arcano de 3° círculo',
			'pergaminho_divino1' => 'Pergaminho divino de 1° círculo',
			'pergaminho_divino2' => 'Pergaminho divino de 2° círculo',
			'pergaminho_divino3' => 'Pergaminho divino de 3° círculo',
			'todos_os_pergaminhos' => 'Todos os pergaminhos sorteados',
			'todos_os_itens' => 'armas, armaduras, cajados, escudos, poções, varinhas e acessórios menores, médio e maiores juntos.'
		];
	}

	function sortear_item($nome, $qtd=2){
		if(substr($nome, 0, 17) == 'pergaminho_arcano') {
			$circulo = substr($nome, 17,18);
			$file = substr($nome, 0, 17);
			$itens = explode("\n", file_get_contents($this->file_path.'itens_magicos/'.$file.'/'.$circulo.'.txt'));
		} elseif (substr($nome, 0, 17) == 'pergaminho_divino') {
			$circulo = substr($nome, 17,18);
			$file = substr($nome, 0, 17);
			$itens = explode("\n", file_get_contents($this->file_path.'itens_magicos/'.$file.'/'.$circulo.'.txt'));
		} else {
			$itens = explode("\n", file_get_contents($this->file_path.'itens_magicos/'.$nome.'.txt'));
		}
		$indices = array_rand($itens, $qtd);
		$items_sorteadas = [];
		foreach ($indices as $key => $value) {
			$itens_sorteados[] = $itens[$value];	
		}

		foreach ($itens_sorteados as $key => $value) {
			$nome = explode('|',$value)[0];
			$preco = explode('|',$value)[1];
			$items_finais[] = ['nome' => $nome, 'preco' => $preco];
		}

		shuffle($items_finais);

		return $items_finais;
	}

	public function sortear_todos_pergaminhos(){
		$perg_arca_1 = explode("\n", file_get_contents($this->file_path.'itens_magicos/pergaminho_arcano/1.txt'));
		$perg_arca_comp_1 = [];
		foreach ($perg_arca_1 as $key => $value) {
			$perg_arca_comp_1[] = $value."|1";
		}

		$perg_arca_2 = explode("\n", file_get_contents($this->file_path.'itens_magicos/pergaminho_arcano/2.txt'));
		$perg_arca_comp_2 = [];
		foreach ($perg_arca_2 as $key => $value) {
			$perg_arca_comp_2[] = $value."|2";
		}
		
		$perg_arca_3 = explode("\n", file_get_contents($this->file_path.'itens_magicos/pergaminho_arcano/3.txt'));
		$perg_arca_comp_3 = [];
		foreach ($perg_arca_3 as $key => $value) {
			$perg_arca_comp_3[] = $value."|2";
		}

		$perg_arc = array_merge($perg_arca_comp_1, $perg_arca_comp_2, $perg_arca_comp_3);
		$pergaminhos = [];

		foreach ($perg_arc as $key => $value) {
			$nome = explode('|',$value)[0];
			$preco = explode('|',$value)[1];
			$circulo = explode('|',$value)[2];
			$pergaminhos[] = ['nome' => $nome, 'preco' => $preco, 'tipo' => 'pergaminho_arcano', 'circulo' => $circulo];
		}

		$perg_div_1 = explode("\n", file_get_contents($this->file_path.'itens_magicos/pergaminho_divino/1.txt'));
		$perg_div_comp_1 = [];
		foreach ($perg_div_1 as $key => $value) {
			$perg_div_comp_1[] = $value."|1";
		}

		$perg_div_2 = explode("\n", file_get_contents($this->file_path.'itens_magicos/pergaminho_divino/2.txt'));
		$perg_div_comp_2 = [];
		foreach ($perg_div_2 as $key => $value) {
			$perg_div_comp_2[] = $value."|2";
		}

		$perg_div_3 = explode("\n", file_get_contents($this->file_path.'itens_magicos/pergaminho_divino/3.txt'));
		$perg_div_comp_3 = [];
		foreach ($perg_div_3 as $key => $value) {
			$perg_div_comp_3[] = $value."|3";
		}

		$perg_div = array_merge($perg_div_comp_1, $perg_div_comp_2, $perg_div_comp_3);

		foreach ($perg_div as $key => $value) {
			$nome = explode('|',$value)[0];
			$preco = explode('|',$value)[1];
			$circulo = explode('|',$value)[2];
			$pergaminhos[] = ['nome' => $nome, 'preco' => $preco, 'tipo' => 'pergaminho_divino', 'circulo' => $circulo];
		}

		$sorteados = array_rand($pergaminhos, 10);
		$pergaminhos_sorteados = [];

		foreach ($sorteados as $key => $value) {
			$pergaminhos_sorteados[] = $pergaminhos[$value];
		}

		shuffle($pergaminhos_sorteados);

		return $pergaminhos_sorteados;
	}

	public function sortear_todos_itens(){
		$armaduras = explode("\n", file_get_contents($this->file_path.'itens_magicos/armaduras.txt'));
		$armas = explode("\n", file_get_contents($this->file_path.'itens_magicos/armas.txt'));
		$cajados = explode("\n", file_get_contents($this->file_path.'itens_magicos/cajados.txt'));
		$escudos = explode("\n", file_get_contents($this->file_path.'itens_magicos/escudos.txt'));
		$pocoes = explode("\n", file_get_contents($this->file_path.'itens_magicos/pocoes.txt'));
		$varinhas = explode("\n", file_get_contents($this->file_path.'itens_magicos/varinhas.txt'));
		$acessorio_menor = explode("\n", file_get_contents($this->file_path.'itens_magicos/acessorio_menor.txt'));
		$acessorio_medio = explode("\n", file_get_contents($this->file_path.'itens_magicos/acessorio_medio.txt'));
		$acessorio_maior = explode("\n", file_get_contents($this->file_path.'itens_magicos/acessorio_maior.txt'));

		$itens = array_merge($armaduras, $armas, $cajados, $escudos, $pocoes, $varinhas, $acessorio_menor, $acessorio_medio, $acessorio_maior);
		$todos_os_itens = [];

		foreach ($itens as $key => $value) {
			$nome = explode('|',$value)[0];
			$preco = explode('|',$value)[1];
			$todos_os_itens[] = ['nome' => $nome, 'preco' => $preco];
		}

		$sorteados = array_rand($todos_os_itens, 10);
		$todos_os_itens_sorteados = [];

		foreach ($sorteados as $key => $value) {
			$todos_os_itens_sorteados[] = $todos_os_itens[$value];
		}

		shuffle($todos_os_itens_sorteados);

		return $todos_os_itens_sorteados;
	}

	public function sortear_todos_acessorios(){
		$acessorio_menor = explode("\n", file_get_contents($this->file_path.'itens_magicos/acessorio_menor.txt'));
		$acessorio_medio = explode("\n", file_get_contents($this->file_path.'itens_magicos/acessorio_medio.txt'));
		$acessorio_maior = explode("\n", file_get_contents($this->file_path.'itens_magicos/acessorio_maior.txt'));

		$itens = array_merge($acessorio_menor, $acessorio_medio, $acessorio_maior);
		$todos_os_itens = [];

		foreach ($itens as $key => $value) {
			$nome = explode('|',$value)[0];
			$preco = explode('|',$value)[1];
			$todos_os_itens[] = ['nome' => $nome, 'preco' => $preco];
		}

		$sorteados = array_rand($todos_os_itens, 10);
		$todos_os_itens_sorteados = [];

		foreach ($sorteados as $key => $value) {
			$todos_os_itens_sorteados[] = $todos_os_itens[$value];
		}

		shuffle($todos_os_itens_sorteados);

		return $todos_os_itens_sorteados;
	}

	public function itens_magicos($nome, $poder) {
		$armas = explode("\n", file_get_contents($this->file_path.'itens_magicos/'.$nome.'.txt'));
		$poderes = explode("\n", file_get_contents($this->file_path.'itens_magicos/'.$poder.'.txt'));

		foreach ($poderes as $key => $value) {
			$habilidade = explode('|',$value)[0];
			$preco = explode('|',$value)[1];
			$todos_os_itens[] = ['habilidade' => $habilidade, 'preco' => $preco];
		}

		$sorteados = array_rand($todos_os_itens, 10);
		$armas_magicas_sorteadas = [];

		foreach ($sorteados as $key => $value) {
			$todos_os_itens[$value]['nome'] = $armas[array_rand($armas)];
			$armas_magicas_sorteadas[] = $todos_os_itens[$value];
		}

		shuffle($armas_magicas_sorteadas);

		return $armas_magicas_sorteadas;
	}

	public function objetos($tipo){
		$objetos = explode("\n", file_get_contents($this->file_path.'itens_magicos/'.$tipo.'.txt'));
		$objetos_sorteio = [];
		foreach ($objetos as $key => $value) {
			$gema_preco = explode('|', $value)[0];
			$gema_preco_base = explode(' ', explode('x', $gema_preco)[1])[0];
			$gema_dado = explode('d', explode('x', $gema_preco)[0])[1];
			$gema_qtd = explode('d',$gema_preco_base)[0];
			$preco_final = 0;
			for ($i=0; $i < $gema_qtd; $i++) { 
				$preco_final += mt_rand(1, $gema_dado);
			}

			$gema_desc = explode('|', $value)[1];

			$objetos_sorteio[] = ['preco' => $preco_final.' TO', 'nome' => $gema_desc];
		}

		$sorteados = array_rand($objetos_sorteio, 4);
		$objetos_sorteadas = [];

		foreach ($sorteados as $key => $value) {
			$objetos_sorteadas[] = $objetos_sorteio[$value];
		}

		shuffle($objetos_sorteadas);

		return $objetos_sorteadas;
	}
}