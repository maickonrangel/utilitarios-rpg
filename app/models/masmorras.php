<?php

class Masmorras_Model {
	
	public $file_path;

	function __construct(){
		$this->file_path = PATH_BASE.'config/locale/pt-br/txt/livro_do_mestre/masmorra/';
	}

	public function classe_do_pdm($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'classe_do_pdm.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	public function criador_da_masmorra($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'criador_da_masmorra.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	public function cultos_ou_grupos_religiosos_na_masmorra($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'cultos_ou_grupos_religiosos_na_masmorra.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	public function historia_da_masmorra($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'historia_da_masmorra.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	public function localizacao_da_masmorra($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'localizacao_da_masmorra.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	public function localizacao_exotica_da_masmorra($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'localizacao_exotica_da_masmorra.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	public function proposito_da_masmorra($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'proposito_da_masmorra.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	public function tendencia_do_pdm($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'tendencia_do_pdm.txt', $qtd, 'FILE'))->getRaffleItens();
	}
}