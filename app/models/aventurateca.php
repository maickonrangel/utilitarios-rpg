<?php
// Help RPG 2016
// @author Maickon Rangel
// Classe de modelo gerada no automatico

class Aventurateca_Model {

	public function str_like_analyser($str){
		$str_db = $this->str_like_db();
		if (array_key_exists($str, $str_db)) {
			$explode_str = explode(' ', $str);
			foreach ($str_db as $key => $value) {
				foreach ($explode_str as $ex_key => $ex_value) {
					if ($ex_value == $key) {
						$explode_str[$ex_key] = $value;
					}
				}
			}
			return implode(' ', $explode_str);
		} else {
			return $str;
		}
	}

	public function str_like_db(){
		return [
			'imagem' => 'imagens',
			'canal' => 'canais',
			'item' => 'itens',
			'site' => 'html',
			'áudio' => 'audio',
			'vídeo' => 'video',
			'executavel' => 'exe',
			'executável' => 'exe',
			'aplicativo' => 'apk',
			'hostórias' => 'historias',
			'hostória' => 'historias'
		];
	}

	public function linkar_hashtag($str){
		$hashtags = explode('#', $str);
		unset($hashtags[0]);
		$novas_hashtags = '';
		foreach ($hashtags as $key => $value) {
			$hash = (strtolower($value));
			$novas_hashtags .= "<a href=\"".URL_BASE."aventurateca/pesquisar_hashtag/{$hash}\">#{$value}</a> ";
		}
		return $novas_hashtags;
	}

	public function pesquisar($helper, $path = 'pesquisar'){
		$form = '
		<form action="'.URL_BASE.'aventurateca/'.$path.'" method="post">
	    	<div class="item-pesquisa" style="width:280px;">
			    <select name="categoria">
			    	<option value="todos">Selecione...</option>
			    	<option value="todos">Todos</option>';
				    foreach ($helper->tipos_form as $key => $value):
			    		$form .= '<option value="'.$key.'">'.$value.'</option>';
				    endforeach;
			    $form .= '</select>
	    	</div>
	    	<div class="item-pesquisa" style="width:280px;">
				<input list="sistema_rpg" name="sistema_rpg" placeholder="Informe um sistema de RPG associado" style="height:30px; width:280px;">
				<datalist id="sistema_rpg">';
				     foreach ($helper->select_classes() as $key => $value):
				    		$form .= '<option value="'.$value.'">'.$value.'</option>';
					    endforeach;
				 $form .= '</datalist>
	    	</div>
	    	<div class="item-pesquisa" style="width:280px";>
				<input id="pesquisar" name="pesquisar" placeholder="Pesquisar..." type="text" value="">
	    	</div>
	    	<div class="item-btn-pesquisa">
				<input type="submit" class="btn-pesquisa" name="Pesquisar" value="Pesquisar">
	    	</div>
		</form>';

		echo $form;
	}
}