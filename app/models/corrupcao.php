<?php

class Corrupcao_Model {

	public $file_path;

	function __construct(){
		$this->file_path = PATH_BASE.'config/locale/pt-br/txt/corrupcoes/';
	}

	function sortear($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'corrupcoes.txt', $qtd, 'FILE'))->getRaffleItens();
	}
}