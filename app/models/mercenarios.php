<?php

class Mercenarios_Model {

	public $file_path;

	function __construct(){
		$this->file_path = PATH_BASE.'config/locale/pt-br/txt/mercenarios/trabalhos-pagamentos.txt';
	}

	public function sortear_trabalho(){
		$file = file_get_contents($this->file_path);
		$tarefas_de_mercenario = explode("\n", $file);
		$tarefa_escolhida = $tarefas_de_mercenario[array_rand($tarefas_de_mercenario)];
		$descricao_tarefa = explode('|', $tarefa_escolhida);
		$trabalho = [
			'Objetivo' => $descricao_tarefa[1],
			'Tempo' => $descricao_tarefa[0],
			'Recompensa' => $descricao_tarefa[2]
		];

		return $trabalho;
	}
}
