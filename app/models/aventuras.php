
<?php
	// Help RPG 2016
	// @author Maickon Rangel
	// Classe de modelo gerada no automatico

	class Aventuras_Model {

		public $antagonista;
		public $coadjuvante;
		public $complicacao;
		public $localidade;
		public $objetivo;
		public $recompensa;

		public $tipo_aventura;
		public $file_path;

		function __construct(){
			$this->file_path = PATH_BASE.'config/locale/pt-br/txt/aventuras/';
		}

		public function generate_adventure($type){
			$this->antagonista 	= new Raffleitemfile_Core($this->file_path.$type.'/antagonista.txt', 1);
			$this->coadjuvante 	= new Raffleitemfile_Core($this->file_path.$type.'/coadjuvante.txt', 1);
			$this->complicacao 	= new Raffleitemfile_Core($this->file_path.$type.'/complicacao.txt', 1);
			$this->localidade 	= new Raffleitemfile_Core($this->file_path.$type.'/localidade.txt', 1);
			$this->objetivo 	= new Raffleitemfile_Core($this->file_path.$type.'/objetivo.txt', 1);
			$this->recompensa 	= new Raffleitemfile_Core($this->file_path.$type.'/recompensa.txt', 1);
			$this->type = $type;
		}
	}