<?php
class Personalidades_Model {
	
	public $file_path;

	function __construct(){
		$this->file_path = PATH_BASE.'config/locale/pt-br/txt/personalidades/';
	}

	function aspecto_negativo(){
		return (new Raffleitemfile_Core($this->file_path.'aspectos_negativos.txt', 1, 'FILE'))->getRaffleItens();
	}

	function aspecto_positivo(){
		return (new Raffleitemfile_Core($this->file_path.'aspectos_positivos.txt', 1, 'FILE'))->getRaffleItens();
	}

	function aspecto_geral(){
		return (new Raffleitemfile_Core($this->file_path.'caracteristicas_gerais.txt', 1, 'FILE'))->getRaffleItens();
	}

	function aspecto_ideologia(){
		return (new Raffleitemfile_Core($this->file_path.'ideologias.txt', 1, 'FILE'))->getRaffleItens();
	}

	function aspecto_medo(){
		return (new Raffleitemfile_Core($this->file_path.'medos.txt', 1, 'FILE'))->getRaffleItens();
	}

	function aspecto_tendencia(){
		return (new Raffleitemfile_Core($this->file_path.'tendencia.txt', 1, 'FILE'))->getRaffleItens();
	}

	//--

	function forma_boca(){
		return (new Raffleitemfile_Core($this->file_path.'forma_boca.txt', 1, 'FILE'))->getRaffleItens();
	}

	function forma_nariz(){
		return (new Raffleitemfile_Core($this->file_path.'forma_nariz.txt', 1, 'FILE'))->getRaffleItens();
	}

	function forma_olhos(){
		return (new Raffleitemfile_Core($this->file_path.'forma_olhos.txt', 1, 'FILE'))->getRaffleItens();
	}

	function forma_queixo(){
		return (new Raffleitemfile_Core($this->file_path.'forma_queixo.txt', 1, 'FILE'))->getRaffleItens();
	}

	function forma_rosto(){
		return (new Raffleitemfile_Core($this->file_path.'forma_rosto.txt', 1, 'FILE'))->getRaffleItens();
	}

	function forma_sobrancelha(){
		return (new Raffleitemfile_Core($this->file_path.'forma_sobrancelha.txt', 1, 'FILE'))->getRaffleItens();
	}

	function forma_testa(){
		return (new Raffleitemfile_Core($this->file_path.'forma_testa.txt', 1, 'FILE'))->getRaffleItens();
	}

	function olhos_cor(){
		return (new Raffleitemfile_Core($this->file_path.'olhos_cor.txt', 1, 'FILE'))->getRaffleItens();
	}

	function pele_cor(){
		return (new Raffleitemfile_Core($this->file_path.'pele_cor.txt', 1, 'FILE'))->getRaffleItens();
	}

	function cabelos_estilo(){
		return (new Raffleitemfile_Core($this->file_path.'cabelos_estilo.txt', 1, 'FILE'))->getRaffleItens();
	}

	function cabelos_cores(){
		return (new Raffleitemfile_Core($this->file_path.'cabelos_cores.txt', 1, 'FILE'))->getRaffleItens();
	}
}