<?php

class Tempo_Model {
	
	public $file_path;

	function __construct(){
		$this->file_path = PATH_BASE.'config/locale/pt-br/txt/tempo_climatico/';
	}

	public function clima($qtd = 1){
		return (new Raffleitemfile_Core($this->file_path.'clima.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	public function precipitacao($qtd = 1){
		return (new Raffleitemfile_Core($this->file_path.'precipitacao.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	public function temperatura($qtd = 1){
		return (new Raffleitemfile_Core($this->file_path.'temperatura.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	public function vento($qtd = 1){
		return (new Raffleitemfile_Core($this->file_path.'vento.txt', $qtd, 'FILE'))->getRaffleItens();
	}
}