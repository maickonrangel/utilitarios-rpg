<?php

class Frases_Model {

	public $file_path;

	function __construct(){
		$this->file_path = PATH_BASE.'config/locale/pt-br/txt/frases/';
	}

	function frases_epicas($qtd){
		return (new Raffleitemfile_Core($this->file_path.'frases-epicas.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function druidas($qtd){
		return (new Raffleitemfile_Core($this->file_path.'druidas.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function frases_famosas($qtd){
		return (new Raffleitemfile_Core($this->file_path.'frases-famosas.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function guerreiro($qtd){
		return (new Raffleitemfile_Core($this->file_path.'guerreiro.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function magos($qtd){
		return (new Raffleitemfile_Core($this->file_path.'magos.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function paladinos($qtd){
		return (new Raffleitemfile_Core($this->file_path.'paladinos.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function samurai($qtd){
		return (new Raffleitemfile_Core($this->file_path.'samurai.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function clerigo($qtd){
		return (new Raffleitemfile_Core($this->file_path.'clerigo.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function bardo($qtd){
		return (new Raffleitemfile_Core($this->file_path.'bardo.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function ladino($qtd){
		return (new Raffleitemfile_Core($this->file_path.'ladino.txt', $qtd, 'FILE'))->getRaffleItens();
	}
}