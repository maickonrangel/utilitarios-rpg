<?php
class Cidades_Model {

	public $file_path;

	function __construct(){
		$this->file_path = PATH_BASE.'config/locale/pt-br/txt/cidades/';
		$this->tamanho = $this->tamanho();
		// parametros = populacao min, populacao max, limite em PO, mod. de poder central, mod. comunidade
		$this->cidades = [
			'Lugarejo' 			=> [20, 40, 40, -1, -3],
			'Povoado' 			=> [81, 400, 100, 0, -2],
			'Aldeia' 			=> [401, 900, 200, 1, -1],
			'Vila pequena' 		=> [901, 2000, 800, 2, 0],
			'Vila grande' 		=> [2001, 5000, 3000, 3, 3],
			'Cidade pequena' 	=> [5001, 12000, 15000, 4, 6],
			'Cidade grande' 	=> [12001, 25000, 40000, 5, 9],
			'Metrópole' 		=> [25001, 100000, 100000, 6, 12],
		];
	}

	function nome(){
		return (new Raffleitemfile_Core($this->file_path.'nome.txt', 1, 'FILE'))->getRaffleItens();
	}

	function tamanho(){
		return (new Raffleitemfile_Core($this->file_path.'tamanho.txt', 1, 'FILE'))->getRaffleItens();
	}

	function tendencia_do_poder_central(){
		return (new Raffleitemfile_Core($this->file_path.'tendencia.txt', 1, 'FILE'))->getRaffleItens();
	}

	function tipo_de_defesa(){
		return (new Raffleitemfile_Core($this->file_path.'defesa.txt', 1, 'FILE'))->getRaffleItens();
	}

	function tipo_de_religiao(){
		return (new Raffleitemfile_Core($this->file_path.'religiao.txt', 1, 'FILE'))->getRaffleItens();
	}

	function tipo_de_cultura(){
		return (new Raffleitemfile_Core($this->file_path.'cultura.txt', 3, 'FILE'))->getRaffleItens();
	}

	function inporta($qtd){
		return (new Raffleitemfile_Core($this->file_path.'inporta.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function exporta($qtd){
		return (new Raffleitemfile_Core($this->file_path.'exporta.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function fonte_de_renda($qtd){
		return (new Raffleitemfile_Core($this->file_path.'fontes_de_renda.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function populacao(){
		$min = $this->cidades[rtrim($this->tamanho)][0];
		$max = $this->cidades[rtrim($this->tamanho)][1];
		$populacao = number_format(mt_rand($min, $max));
		return $populacao . ' habitantes';
	}

	function limite_po(){
		return number_format($this->cidades[rtrim($this->tamanho)][2]);
	}

	function composicao_racial(){
		$racas = [
				'Humanos',
				'Halflings',
				'Elfos',
				'Anões',
				'Gnomos',
				'Meio-elfos',
				'Meio-orcs',
				'Outros'
			];

		$racas_porcentagem = [
			mt_rand(40,69),
			mt_rand(5,10),
			mt_rand(1,6),
			mt_rand(1,5),
			mt_rand(1,4),
			mt_rand(1,3),
			mt_rand(1,2),
			mt_rand(1,1),
			];

		$total = 0;
		foreach ($racas_porcentagem as $value) {
			$total += $value;
		}

		if ($total < 100) {
			$faltante = 100 - $total;
		}

		$pos = mt_rand(0,7);
		$racas_porcentagem[$pos] += $faltante;

		$composicao_racial = array();
		for ($i=0; $i < count($racas); $i++) { 
			$composicao_racial[$i] = "{$racas_porcentagem[$i]}% {$racas[$i]}";
		}

		return $composicao_racial;
	}	

	function poder_central(){
		$d20 = mt_rand(1, 20);
		$modificador = $this->cidades[rtrim($this->tamanho)][3];
		$rolagem = $d20 + $modificador;

		if ($rolagem <= 13) {
			$opcoes = [
				'Um burgomestre',
				'Um conselho',
				'Um senhor feudal que governa a área como vassalo de um suserano mais poderoso',
				'Um nobre que governa a comunidade como uma cidade-estado'
			];
			$this->tipo_poder_central = 'Convencional';
			$this->tipo_poder_central_descricao = 'A comunidade tem uma forma de governo tradicional';
			$this->tipo_poder_central_escolhido = $opcoes[mt_rand(0,3)];
		} elseif ($rolagem >= 14 and $rolagem <= 18) {
			$opcoes = [
				'Uma gilda',
				'Uma organização formal de mercadores.',
				'Uma organização formal de artesãos.',
				'Uma organização formal de profissionais.',
				'Uma organização formal de ladrões.',
				'Uma organização formal de assasinos.',
				'Uma organização formal de combatentes.',
				'Uma organização de um grupo de aventureiros consagrados.',
				'Um ancião.'
			];
			$this->tipo_poder_central = 'Incomum';
			$this->tipo_poder_central_descricao = 'Talvez a comunidade tenha um burgomestre ou um conselho, mas o verdadeiro poder jaz em outras mãos';
			$this->tipo_poder_central_escolhido = $opcoes[mt_rand(0,8)];
		} else {
			$opcoes = [
				'Um feiticeiro solitário enclausurado em uma torre.',
				'Um mago ditador.',
				'Um mago solitário.',
				'Um sacertote de um deus local.',
				'Um sacertote.',
				'Um feiticeiro poderoso.',
				'Um clérigo.'
			];
			$this->tipo_poder_central = 'Mágico';
			$this->tipo_poder_central_descricao = 'A comunidade tem como líder um ou mais conjuradores, pode ser um clério, mago, feiticeiro ou etc';
			$this->tipo_poder_central_escolhido = $opcoes[mt_rand(0,6)];
		}
	}
}