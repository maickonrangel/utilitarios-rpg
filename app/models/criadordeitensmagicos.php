<?php 

class Criadordeitensmagicos_Model {

	public $tipo;
	public $descricao;

	public function __construct(){
		$this->file_path = 'config/locale/pt-br/txt/tormenta_rpg';
	}

	public function get_tipo_de_magia($tipo = null){
		if ($tipo == null) {
			$tipo = ['arcana','divina'];
			return $tipo[array_rand($tipo)];
		} else {
			return $tipo;
		}
	}

	public function get_magia($circulo){
		if ($circulo == null) {
			$circulo = mt_rand(0,9);
			return $circulo;
		} else {
			return $circulo;
		}
	}

	public function nivel(){
		return mt_rand(1,20);
	}

	public function circulo_maximo($nivel){
		return intval($nivel/2);
	}

	public function pocao(){
		$nivel_conj = $this->nivel();
		$nivel_mag = $this->circulo_maximo($nivel_conj);
		$preco = 25 * $nivel_conj * $nivel_mag;
		$magia = new Raffleitemfile_Core($this->file_path.'/magias/'.$nivel_mag.'.txt', 1);
		$pocao = [
			'nivel-conj' => $nivel_conj,
			'nivel-mag' => $nivel_mag,
			'preco' => $preco,
			'nome' => 'Poção de/da '. $magia
		];

		echo '<pre>';
		print_r($pocao);
		echo '</pre>';
	}
}