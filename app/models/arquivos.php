<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2018
	Model: arquivos
*/

class Arquivos_Model extends Dbrecord_Core {

	private $permit;

	public function __construct(){
		parent::__construct();
		$this->permit = [
			'id',
			'fk_usuarios',
			'tipo_geral',
			'tipo_documento',
			'nome',
			'palavras_chave',
			'sistema_rpg',
			'url_imagem',
			'url_documento',
			'descricao',
			'cadastrado'
		];
	}

	public function get_permit(){
		return $this->permit;
	}
}