<?php


class Fichasvampiro_Model {

	public $atributos;
	public $habilidades;
	public $antecedente;
	public $nome;
	public $sexo;
	public $arquetipo;
	public $cla;
	public $conceito;
	public $natureza;
	public $geracao;
	public $virtude;
	public $humanidade;
	public $fdv;
	public $pts_sangue;
	public $qualidades_defeitos;

	public function __construct($geracao, $cla, $natureza, $arquetipo) {
		$this->sexo();
		$this->nome();
		$this->arquetipo($arquetipo);
		$this->cla($cla);
		$this->antecedente();
		$this->virtude();
		$this->conceito();
		$this->formatar_atributos();
		$this->formatar_habilidades();
		$this->natureza($natureza);
		$this->geracao($geracao);
		$this->set_label();
		$this->humanidade();
		$this->forca_de_vontade();
		$this->pontos_de_sangue();
		$this->pts_bonus();
	}

	public function set_label(){
		 $this->label_attr = [
          'fisico'=> ['for'=>'Força','des'=>'Destreza','vig'=>'Vigor'],
          'social'=> ['car'=>'Carisma ','man'=>'Manipulação','apa'=>'Aparência'],
          'mental'=> ['per'=>'Percepção','int'=>'Inteligência','rac'=>'Raciocínio']
      	];

      	$this->label_hab = [
      		'talento' => [
      			'pro' => 'Prontidão',
				'esp' => 'Esportes',
				'bri' => 'Briga',
				'esq' => 'Esquiva',
				'emp' => 'Empatia',
				'exp' => 'Expressão',
				'int' => 'Intimidação',
				'lid' => 'Liderança',
				'man' => 'Manha',
				'lab' => 'Lábia'
				],
			'pericia' => [
      			'emp' => 'Emp. c/animais',
      			'ofi' => 'Ofícios',
				'con' => 'Condução',
				'etq' => 'Etiqueta',
				'armf' => 'Armas de fogo',
				'armb' => 'Armas brancas',
				'per' => 'Performace',
				'seg' => 'Segurança',
				'fur' => 'Furtividade',
				'sob' => 'Sobrevivência'
				],
 
			'conhecimento' => [
      			'aca' => 'Acadêmicos',
      			'com' => 'Computador',
				'fin' => 'Finanças',
				'inv' => 'Investigação',
				'dir' => 'Direito',
				'lin' => 'Linguístia',
				'med' => 'Medicina',
				'ocu' => 'Ocultismo',
				'pol' => 'Política',
				'cie' => 'Ciências'
				],
      	];

      	$this->label_geracao = [
      		3 => 'Terceira',
			4 => 'Quarta',
			5 => 'Quinta',
			6 => 'Sexta',
			7 => 'Sétima',
			8 => 'Oitava',
			9 => 'Nona',
			10 => 'Décima',
			11 => 'Décima primeira',
			12 => 'Décima segunda',
			13 => 'Décima terceira'
     	];
	}

	public function get_arquetipo(){
		$arq = (new Raffleitemfile_Core(PATH_BASE.'config/locale/pt-br/txt/vampiro/arquetipo.txt', 42, 'FILE'))->getRaffleItens();
		foreach ($arq as $key => $value) {
			$arquetipos[] = explode(' — ', $value)[0];
		}
		return $arquetipos;
	}

	public function get_natureza(){
		$nat = (new Raffleitemfile_Core(PATH_BASE.'config/locale/pt-br/txt/vampiro/natureza.txt', 30, 'FILE'))->getRaffleItens();
		foreach ($nat as $key => $value) {
			$natureza[] = explode(':', $value)[0];
		}
		return $natureza;
	}

	public function get_cla(){
		$cla = (new Raffleitemfile_Core(PATH_BASE.'config/locale/pt-br/txt/vampiro/cla.txt', 14, 'FILE'))->getRaffleItens();
		foreach ($cla as $key => $value) {
			$clan[] = explode(' - ', $value)[0];
		}
		return $clan;
	}

	public function sexo(){
		$sexo = ['masculino','feminino'];
		$this->sexo  = $sexo[array_rand($sexo)];
	}

	public function nome(){
		if ($this->sexo == 'masculino') {
			$this->nome = (new Raffleitemfile_Core(PATH_BASE.'config/locale/pt-br/txt/nomes/vampiros.txt', 1, 'FILE'))->getRaffleItens();
		} else {
			$this->nome = (new Raffleitemfile_Core(PATH_BASE.'config/locale/pt-br/txt/nomes/vampiras.txt', 1, 'FILE'))->getRaffleItens();
		}
	}

	public function arquetipo($arquetipo_nome = null){
		if ($arquetipo_nome != null) {
			$this->arquetipo = $arquetipo_nome;
		} else {
			$this->arquetipo = (new Raffleitemfile_Core(PATH_BASE.'config/locale/pt-br/txt/vampiro/arquetipo.txt', 1, 'FILE'))->getRaffleItens();
		}
	}

	public function geracao($geracao = null){
		if ($geracao != null) {
			$this->geracao = $geracao;
		} else {
			$this->geracao = mt_rand(3,13);
		}
	}

	public function natureza($natureza_nome){
		if ($natureza_nome != null) {
			$natureza = (new Raffleitemfile_Core(PATH_BASE.'config/locale/pt-br/txt/vampiro/natureza.txt', 30, 'FILE'))->getRaffleItens();
			foreach ($natureza as $key => $value) {
				$nat = explode(":", $value);
				if ($nat[0] == $natureza_nome) {
					$this->natureza['nome'] = $nat[0];
					$this->natureza['descricao'] = $nat[1];
				}
			}
		} else {
			$natureza = (new Raffleitemfile_Core(PATH_BASE.'config/locale/pt-br/txt/vampiro/natureza.txt', 1, 'FILE'))->getRaffleItens();
			$descricao = explode(":", $natureza);
			$this->natureza['nome'] = $descricao[0];
			$this->natureza['descricao'] = $descricao[1];
		}
	}

	public function cla($cla_nome = null){
		if ($cla_nome != null) {
			$clans = (new Raffleitemfile_Core(PATH_BASE.'config/locale/pt-br/txt/vampiro/cla.txt', 14, 'FILE'))->getRaffleItens();	
			foreach ($clans as $key => $value) {
				$cla = explode(" - ", $value);
				if ($cla_nome == $cla[0]){
					$this->cla['nome'] = $cla[0];
					$this->cla['descricao'] = $cla[1];
					$disciplinas = explode(",", $cla[2]);
					foreach ($disciplinas as $key2 => $value2) {
						$this->cla['disciplinas'][] = ['nome'=>$value2, 'valor'=>0];
					}	
				}
			}
		} else {
			$cla = (new Raffleitemfile_Core(PATH_BASE.'config/locale/pt-br/txt/vampiro/cla.txt', 1, 'FILE'))->getRaffleItens();	
			$cla = explode(" - ", $cla);
			$this->cla['nome'] = $cla[0];
			$this->cla['descricao'] = $cla[1];
			$disciplinas = explode(",", $cla[2]);
			foreach ($disciplinas as $key => $value) {
				$this->cla['disciplinas'][] = ['nome'=>$value, 'valor'=>0];
			}
		}
		$pts = 3;
		while ($pts > 0) {
			$qtd_sorteada = mt_rand(1,$pts);
			$this->cla['disciplinas'][mt_rand(0,2)]['valor'] += $qtd_sorteada;
			$pts -= $qtd_sorteada;
		}
	}

	public function antecedente(){
		$antecedentes = (new Raffleitemfile_Core(PATH_BASE.'config/locale/pt-br/txt/vampiro/antecedentes.txt', 10, 'FILE'))->getRaffleItens();	
		$pts = 5;
		$cont = 0;
		while ($pts > 0) {
			$qtd_sorteada = mt_rand(1,$pts);
			$indice = array_rand($antecedentes);
			$antecedente = explode(":", $antecedentes[$indice]);
			$this->antecedente[$cont]['nome'] = $antecedente[0];
			$this->antecedente[$cont]['descricao'] = $antecedente[1];
			$this->antecedente[$cont]['valor'] = $qtd_sorteada;
			$pts -= $qtd_sorteada;
			unset($antecedentes[$indice]);
			$cont++;
		}
	}

	public function humanidade(){
		$this->humanidade = $this->virtude['consiencia']['valor'] + $this->virtude['autocontrole']['valor'];
	}

	public function forca_de_vontade(){
		$this->fdv = $this->virtude['coragem']['valor'];
	}

	public function pontos_de_sangue(){
		$geracao = [
			3 =>60,
			4 =>50,
			5 =>40,
			6 =>30,
			7 =>20,
			8 =>15,
			9 =>14,
			10 =>13,
			11 =>12,
			12 =>11,
			13 =>10
		];

		if ($this->geracao != 13) {
			$this->pts_sangue = mt_rand(prev($geracao), $geracao[$this->geracao]);
		} else {
			$this->pts_sangue = mt_rand(1,10);
		}

	}

	public function virtude(){
		$consiencia = mt_rand(1,7);
		$this->virtude['consiencia']['nome'] = 'Consciência';
		$this->virtude['consiencia']['descricao'] = 'A virtude que permite ao personagem pensar sobre o que é certo e o que é errado; isso pode trazer remorsos ao personagem quando sucumbir à Besta interior.';
		$this->virtude['consiencia']['valor'] = 1+$consiencia;

		if ((7 - $consiencia) > 0) {
			$autocontrole = mt_rand(1, (7 - $consiencia));
		} else {
			$autocontrole = 0;
		}
		$this->virtude['autocontrole']['nome'] = 'Autocontrole';
		$this->virtude['autocontrole']['descricao'] = 'Indica quanto o personagem pode se controlar para resistir à Besta e a impulsos emocionais.';
		$this->virtude['autocontrole']['valor'] = 1+$autocontrole;

		if ((7 - ($autocontrole + $consiencia)) > 0) {
			$coragem = 7 - ($autocontrole + $consiencia);
		} else {
			$coragem = 0;
		}

		$this->virtude['coragem']['nome'] = 'Coragem';
		$this->virtude['coragem']['valor'] = 1+$coragem;
		$this->virtude['coragem']['descricao'] = 'Indica quão corajoso o personagem é para enfrentar os piores temores dos membros, como luz do sol, fogo, Fé Verdadeira.';
	}

	public function conceito(){
		$this->conceito = (new Raffleitemfile_Core(PATH_BASE.'config/locale/pt-br/txt/vampiro/conceitos.txt', 1, 'FILE'))->getRaffleItens();	
	}

	public function atributos(){
		$pontos = [7,5,3];
		shuffle($pontos);
		$atributos = [];
		for ($i=0; $i < count($pontos); $i++) {
			$atributos[] = $this->distribuir_pts_atributo($pontos[$i]);
		}

		return $atributos;
	}

	public function distribuir_pts_atributo($pts){
		$pt = $pts;
		while ($pt != 0) {
			$gasto = mt_rand(1, $pt);
			$tipos[] = $gasto;
			$pt -= $gasto;
		}

		if (count($tipos) >= 4) {
			$total = 0;
			foreach ($tipos as $key => $value) {
				if ($key >= 3) {
					$total += $value;
					unset($tipos[$key]);
				}
			}
			$tipos[2] += $total;
		}
		return $tipos;
	}

	public function formatar_atributos(){
		$fisico = ['for'=>1,'des'=>1,'vig'=>1];
		$social = ['car'=>1,'man'=>1,'apa'=>1];
		$mental = ['per'=>1,'int'=>1,'rac'=>1];

		$attr = $this->atributos();
		$fisico['for'] += isset($attr[0][0])?$attr[0][0]:0;
		$fisico['des'] += isset($attr[0][1])?$attr[0][1]:0;
		$fisico['vig'] += isset($attr[0][2])?$attr[0][2]:0;

		$social['car'] += isset($attr[1][0])?$attr[1][0]:0;
		$social['man'] += isset($attr[1][1])?$attr[1][1]:0;
		$social['apa'] += isset($attr[1][2])?$attr[1][2]:0;

		$mental['per'] += isset($attr[2][0])?$attr[2][0]:0;
		$mental['int'] += isset($attr[2][1])?$attr[2][1]:0;
		$mental['rac'] += isset($attr[2][2])?$attr[2][2]:0;

		$this->atributos['fisico'] = $fisico;
		$this->atributos['social'] = $social;
		$this->atributos['mental'] = $mental;
	}

	public function habilidades(){
		$pontos = [13,9,5];
		shuffle($pontos);
		$habilidades = [];
		for ($i=0; $i < count($pontos); $i++) {
			$habilidades[] = $this->distribuir_pts_habilidade($pontos[$i]);
		}
		return $habilidades;
	}

	public function distribuir_pts_habilidade($pts){
		$pt = $pts;
		while ($pt != 0) {
			$gasto = mt_rand(1, $pt);
			if ($gasto <= 3) {
				$tipos[] = $gasto;
				$pt -= $gasto;
			}
		}

		if (count($tipos) >= 10) {
			$total = 0;
			foreach ($tipos as $key => $value) {
				if ($key >= 9) {
					$total += $value;
					unset($tipos[$key]);
				}
			}
			$tipos[mt_rand(0,9)] += $total; //tem um erro aqui
		}
		return $tipos;
	}

	public function formatar_habilidades(){
		$talento = ['pro'=>0,'esp'=>0,'bri'=>0,'esq'=>0,'emp'=>0,'exp'=>0,'int'=>0,'lid'=>0,'man'=>0,'lab'=>0];
		$pericia = ['emp'=>0,'ofi'=>0,'con'=>0,'etq'=>0,'armf'=>0,'armb'=>0,'per'=>0,'seg'=>0,'fur'=>0,'sob'=>0];
		$conhecimento = ['aca'=>0,'com'=>0,'fin'=>0,'inv'=>0,'dir'=>0,'lin'=>0,'med'=>0,'ocu'=>0,'pol'=>0,'cie'=>0];

		$attr = $this->habilidades();
		$talento['pro'] += isset($attr[0][0])?$attr[0][0]:0;
		$talento['esp'] += isset($attr[0][1])?$attr[0][1]:0;
		$talento['bri'] += isset($attr[0][2])?$attr[0][2]:0;
		$talento['esq'] += isset($attr[0][3])?$attr[0][3]:0;
		$talento['emp'] += isset($attr[0][4])?$attr[0][4]:0;
		$talento['exp'] += isset($attr[0][5])?$attr[0][5]:0;
		$talento['int'] += isset($attr[0][6])?$attr[0][6]:0;
		$talento['lid'] += isset($attr[0][7])?$attr[0][7]:0;
		$talento['man'] += isset($attr[0][8])?$attr[0][8]:0;
		$talento['lab'] += isset($attr[0][9])?$attr[0][9]:0;

		$pericia['emp']  += isset($attr[1][0])?$attr[1][0]:0;
		$pericia['ofi']  += isset($attr[1][1])?$attr[1][1]:0;
		$pericia['con']  += isset($attr[1][2])?$attr[1][2]:0;
		$pericia['etq']  += isset($attr[1][3])?$attr[1][3]:0;
		$pericia['armf'] += isset($attr[1][4])?$attr[1][4]:0;
		$pericia['armb'] += isset($attr[1][5])?$attr[1][5]:0;
		$pericia['per']  += isset($attr[1][6])?$attr[1][6]:0;
		$pericia['seg']  += isset($attr[1][7])?$attr[1][7]:0;
		$pericia['fur']  += isset($attr[1][8])?$attr[1][8]:0;
		$pericia['sob']  += isset($attr[1][9])?$attr[1][9]:0;

		$conhecimento['aca'] += isset($attr[2][0])?$attr[2][0]:0;
		$conhecimento['com'] += isset($attr[2][1])?$attr[2][1]:0;
		$conhecimento['fin'] += isset($attr[2][2])?$attr[2][2]:0;
		$conhecimento['inv'] += isset($attr[2][3])?$attr[2][3]:0;
		$conhecimento['dir'] += isset($attr[2][4])?$attr[2][4]:0;
		$conhecimento['lin'] += isset($attr[2][5])?$attr[2][5]:0;
		$conhecimento['med'] += isset($attr[2][6])?$attr[2][6]:0;
		$conhecimento['ocu'] += isset($attr[2][7])?$attr[2][7]:0;
		$conhecimento['pol'] += isset($attr[2][8])?$attr[2][8]:0;
		$conhecimento['cie'] += isset($attr[2][9])?$attr[2][9]:0;

		$this->habilidades['talento'] = $talento;
		$this->habilidades['pericia'] = $pericia;
		$this->habilidades['conhecimento'] = $conhecimento;
	}

	public function pts_bonus(){
		$pts_negativo = 0;
		$pts_total = 15;
		$opcoes_investimento = ['atributos','habilidades','antecedentes','disciplina','virtudes','humanidade','fdv','qualidades'];
		$precos = ['atributos'=>5,'habilidades'=>2,'antecedentes'=>1,'disciplina'=>7,'virtudes'=>2,'humanidade'=>1,'fdv'=>1,'qualidades'=>'x'];

		while ($pts_total > 0) {
			// $sorteio = 7;
			$sorteio = array_rand($opcoes_investimento);
			if ($opcoes_investimento[$sorteio] == 'atributos' && $pts_total >= $precos[$opcoes_investimento[$sorteio]]) {
				$atributos = ['for','des','vig','car','man','apa','per','int','rac'];
				$atributo_escolhido = $atributos[array_rand($atributos)];
				if ($atributo_escolhido == 'for' || $atributo_escolhido == 'des' || $atributo_escolhido == 'vig') {
					$this->atributos['fisico'][$atributo_escolhido] += 1;
				} elseif ($atributo_escolhido == 'car' || $atributo_escolhido == 'man' || $atributo_escolhido == 'apa') {
					$this->atributos['social'][$atributo_escolhido] += 1;
				} elseif ($atributo_escolhido == 'per' || $atributo_escolhido == 'int' || $atributo_escolhido == 'rac') {
					$this->atributos['mental'][$atributo_escolhido] += 1;
				}
				$pts_total -= 5;
			}

			if ($opcoes_investimento[$sorteio] == 'habilidades' && $pts_total >= $precos[$opcoes_investimento[$sorteio]]) {
				$tipo = ['talentos','pericias','conhecimentos'];
				if ($tipo[array_rand($tipo)] == 'talentos') {
					$talentos = ['pro','esp','bri','esq','emp','exp','int','lid','man','lab'];
					$habilidade_escolhida = $talentos[array_rand($talentos)];
					$this->habilidades['talento'][$habilidade_escolhida] += 1;
				} elseif ($tipo[array_rand($tipo)] == 'pericias') {
					$pericias = ['emp','ofi','con','etq','armf','armb','per','seg','fur','sob'];
					$habilidade_escolhida = $pericias[array_rand($pericias)];
					$this->habilidades['pericia'][$habilidade_escolhida] += 1;
				} elseif ($tipo[array_rand($tipo)] == 'conhecimentos') {
					$conhecimentos = ['aca','com','fin','inv','dir','lin','med','ocu','pol','cie'];
					$habilidade_escolhida = $conhecimentos[array_rand($conhecimentos)];
					$this->habilidades['conhecimento'][$habilidade_escolhida] += 1;
				}
				$pts_total -= 2;
			}

			if ($opcoes_investimento[$sorteio] == 'antecedentes' && $pts_total >= $precos[$opcoes_investimento[$sorteio]]) {
				$this->antecedente[array_rand($this->antecedente)]['valor'] += 1;
				$pts_total -= 1;
			}
			
			if ($opcoes_investimento[$sorteio] == 'disciplina' && $pts_total >= $precos[$opcoes_investimento[$sorteio]]) {
				$this->cla['disciplinas'][array_rand($this->cla['disciplinas'])]['valor'] += 1;
			}

			if ($opcoes_investimento[$sorteio] == 'virtudes' && $pts_total >= $precos[$opcoes_investimento[$sorteio]]) {
				$tipo = ['coragem','autocontrole','consiencia'];
				if ($tipo[array_rand($tipo)] == 'coragem') {
					$this->virtude['coragem']['valor'] += 1;
				} elseif ($tipo[array_rand($tipo)] == 'autocontrole') {
					$this->virtude['autocontrole']['valor'] += 1;
				} elseif ($tipo[array_rand($tipo)] == 'consiencia') {
					$this->virtude['consiencia']['valor'] += 1;
				}
				$pts_total -= 2;
			}

			if ($opcoes_investimento[$sorteio] == 'humanidade' && $pts_total >= $precos[$opcoes_investimento[$sorteio]]) {
				$this->humanidade += 1;
				$pts_total -= 1;
			}

			if ($opcoes_investimento[$sorteio] == 'fdv' && $pts_total >= $precos[$opcoes_investimento[$sorteio]]) {
				$this->fdv += 1;
				$pts_total -= 1;
			}

			if ($opcoes_investimento[$sorteio] == 'qualidades' && $pts_total >= $precos[$opcoes_investimento[$sorteio]]) {
				$qualidades_defeitos = (new Raffleitemfile_Core(PATH_BASE.'config/locale/pt-br/txt/vampiro/qualidades_defeitos.txt', mt_rand(2,3), 'FILE'))->getRaffleItens();
				shuffle($qualidades_defeitos);
				foreach ($qualidades_defeitos as $key => $value) {
					$partes = explode(' - ', $value);
					$ponto = explode(' ', $partes[1]);
					$numero = substr($ponto[0], 1);
					$tipo = substr($ponto[3], 0, strlen($ponto[3])-1);
					if ($tipo == 'Defeito' and ($pts_negativo+$numero) <= 7) {
						$pts_negativo += $numero;
						$pts_total += $pts_negativo;
						$this->qualidades_defeitos[] = [
							'nome' => $partes[0],
							'pts' => $partes[1],
							'numero'=> $numero,
							'tipo'=> $tipo,
							'desc' => $partes[2]
						];
					} elseif ($tipo == 'Qualidade' and $numero <= $pts_total) {
						$pts_total -= $numero;
						$this->qualidades_defeitos[] = [
							'nome' => $partes[0],
							'pts' => $partes[1],
							'numero'=> $numero,
							'tipo'=> $tipo,
							'desc' => $partes[2]
						];
					} else {
						break;
					}
				}
			}
		}

		
	}
}