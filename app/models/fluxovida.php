<?php

class Fluxovida_Model {
	
	public $ficha;
	public $file_path;

	function __construct(){
		$this->file_path = PATH_BASE.'config/locale/pt-br/txt/fluxovida/';
		$this->ficha = [
			'pele' 				=> [ 'nome' => $this->cor_da_pele(), 'label' => 'Cor da Pele'],
			'olho' 				=> [ 'nome' => $this->cor_dos_olhos(), 'label' => 'Cor dos olhos'],
			'cabelos' 			=> [ 'nome' => $this->cor_dos_cabelos(), 'label' => 'Cor dos cabelos'],
			'cabelos_pelos'		=> [ 'nome' => $this->cabelos_pelos_faciais(), 'label' => 'Cabelos e pelos faciais'],
			'nascimento' 		=> [ 'nome' => $this->nascimento(), 'label' => 'Você nasceu'],
			'pais' 				=> [ 'nome' => $this->seus_pais(), 'label' => 'Seus pais'],
			'irmaos' 			=> [ 'nome' => $this->irmaos(), 'label' => 'Você tem irmãos'],
			'sorte_tragedia' 	=> [ 'nome' => $this->sorte_tragedia(), 'label' => 'Sorte ou Tragédia'],
			'status_social' 	=> [ 'nome' => $this->status_social(), 'label' => 'Status social & Profissão'],
			'amigos_inimigos' 	=> [ 'nome' => $this->amigos_inimigos(), 'label' => 'Amigos & Inimigos'],
			'relacao_amizade' 	=> [ 'nome' => $this->origem_relacao_amizade(), 'label' => 'Origem ou Relação de Amizade'],
			'causas_amizade' 	=> [ 'nome' => $this->causas_inimizade(), 'label' => 'Causas da Inimizade'],
			'consequencias' 	=> [ 'nome' => $this->consequencias_inimizade(), 'label' => 'Consequências da Inimizade'],
			'vida_amorosa' 		=> [ 'nome' => $this->vida_amorosa(), 'label' => 'Vida Amorosa'],
			'inspiracao' 		=> [ 'nome' => $this->inspiracao(), 'label' => 'Razão de viver, Inspiração'],
			'reliquia' 			=> [ 'nome' => $this->reliquia(), 'label' => 'Principal Relíquia'],
			'nao_gosta' 		=> [ 'nome' => $this->nao_gosta(), 'label' => 'Eu não gosto de...'],
			'medo' 				=> [ 'nome' => $this->medo(), 'label' => 'Meu principal medo é...'],
			'opiniao' 			=> [ 'nome' => $this->opiniao(), 'label' => 'Opinião sobre estranhos e desconhecidos'],
			'arrependimentos' 	=> [ 'nome' => $this->arrependimentos(), 'label' => 'Arrependimentos'],
			'costumes' 			=> [ 'nome' => $this->costumes(), 'label' => 'Costumes, hábitos e manias'],
			'vida_aventureiro' 	=> [ 'nome' => $this->vida_aventureiro(), 'label' => 'O que te levou à vida de aventureiro'],
			'feito' 			=> [ 'nome' => $this->feito(), 'label' => 'Primeiro feito notável'],
			'criaturas_odiadas' => [ 'nome' => $this->criaturas_odiadas(), 'label' => 'Criaturas odiadas'],
			'habilidades_inuteis' => [ 'nome' => $this->habilidades_inuteis(), 'label' => 'Habilidades Inúteis'],
			'ultima_frase' 		=> [ 'nome' => $this->ultima_frase(), 'label' => 'Última frase antes de morrer'],
		];
	}

	public function cor_da_pele(){
		$cor = (new Raffleitemfile_Core($this->file_path.'cor_da_pele.txt', 1, 'FILE'))->getRaffleItens();
		$especial = mt_rand(1, 40);

		switch ($especial) {
			case 27:
				$cicatrizes = ['cicatrizes comuns','cicatrizes ritualísticas'];
				$cicatriz = $cicatrizes[array_rand($cicatrizes)];
				$cor = "{$cor} {$cicatriz}";
			break;

			case 28:
				$cor = "{$cor} com tatuagens";
			break;

			case 29:
				while ($cor == 'Albino') {
					$cor = (new Raffleitemfile_Core($this->file_path.'cor_da_pele.txt', 1, 'FILE'))->getRaffleItens();
				}
				$cor = "{$cor} com marca de nascença";
			break;

			case 30:
				$manchas = ['mancha mais clara do que a pele','mancha mais escura do que a pele'];
				$mancha = $manchas[array_rand($manchas)];
				while ($cor == 'Albino') {
					$cor = (new Raffleitemfile_Core($this->file_path.'cor_da_pele.txt', 1, 'FILE'))->getRaffleItens();
				}
				$cor = "{$cor} com {$mancha}";
			break;

			case 31:
				$cicatrizes = ['cicatrizes comuns','cicatrizes ritualísticas'];
				$cicatriz = $cicatrizes[array_rand($cicatrizes)];
				$cor = "{$cor} com {$cicatriz} e tatuagens";
			break;

			case 32:
				while ($cor == 'Albino') {
					$cor = (new Raffleitemfile_Core($this->file_path.'cor_da_pele.txt', 1, 'FILE'))->getRaffleItens();
				}
				$cicatrizes = ['cicatrizes comuns','cicatrizes ritualísticas'];
				$cicatriz = $cicatrizes[array_rand($cicatrizes)];
				$cor = "{$cor} com {$cicatriz}, tatuagens e marca de nascença";
			break;

			case 33:
				while ($cor == 'Albino') {
					$cor = (new Raffleitemfile_Core($this->file_path.'cor_da_pele.txt', 1, 'FILE'))->getRaffleItens();
				}
				$cicatrizes = ['cicatrizes comuns','cicatrizes ritualísticas'];
				$cicatriz = $cicatrizes[array_rand($cicatrizes)];
				$cor = "{$cor} com {$cicatriz} e marca de nascença";
			break;

			case 34:
				while ($cor == 'Albino') {
					$cor = (new Raffleitemfile_Core($this->file_path.'cor_da_pele.txt', 1, 'FILE'))->getRaffleItens();
				}
				$cicatrizes = ['cicatrizes comuns','cicatrizes ritualísticas'];
				$cicatriz = $cicatrizes[array_rand($cicatrizes)];
				$cor = "{$cor} com marca de nascença e tatuagens";
			break;

			case 35:
				$cicatrizes = ['cicatrizes comuns','cicatrizes ritualísticas'];
				$cicatriz = $cicatrizes[array_rand($cicatrizes)];

				$manchas = ['mancha mais clara do que a pele','mancha mais escura do que a pele'];
				$mancha = $manchas[array_rand($manchas)];

				$cor = "{$cor} com {$cicatriz} e {$mancha}";
			break;

			case 36:
				while ($cor == 'Albino') {
					$cor = (new Raffleitemfile_Core($this->file_path.'cor_da_pele.txt', 1, 'FILE'))->getRaffleItens();
				}
				$cicatrizes = ['cicatrizes comuns','cicatrizes ritualísticas'];
				$cicatriz = $cicatrizes[array_rand($cicatrizes)];

				$manchas = ['mancha mais clara do que a pele','mancha mais escura do que a pele'];
				$mancha = $manchas[array_rand($manchas)];

				$cor = "{$cor} com {$cicatriz}, {$mancha} e tatuagens";
			break;

			case 37:
				while ($cor == 'Albino') {
					$cor = (new Raffleitemfile_Core($this->file_path.'cor_da_pele.txt', 1, 'FILE'))->getRaffleItens();
				}
				$cicatrizes = ['cicatrizes comuns','cicatrizes ritualísticas'];
				$cicatriz = $cicatrizes[array_rand($cicatrizes)];

				$manchas = ['mancha mais clara do que a pele','mancha mais escura do que a pele'];
				$mancha = $manchas[array_rand($manchas)];

				$cor = "{$cor} com {$cicatriz}, {$mancha} e marca de nascença";
			break;

			case 38:
				while ($cor == 'Albino') {
					$cor = (new Raffleitemfile_Core($this->file_path.'cor_da_pele.txt', 1, 'FILE'))->getRaffleItens();
				}
				$cicatrizes = ['cicatrizes comuns','cicatrizes ritualísticas'];
				$cicatriz = $cicatrizes[array_rand($cicatrizes)];

				$manchas = ['mancha mais clara do que a pele','mancha mais escura do que a pele'];
				$mancha = $manchas[array_rand($manchas)];

				$cor = "{$cor} com {$cicatriz}, {$mancha}, tatuagens e marca de nascença";
			break;
		}

		return $cor;
	}

	public function cor_dos_olhos(){
		$olhos = (new Raffleitemfile_Core($this->file_path.'cor_dos_olhos.txt', 1, 'FILE'))->getRaffleItens();
		$especial = mt_rand(1, 30);
		switch ($especial) {
			case 26:
				$olhos = "{$olhos} com olheiras profundas e permanentes";
			break;

			case 27:
				$olhos = "{$olhos} com esclera (branco do olho) com veios de sangue";
			break;

			case 28:
				$olhos = "{$olhos} com esclera (branco do olho) amarelada";
			break;

			case 29:
				$olhos = "{$olhos} e caolho";
			break;

			case 30:
				$olhos2 = (new Raffleitemfile_Core($this->file_path.'cor_dos_olhos.txt', 1, 'FILE'))->getRaffleItens();
				while ($olhos2 == $olhos) {
					$olhos2 = (new Raffleitemfile_Core($this->file_path.'cor_dos_olhos.txt', 1, 'FILE'))->getRaffleItens();
				}
				$olhos = "Um olho de cada cor: {$olhos} e {$olhos2}";
			break;

		}

		return $olhos;	
	}

	public function cor_dos_cabelos(){
		$cabelos = (new Raffleitemfile_Core($this->file_path.'cor_dos_cabelos.txt', 1, 'FILE'))->getRaffleItens();
		$especial = mt_rand(1, 30);
		switch ($especial) {
			case 27:
				$cabelos = "{$cabelos} com têmporas grisalhas";
			break;

			case 28:
				$cabelos = "{$cabelos} mas grisalho";
			break;

			case 29:
				$cabelos = "{$cabelos} mas calvo";
			break;

			case 30:
				$tipos = ['careca natural','careca cabeça raspada'];
				$tipo = $tipos[array_rand($tipos)];
				$cabelos = "Era cor {$cabelos} mas por algum moívo hoje é {$tipo}";
			break;

		}

		return $cabelos;	
	}

	public function cabelos_pelos_faciais(){
		return (new Raffleitemfile_Core($this->file_path.'cabelos_e_pelos_faciais.txt', 1, 'FILE'))->getRaffleItens();
	}

	public function nascimento(){
		$rolagem  = mt_rand(1, 30);
		if ($rolagem <= 15) {
			$nascimento = (new Raffleitemfile_Core($this->file_path.'nascimento_mundano.txt', 1, 'FILE'))->getRaffleItens();
		} else {
			$especial = mt_rand(1, 30);
			if ($especial == 29) {
				$nascimento1 = (new Raffleitemfile_Core($this->file_path.'nascimento_mundano.txt', 1, 'FILE'))->getRaffleItens();
				$nascimento2 = (new Raffleitemfile_Core($this->file_path.'condicao_especial.txt', 1, 'FILE'))->getRaffleItens();
				$nascimento = "{$nascimento1} {$nascimento2}";
			} else if ($especial == 30){
				$nascimento = (new Raffleitemfile_Core($this->file_path.'condicao_especial.txt', 2, 'FILE'))->getRaffleItens();
				$nascimento = "{$nascimento[0]} {$nascimento[1]}";
			} else {
				$nascimento = (new Raffleitemfile_Core($this->file_path.'condicao_especial.txt', 1, 'FILE'))->getRaffleItens();
			}
		}
		return $nascimento;
	}

	public function seus_pais(){
		return (new Raffleitemfile_Core($this->file_path.'seus_pais.txt', 1, 'FILE'))->getRaffleItens();	
	}

	public function irmaos(){
		$rolagem = mt_rand(1, 30);
		$irmaos = '';
		if ($rolagem <= 15) {
			$irmaos = 'Não, sou filho único';
		} else {
			$doisD6 = mt_rand(1, 6) + mt_rand(1, 6);
			$irmaos = "Sim, tenho {$doisD6} irmãos";
		}
		return $irmaos;
	}

	public function sorte_tragedia(){
		$rolagem = mt_rand(1, 30);
		$sorte_tragedia = '';
		if ($rolagem <= 6) {
			$sorte_tragedia = 'Nada aconteceu, você teve uma vida bem mundana e monótona.';
		} else if ($rolagem >= 7 && $rolagem <= 14) {
			$rolagem_da_sorte = mt_rand(1,30);
			if ($rolagem_da_sorte == 30) {
				$sorte2 = (new Raffleitemfile_Core($this->file_path.'sorte.txt', 2, 'FILE'))->getRaffleItens();
				$sorte_tragedia = "{$sorte2[0]}.<br>{$sorte2[1]}.";
			} else {
				$sorte_tragedia = (new Raffleitemfile_Core($this->file_path.'sorte.txt', 1, 'FILE'))->getRaffleItens();
			}
		} else if ($rolagem >= 15 && $rolagem <= 22) {
			$rolagem_da_tragedia = mt_rand(1,30);
			if ($rolagem_da_tragedia == 30) {
				$tragedia2 = (new Raffleitemfile_Core($this->file_path.'tragedia.txt', 2, 'FILE'))->getRaffleItens();
				$sorte_tragedia = "{$tragedia2[0]}.<br>{$tragedia2[1]}.";
			} else {
				$sorte_tragedia = (new Raffleitemfile_Core($this->file_path.'tragedia.txt', 1, 'FILE'))->getRaffleItens();
			}
		} else if ($rolagem >= 23 && $rolagem <= 30) {
			$sorte = (new Raffleitemfile_Core($this->file_path.'sorte.txt', 1, 'FILE'))->getRaffleItens();
			$tragedia = (new Raffleitemfile_Core($this->file_path.'tragedia.txt', 1, 'FILE'))->getRaffleItens();
			$sorte_tragedia = "{$sorte}<br>{$tragedia}";
		}

		return $sorte_tragedia;
	}

	public function status_social(){
		$rolagem = mt_rand(1, 30);
		if ($rolagem <= 3) {
			$status = 'Escravo';
			$profissao = (new Raffleitemfile_Core($this->file_path.'escravos_profissao_anterior.txt', 1, 'FILE'))->getRaffleItens();
		} else if ($rolagem >= 4 && $rolagem <= 29) {
			$status = 'Plebeu';
			$profissao = (new Raffleitemfile_Core($this->file_path.'plebeus_profissao_anterior.txt', 1, 'FILE'))->getRaffleItens();
		} else {
			$status = 'Nobre';
			$profissao = (new Raffleitemfile_Core($this->file_path.'nobres_profissao_anterior.txt', 1, 'FILE'))->getRaffleItens();
		}

		return "{$status} - {$profissao}";
	}

	public function amigos_inimigos(){
		$rolagem = mt_rand(1, 30);
		$amigos_inimigos = '';
		$tipo = '';
		if ($rolagem <= 6) {
			$amigos_inimigos = 'Nenhum amigo ou inimigo digno de nota.';
		} else if ($rolagem >= 7 && $rolagem <= 14) {
			$tipo = "<b>Amigo</b>: ";
			$amigos_inimigos = (new Raffleitemfile_Core($this->file_path.'amigo.txt', 1, 'FILE'))->getRaffleItens();
		} else if ($rolagem >= 15 && $rolagem <= 22) {
			$tipo = "<b>Inimigo</b>: ";
			$amigos_inimigos = (new Raffleitemfile_Core($this->file_path.'inimigo.txt', 1, 'FILE'))->getRaffleItens();
		} else if ($rolagem >= 23 && $rolagem <= 30) {
			$amigos_inimigos1 = (new Raffleitemfile_Core($this->file_path.'inimigo.txt', 1, 'FILE'))->getRaffleItens();
			$amigos_inimigos2 = (new Raffleitemfile_Core($this->file_path.'amigo.txt', 1, 'FILE'))->getRaffleItens();
			$amigos_inimigos = "Amigo:{$amigos_inimigos1}<br>Inimigo:{$amigos_inimigos2}";
		}
		return "{$tipo} {$amigos_inimigos}";
	}

	public function origem_relacao_amizade(){
		return (new Raffleitemfile_Core($this->file_path.'origem_relacao_amizade.txt', 1, 'FILE'))->getRaffleItens();
	}
	
	public function causas_inimizade(){
		return (new Raffleitemfile_Core($this->file_path.'inimizade.txt', 1, 'FILE'))->getRaffleItens();
	}

	public function consequencias_inimizade(){
		return (new Raffleitemfile_Core($this->file_path.'inimizade_consequencias.txt', 1, 'FILE'))->getRaffleItens();
	}

	public function vida_amorosa(){
		$rolagem = mt_rand(1, 30);
		$vida_amorosa = '';
		if ($rolagem <= 5) {
			$vida_amorosa = 'Você é um solitário inveterado. Nunca se interessou ou nunca teve ninguém.';
		} else if ($rolagem >= 6 && $rolagem <= 10) {
			$vida_amorosa = 'Apenas romances passageiros e pouco dignos de nota';
		} else if ($rolagem >= 11 && $rolagem <= 20) {
			$vida_amorosa = (new Raffleitemfile_Core($this->file_path.'amor.txt', 1, 'FILE'))->getRaffleItens();
		} else if ($rolagem >= 21 && $rolagem <= 30) {
			$vida_amorosa = 'Apenas um, mas um caso feliz e duradouro.';
		}
		return $vida_amorosa;
	}

	public function inspiracao(){
		return (new Raffleitemfile_Core($this->file_path.'razao_de_viver.txt', 1, 'FILE'))->getRaffleItens();
	}

	public function reliquia(){
		return (new Raffleitemfile_Core($this->file_path.'reliquia.txt', 1, 'FILE'))->getRaffleItens();
	}

	public function nao_gosta(){
		$rolagem = mt_rand(1, 30);
		if ($rolagem == 30) {
			$nao_gosta = (new Raffleitemfile_Core($this->file_path.'nao_gosta.txt', 2, 'FILE'))->getRaffleItens();
			return "{$nao_gosta[0]} e {$nao_gosta[1]}";
		} else {
			return (new Raffleitemfile_Core($this->file_path.'nao_gosta.txt', 1, 'FILE'))->getRaffleItens();
		}
	}

	public function medo(){
		$rolagem = mt_rand(1, 30);
		if ($rolagem == 30) {
			$medo = (new Raffleitemfile_Core($this->file_path.'medo.txt', 2, 'FILE'))->getRaffleItens();
			return "{$medo[0]} e {$medo[1]}";
		} else {
			return (new Raffleitemfile_Core($this->file_path.'medo.txt', 1, 'FILE'))->getRaffleItens();
		}	
	}

	public function opiniao(){
		return (new Raffleitemfile_Core($this->file_path.'opiniao.txt', 1, 'FILE'))->getRaffleItens();
	}

	public function arrependimentos(){
		return (new Raffleitemfile_Core($this->file_path.'arrependimentos.txt', 1, 'FILE'))->getRaffleItens();
	}

	public function costumes(){
		return (new Raffleitemfile_Core($this->file_path.'habitos_e_manias.txt', 1, 'FILE'))->getRaffleItens();
	}

	public function vida_aventureiro(){
		return (new Raffleitemfile_Core($this->file_path.'vida_aventureiro.txt', 1, 'FILE'))->getRaffleItens();
	}

	public function feito(){
		return (new Raffleitemfile_Core($this->file_path.'feito_notavel.txt', 1, 'FILE'))->getRaffleItens();
	}

	public function criaturas_odiadas(){
		$rolagem = mt_rand(1, 30);
		$criaturas_odiadas = '';
		$tipo = '';
		if ($rolagem <= 5) {
			$criaturas_odiadas = 'Você não sente ódio por nenhuma criatura em espacial.';
		} else if ($rolagem >= 6 && $rolagem <= 20) {
			$criatura = (new Raffleitemfile_Core($this->file_path.'criaturas_odiadas.txt', 1, 'FILE'))->getRaffleItens();
			$motivo = (new Raffleitemfile_Core($this->file_path.'motivos_odiar_criaturas.txt', 1, 'FILE'))->getRaffleItens();
			$criaturas_odiadas = "Uma criatura do tipo: <b>{$criatura}</b> conseguiu atrair o seu ódio. <b>Motivo</b>: {$motivo}";
		} else if ($rolagem >= 21 && $rolagem <= 28) {
			$criatura = (new Raffleitemfile_Core($this->file_path.'criaturas_odiadas.txt', 2, 'FILE'))->getRaffleItens();
			$motivo = (new Raffleitemfile_Core($this->file_path.'motivos_odiar_criaturas.txt', 2, 'FILE'))->getRaffleItens();
			$criaturas_odiadas = "Duas espécies de criaturas conseguiram atrair o seu ódio. Estas criaturas são:<br>";
			$criaturas_odiadas .= "<b>{$criatura[0]}</b> - <b>Motivo</b>: {$motivo[0]}<br>";
			$criaturas_odiadas .= "<b>{$criatura[1]}</b> - <b>Motivo</b>: {$motivo[1]}";
		} else if ($rolagem >= 29 && $rolagem <= 30) {
			$criatura = (new Raffleitemfile_Core($this->file_path.'criaturas_odiadas.txt', 3, 'FILE'))->getRaffleItens();
			$motivo = (new Raffleitemfile_Core($this->file_path.'motivos_odiar_criaturas.txt', 3, 'FILE'))->getRaffleItens();
			$criaturas_odiadas = "Três espécies de criaturas conseguiram atrair o seu ódio. Estas criaturas são:<br>";
			$criaturas_odiadas .= "<b>{$criatura[0]}</b> - <b>Motivo</b>: {$motivo[0]}<br>";
			$criaturas_odiadas .= "<b>{$criatura[1]}</b> - <b>Motivo</b>: {$motivo[1]}<br>";
			$criaturas_odiadas .= "<b>{$criatura[2]}</b> - <b>Motivo</b>: {$motivo[2]}";
		}

		return $criaturas_odiadas;	
	}

	public function habilidades_inuteis(){
		$rolagem = mt_rand(1, 30);
		if ($rolagem == 30) {
			$habilidade = (new Raffleitemfile_Core($this->file_path.'habilidades_inuteis.txt', 2, 'FILE'))->getRaffleItens();
			$habilidade = "{$habilidade[0]} e {$habilidade[1]}";
		} else {
			$habilidade = (new Raffleitemfile_Core($this->file_path.'habilidades_inuteis.txt', 1, 'FILE'))->getRaffleItens();	
		}

		return $habilidade;
	}

	public function ultima_frase(){
		return (new Raffleitemfile_Core($this->file_path.'frases_antes_de_morrer.txt', 1, 'FILE'))->getRaffleItens();	
	}
}