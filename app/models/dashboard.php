<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2018
	Model: dashboard
*/

class Dashboard_Model extends Dbrecord_Core {
    
    public function compare_data($ativado, $plano){
    	if (!empty($ativado) && $plano < 7) {
    		$tempo = $this->configurar_tempo_por_plano($plano);
    	    $ativado = str_replace('/', "-", $ativado);
            $agora = strtotime("now");
            $data_final_seg = strtotime($tempo, strtotime($ativado));
            $data_final = date('d/m/Y H:i', strtotime($tempo, strtotime($ativado)));  
            if (($agora) > $data_final_seg) {
        	    echo '<td class="ilicito">'.$data_final.'</td>';
            } else {
        	    echo '<td class="on">'.$data_final.'</td>';
            }
    	} else {
    		echo '<td></td>';
    	}
    }

    public function configurar_tempo_por_plano($plano){
        $time = '';
        switch ($plano) {
        	case 1:$time = '+1 month';
        	break;
        	case 2:$time = '+6 month';
        	break;
        	case 3:$time = '+1 year';
        	break;
        	case 4:$time = '+1 month';
        	break;
        	case 5:$time = '+6 month';
        	break;
        	case 6:$time = '+1 year';
        	break;
        }
        return $time;
    }

    public function planos($id){
    	$plano = '';
        switch ($id) {
        	case 1:$plano = '<b>UM</b><br>R$ 3,00';
        	break;
        	case 2:$plano = '<b>US</b><br>R$ 17,10';
        	break;
        	case 3:$plano = '<b>UA</b><br>R$ 32,40';
        	break;
        	case 4:$plano = '<b>GM</b><br>R$ 15,00';
        	break;
        	case 5:$plano = '<b>GS</b><br>R$ 85,50';
        	break;
        	case 6:$plano = '<b>GA</b><br>R$ 162,00';
        	break;
        	case 7:$plano = '<b>Free</b>';
        	break;
        }
        return $plano;
    }
}