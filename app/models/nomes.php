<?php

class Nomes_Model {
	
	public $names;
	public $file_path;

	function __construct(){
		$this->file_path = PATH_BASE.'config/locale/pt-br/txt/nomes/';
		$this->get_files_names();
	}

	public function get_files_names(){
		$dir = new DirectoryIterator($this->file_path);
		$names = [];
		foreach ($dir as $fileInfo) {
			if ($fileInfo->getFilename() != '.' && $fileInfo->getFilename() != '..') {
			    $name = $fileInfo->getFilename();
			    $names[] = $name;
			}
		}
		
		$new_names = [];
		foreach ($names as $key => $value) {
			$new_names[] = ucfirst(str_replace('_', ' ', substr($value, 0, -4)));
		}

		$this->names = $new_names;
	}
}