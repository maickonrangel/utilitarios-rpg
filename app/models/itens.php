<?php

class Itens_Model {

	public $file_path;

	function __construct(){
		$this->file_path = PATH_BASE.'config/locale/pt-br/txt/itens/';
	}

	function aneis_magicos($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'aneis_magicos.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function armaduras($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'armaduras.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function armaduras_magicas($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'armaduras_magicas.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function armas_comuns($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'armas_comuns.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function armas_magicas($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'armas_magicas.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function bugigangas($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'bugigangas.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function habilidade_magica($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'habilidade_magica.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function itens_adversos($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'itens_adversos.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function itens_estupidos($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'itens_estupidos.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function itens_magicos_ded5e_comuns($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'itens_magicos_ded5e_comuns.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function itens_magicos_ded5e_incomun($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'itens_magicos_ded5e_incomun.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function itens_magicos_ded5e_lendarios($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'itens_magicos_ded5e_lendarios.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function itens_magicos_ded5e_muito_raros($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'itens_magicos_ded5e_muito_raros.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function itens_magicos_ded5e_raros($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'itens_magicos_ded5e_raros.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function itens_magicos_ded5e_tabela_a($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'itens_magicos_ded5e_tabela-a.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function itens_magicos_ded5e_tabela_b($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'itens_magicos_ded5e_tabela-b.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function itens_magicos_ded5e_tabela_c($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'itens_magicos_ded5e_tabela-c.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function itens_magicos_ded5e_tabela_d($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'itens_magicos_ded5e_tabela-d.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function itens_magicos_ded5e_tabela_e($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'itens_magicos_ded5e_tabela-e.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function itens_magicos_ded5e_tabela_f($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'itens_magicos_ded5e_tabela-f.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function itens_magicos_ded5e_tabela_g($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'itens_magicos_ded5e_tabela-g.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function itens_magicos_ded5e_tabela_h($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'itens_magicos_ded5e_tabela-h.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function itens_magicos_ded5e_tabela_i($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'itens_magicos_ded5e_tabela-i.txt', $qtd, 'FILE'))->getRaffleItens();
	}
	public function caracteristicas_especiais(){
		$qtd = 1;
		$historia = (new Raffleitemfile_Core($this->file_path.'caracteristicas_especiais_de_itens/historia.txt', $qtd, 'FILE'))->getRaffleItens();

		$propriedade_menor = (new Raffleitemfile_Core($this->file_path.'caracteristicas_especiais_de_itens/propriedade_menor.txt', $qtd, 'FILE'))->getRaffleItens();

		$quem_criou = (new Raffleitemfile_Core($this->file_path.'caracteristicas_especiais_de_itens/quem_criou.txt', $qtd, 'FILE'))->getRaffleItens();

		$sofisma = (new Raffleitemfile_Core($this->file_path.'caracteristicas_especiais_de_itens/sofisma.txt', $qtd, 'FILE'))->getRaffleItens();

		$historia = explode(':',$historia);
		$propriedade_menor = explode(':',$propriedade_menor);
		$quem_criou = explode(':',$quem_criou);
		$sofisma = explode(':',$sofisma);
		
		$caracteristicas = [
			'historia' => ['nome'=>$historia[0], 'desc'=>$historia[1]],
			'propriedade_menor' => ['nome'=>$propriedade_menor[0], 'desc'=>$propriedade_menor[1]],
			'quem_criou' => ['nome'=>$quem_criou[0], 'desc'=>$quem_criou[1]],
			'sofisma' => ['nome'=>$sofisma[0], 'desc'=>$sofisma[1]]
		];

		return $caracteristicas;
	}

	public function rolar4d6_descartando_o_menor(){
		$rolagens = [mt_rand(1,6), mt_rand(1,6), mt_rand(1,6), mt_rand(1,6)]; 
		$total = 0;
		$menor = 6;
		foreach ($rolagens as $key => $value) {
			$total += $value;
			if ($value < $menor) {
				$menor = $value;
			}
		}

		$rolagem_final = ($total - $menor);

		if ($rolagem_final < 10) {
			$rolagem_final = mt_rand(10,11);
		}
		return $rolagem_final;
	}

	public function item_inteligente(){
		$qtd = 1;
		$int = $this->rolar4d6_descartando_o_menor();
		$sab = $this->rolar4d6_descartando_o_menor();
		$car = $this->rolar4d6_descartando_o_menor();

		$defeito_ou_sefredo = (new Raffleitemfile_Core($this->file_path.'item_inteligente/defeito-ou-segredos.txt', $qtd, 'FILE'))->getRaffleItens();

		$maneirismo = (new Raffleitemfile_Core($this->file_path.'item_inteligente/maneirismo.txt', $qtd, 'FILE'))->getRaffleItens();

		$comunicacao = (new Raffleitemfile_Core($this->file_path.'item_inteligente/comunicacao.txt', $qtd, 'FILE'))->getRaffleItens();

		$sentidos = (new Raffleitemfile_Core($this->file_path.'item_inteligente/sentidos.txt', $qtd, 'FILE'))->getRaffleItens();

		$tracos_de_interacao = (new Raffleitemfile_Core($this->file_path.'item_inteligente/tracos-de-interacao.txt', $qtd, 'FILE'))->getRaffleItens();

		$vinculos = (new Raffleitemfile_Core($this->file_path.'item_inteligente/vinculos.txt', $qtd, 'FILE'))->getRaffleItens();

		$proposito_especial = (new Raffleitemfile_Core($this->file_path.'item_inteligente/proposito_especial.txt', $qtd, 'FILE'))->getRaffleItens();

		$confilito = (new Raffleitemfile_Core($this->file_path.'item_inteligente/confilito.txt', $qtd, 'FILE'))->getRaffleItens();

		$item = [
			'habilidades' => ['int'=>$int,'sab'=>$sab,'car'=>$car],
			'defeito_ou_segredo' => $defeito_ou_sefredo,
			'maneirismo'=>$maneirismo,
			'comunicacao'=>$comunicacao,
			'sentidos'=>$sentidos,
			'tracos_de_interacao'=>$tracos_de_interacao,
			'vinculos'=>$vinculos,
			'proposito_especial'=>$proposito_especial,
			'confilito'=>$confilito
		];

		return $item;
	}
}