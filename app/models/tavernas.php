<?php
class Tavernas_Model {
	
	public $file_path;

	function __construct(){
		$this->file_path = PATH_BASE.'config/locale/pt-br/txt/taverna/';
		$this->sexo = array_rand([0,1]);
	}

	function taverna_nome_taverneiro(){
		if($this->sexo == 0):
			$nome = (new Raffleitemfile_Core(PATH_BASE.'config/locale/pt-br/txt/nomes/humanos-masculino.txt', 1))->getRaffleItens();
		else:
			$nome = (new Raffleitemfile_Core(PATH_BASE.'config/locale/pt-br/txt/nomes/humanos-feminino.txt', 1))->getRaffleItens();
		endif;
		return $nome;
	}

	function taverna_bar_nome(){
		return (new Raffleitemfile_Core($this->file_path.'bar-nomes.txt', 1, 'FILE'))->getRaffleItens();
	}

	function taverna_aparencia_taverneiro($qtd){
		$sexo = 'M'?($this->sexo == 0): 'F';
		return (new Raffleitemfile_Core($this->file_path.'aparencias.txt', $qtd, 'FILE', $sexo))->getRaffleItens();
	}

	function taverna_idade_taverneiro($racas){
		$idade = [
					'Humano'	=> mt_rand(15,100),
					'Elfo'		=> mt_rand(35,600),
					'Gnomo' 	=> mt_rand(85,1200),
					'Gnoma'		=> mt_rand(85,1200),
					'Meio elfo' => mt_rand(30,400),
					'Meio orc' 	=> mt_rand(15,500),
					'Halfling' 	=> mt_rand(10,85),
					'Humana'	=> mt_rand(15,100),
					'Anã'		=> mt_rand(15,100),
					'Anão'		=> mt_rand(15,100),
					'Elfa'		=> mt_rand(35,600),
					'Gnoma'		=> mt_rand(85,1200),
					'Meio elfa'	=> mt_rand(30,400)
				];

		return $idade[$racas];
	}

	function taverna_tempo_de_profissao($idade){
		$experiencia = ['anos', 'meses', 'dias'];
		$divisor = mt_rand(2,4);
		$tempo = $experiencia[mt_rand(0,2)];
		$carreira = intval($idade/$divisor);
		if($carreira == 1)
			$carreira = 2;
		return 'O taverneiro tem' ." {$carreira} {$tempo} ". 'de experiência.';

	}

	function taverna_raca_taverneiro(){
		$sexo = 'M'?($this->sexo == 0): 'F';
		return (new Raffleitemfile_Core($this->file_path.'racas.txt', 1, 'FILE', $sexo))->getRaffleItens();
	}

	function taverna_personalidade_taverneiro($qtd){
		$sexo = 'M'?($this->sexo == 0): 'F';
		return (new Raffleitemfile_Core($this->file_path.'personalidades.txt', $qtd, 'FILE', $sexo))->getRaffleItens();
	}

	function taverna_carnes($qtd){
		return (new Raffleitemfile_Core($this->file_path.'carnes.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function taverna_frutas($qtd){
		return (new Raffleitemfile_Core($this->file_path.'frutas.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function taverna_legumes($qtd){
		return (new Raffleitemfile_Core($this->file_path.'legumes.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function taverna_verduras($qtd){
		return (new Raffleitemfile_Core($this->file_path.'verduras.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function taverna_bebidas($qtd){
		return (new Raffleitemfile_Core($this->file_path.'bebidas.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function taverna_objetos_de_briga($qtd){
		return (new Raffleitemfile_Core($this->file_path.'objetos_na_hora_da_briga.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function taverna_atracao_conteporanea($qtd){
		return (new Raffleitemfile_Core($this->file_path.'atracao-comteporanea.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function taverna_artista($qtd = 1){
		return (new Raffleitemfile_Core($this->file_path.'artistas.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function taverna_atracao_medieval($qtd){
		return (new Raffleitemfile_Core($this->file_path.'atracao-medieval.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function taverna_bebidas_bar_ou_restaurante($qtd){
		return (new Raffleitemfile_Core($this->file_path.'bebidas_de_bar_ou_restaurante.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function taverna_bebidas_simples($qtd){
		return (new Raffleitemfile_Core($this->file_path.'bebidas_simples.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function taverna_cervejas($qtd){
		return (new Raffleitemfile_Core($this->file_path.'cervejas.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function taverna_porcoes($qtd){
		return (new Raffleitemfile_Core($this->file_path.'porcoes.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function taverna_pratos($qtd){
		return (new Raffleitemfile_Core($this->file_path.'pratos.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function taverna_sobremesas($qtd){
		return (new Raffleitemfile_Core($this->file_path.'sobremesas.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function taverna_sucos($qtd){
		return (new Raffleitemfile_Core($this->file_path.'sucos.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function taverna_petiscos($qtd){
		return (new Raffleitemfile_Core($this->file_path.'petiscos.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function taverna_garcon($qtd){
		return (new Raffleitemfile_Core($this->file_path.'garcons_personalidade.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function taverna_restaurante_nome(){
		return (new Raffleitemfile_Core($this->file_path.'restaurantes.txt', 1, 'FILE'))->getRaffleItens();
	}

	function taverna_restaurante_pizzas(){
		return (new Raffleitemfile_Core($this->file_path.'pizzas.txt', 1, 'FILE'))->getRaffleItens();
	}

	function taverna_pub_nome(){
		return (new Raffleitemfile_Core($this->file_path.'pub_nomes.txt', 1, 'FILE'))->getRaffleItens();
	}

	function taverna_nome(){
		$tipo = ['Taverna','Estalagem do'];
		$nome = $tipo[mt_rand(0, count($tipo)-1)];
		$sexo = ($this->sexo == 0)?'M': 'F';

		//pq = [personagem][qualidade]
		//oo = [objeto][objeto]
		//pp = [personagem][personagem]
		//oq = [objeto][qualidade]

		$nome_taverna = [];
		$forma = ['pq','oo','pp','oq','np'];
		$forma_escolhida = $forma[mt_rand(0, count($forma)-1)];
	
		switch($forma_escolhida):
			case 'pq': 
				$nome_taverna[] = (new Raffleitemfile_Core($this->file_path.'personagem.txt', 1, 'FILE', $sexo))->getRaffleItens();
				$nome_taverna[]	= (new Raffleitemfile_Core($this->file_path.'qualidade_personagem.txt', 1, 'FILE', $sexo))->getRaffleItens();
			break;

			case 'oo':
				$nome_taverna[] = (new Raffleitemfile_Core($this->file_path.'objetos.txt', 1, 'FILE'))->getRaffleItens();
				$nome_taverna[] = (new Raffleitemfile_Core($this->file_path.'objetos.txt', 1, 'FILE'))->getRaffleItens();
			break;

			case 'pp':
				$nome_taverna[] = (new Raffleitemfile_Core($this->file_path.'personagem.txt', 1, 'FILE', $sexo))->getRaffleItens();
				$nome_taverna[] = (new Raffleitemfile_Core($this->file_path.'personagem.txt', 1, 'FILE', $sexo))->getRaffleItens();
			break;

			case 'oq':
				$nome_taverna[] = (new Raffleitemfile_Core($this->file_path.'objetos.txt', 1, 'FILE'))->getRaffleItens();
				$nome_taverna[]	= (new Raffleitemfile_Core($this->file_path.'qualidade_objetos.txt', 1, 'FILE'))->getRaffleItens();
			break;

			case 'np':
				$nome_taverna[] = (new Raffleitemfile_Core($this->file_path.'tavernas_nomes.txt', 1, 'FILE'))->getRaffleItens();
			break;
		endswitch;

		
		$taverna = "{$nome} ";
		for($i=0; $i<count($nome_taverna); $i++):
			if($forma_escolhida != 'np'):
				if($i == 0 and stristr(substr($nome_taverna[0],2),'a') || stristr(substr($nome_taverna[0],2),'ã') ):
					$taverna = 'Estalagem da ';
				elseif($i == 0 and stristr(substr($nome_taverna[0],2),'o') || stristr(substr($nome_taverna[0],2),'ã') ):
					$taverna = 'Estalagem do ';
				elseif($i == 0 and stristr(substr($nome_taverna[0],2),'s') && stristr(substr($nome_taverna[0],3),'a') ):
					$taverna = 'Estalagem das ';
				endif;
			endif;
			$taverna .= "{$nome_taverna[$i]} ";
		endfor;

		return $taverna;
	}
}