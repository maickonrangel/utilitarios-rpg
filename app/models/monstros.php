<?php

class Monstros_Model {
	
	public $file_path;

	function __construct(){
		$this->file_path = PATH_BASE.'config/locale/pt-br/txt/livro_do_mestre/monstros/';
		$this->ambientes = [
				'artico' => 'Ártico',
				'colina' => 'Colina',
				'costa' => 'Costa',
				'deserto' => 'Deserto',
				'floresta' => 'Floresta',
				'montanha' => 'Montanha',
				'pantano' => 'Pântano',
				'planice' => 'Planice',
				'subaquatico' => 'Subaquático',
				'subterraneo' => 'Subterrâneo',
				'urbano' => 'Urbano'
			];
	}

	public function sortear($arquivo, $qtd=1){
		$arquivo = explode("\n", file_get_contents($this->file_path.$arquivo.'.txt'));
		foreach ($arquivo as $key => $value) {
			$linha = explode('|',$value);
			$monstros[] = ['nome' => $linha[0], 'nd' => $linha[1], 'xp' => $linha[2]];
		}

		$escolhidos = array_rand($monstros, 10);
		$monstros_sorteados = [];
		foreach ($escolhidos as $key => $value) {
			$monstros_sorteados[] = $monstros[$value];
		}
		return $monstros_sorteados;
	}

	public function artico($type, $qtd=1){
		return $this->sortear($type, $qtd);
	}

	public function colina($type, $qtd=1){
		return $this->sortear($type, $qtd);
	}

	public function costa($type, $qtd=1){
		return $this->sortear($type, $qtd);
	}

	public function deserto($type, $qtd=1){
		return $this->sortear($type, $qtd);
	}

	public function floresta($type, $qtd=1){
		return $this->sortear($type, $qtd);
	}

	public function montanha($type, $qtd=1){
		return $this->sortear($type, $qtd);
	}

	public function pantano($type, $qtd=1){
		return $this->sortear($type, $qtd);
	}

	public function planice($type, $qtd=1){
		return $this->sortear($type, $qtd);
	}

	public function subaquatico($type, $qtd=1){
		return $this->sortear($type, $qtd);
	}

	public function subterraneo($type, $qtd=1){
		return $this->sortear($type, $qtd);
	}

	public function urbano($type, $qtd=1){
		return $this->sortear($type);
	}
}