<?php

class Sorte_Model {
	
	public $file_path;

	function __construct(){
		$this->file_path = PATH_BASE.'config/locale/pt-br/txt/sorte/';
	}

	public function acontecimento($qtd){
		return (new Raffleitemfile_Core($this->file_path.'acontecimento.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	public function afetado($qtd){
		return (new Raffleitemfile_Core($this->file_path.'sorte-afetado.txt', $qtd, 'FILE'))->getRaffleItens();
	}
}