<?php

class Ferramentasded5e_Model {

	public $file_path;

	function __construct(){
		$this->file_path = PATH_BASE.'config/locale/pt-br/txt/livro_do_mestre/';
	}

	function acao_de_vilao_baseado_em_eventos($qtd = 1){
		return (new Raffleitemfile_Core($this->file_path.'acao_de_vilao_baseado_em_eventos.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function aliados($qtd = 1){
		return (new Raffleitemfile_Core($this->file_path.'aliados.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function aventuras_em_eventos_fechados($qtd = 1){
		return (new Raffleitemfile_Core($this->file_path.'aventuras_em_eventos_fechados.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function climax_da_aventura($qtd = 1){
		return (new Raffleitemfile_Core($this->file_path.'climax_da_aventura.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function dilemas_morais($qtd = 1){
		return (new Raffleitemfile_Core($this->file_path.'dilemas_morais.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function encontros_em_florestas_silvestres($qtd = 1){
		return (new Raffleitemfile_Core($this->file_path.'encontros_em_florestas_silvestres.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function eventos($qtd = 1){
		return (new Raffleitemfile_Core($this->file_path.'eventos.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function introducao_de_aventura($qtd = 1){
		return (new Raffleitemfile_Core($this->file_path.'introducao_de_aventura.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function missoes_secundarias($qtd = 1){
		return (new Raffleitemfile_Core($this->file_path.'missoes_secundarias.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function objetivos_ambiente_selvagem($qtd = 1){
		return (new Raffleitemfile_Core($this->file_path.'objetivos_ambiente_selvagem.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function objetivos_baseado_em_eventos($qtd = 1){
		return (new Raffleitemfile_Core($this->file_path.'objetivos_baseado_em_eventos.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function objetivos_na_masmorra($qtd = 1){
		return (new Raffleitemfile_Core($this->file_path.'objetivos_na_masmorra.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function outros_objetivos($qtd = 1){
		return (new Raffleitemfile_Core($this->file_path.'outros_objetivos.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function patronos_da_aventura($qtd = 1){
		return (new Raffleitemfile_Core($this->file_path.'patronos_da_aventura.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function viloes_da_aventura($qtd = 1){
		$vilao = (new Raffleitemfile_Core($this->file_path.'viloes_da_aventura.txt', $qtd, 'FILE'))->getRaffleItens();
		$fraqueza = (new Raffleitemfile_Core($this->file_path.'/vilao/fraquezas_do_vilao.txt', $qtd, 'FILE'))->getRaffleItens();
		$metodos = (new Raffleitemfile_Core($this->file_path.'/vilao/metodos_do_vilao.txt', mt_rand(1,3), 'FILE'))->getRaffleItens();
		$trama = (new Raffleitemfile_Core($this->file_path.'/vilao/trama_do_vilao.txt', $qtd, 'FILE'))->getRaffleItens();

		$viloes = [
			'vilão' => $vilao,
			'fraqueza' => $fraqueza,
			'métodos' => $metodos,
			'trama' => $trama
		];

		return $viloes;
	}

	function farreando($qtd = 1){
		return (new Raffleitemfile_Core($this->file_path.'farreando.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function complicacao_perseguicao_urbana($qtd = 1){
		return (new Raffleitemfile_Core($this->file_path.'complicacao_perseguicao_urbana.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function complicacao_perseguicao_na_natureza($qtd = 1){
		return (new Raffleitemfile_Core($this->file_path.'complicacao_perseguicao_na_natureza.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function venenos($qtd = 1){
		return (new Raffleitemfile_Core($this->file_path.'venenos.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function ferimento_persistente($qtd = 1){
		return (new Raffleitemfile_Core($this->file_path.'ferimento_persistente.txt', $qtd, 'FILE'))->getRaffleItens();
	}
}