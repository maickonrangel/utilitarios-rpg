<?php


class Fichasded35_Model{

	public $nivel;
	public $tendencia;
	public $deslocamento;
	public $tamanho;
	public $classes;
	public $classe;
	public $fortitude;
	public $reflexos;
	public $vontade;
	public $pericias;
	public $ca;

	public $tabela_de_classe;
	public $tabela_de_armas;
	public $tabela_de_armaduras;
	public $tabela_de_escudos;
	public $tabela_de_talentos_combatente;
	public $tabela_de_talentos_conjurador;
	public $talentos_selecionados;
	
	public $magias_selecionadas;
	public $magias_conhecidas;

	public $armas_equipadas;
	public $escudos_equipados;
	public $armaduras_equipadas;
	public $habilidades_basicas;
	public $nome;
	public $raca;
	public $descricao_racial;
	public $ataque_distancia;
	public $ataque_corporal;

	public function __construct($classe = null, $raca = null, $nivel = null){
		if ($classe == null) {
			$classe = $this->classe();
		}

		$this->classe = $classe;
		$this->classes_config();
		$this->habilidades_basicas();
		$this->nivel($nivel);
		$this->pv($classe);
		$this->tendencia($classe);
		$this->tamanho_e_deslocamento();
		$this->habilidades_de_classe($classe);
		$this->arma();
		$this->armadura();
		$this->escudos();
		$this->testes_de_resistencia();
		$this->talentos();
		$this->pericias($classe);
		$this->armas_equipadas();
		$this->armadura_equipada();
		$this->escudo_equipado();
		$this->bonus_de_ataque_corpo_a_corpo_e_distancia();
		$this->ca($classe);
		$this->nome();
		$this->raca($raca);
		$this->ajustes_raciais();
		$this->talentos_de_classe_combatente();
		$this->talentos_de_classe_conjurador();
		$this->investir_pts_habilidade_chave($classe);
		$this->magias($classe);
		$this->selecionar_talentos_de_classe($classe);
	}

	public function nome(){
		$this->nome = (new Raffleitemfile_Core(PATH_BASE.'config/locale/pt-br/txt/nomes/neerlandes.txt', 1, 'FILE'))->getRaffleItens();
	}

	public function classe(){
		$classes = [
			'barbaro', 	
			'bardo', 	
			'clerigo', 	
			'druida', 	
			'feiticeiro',
			'guerreiro', 
			'ladino', 	
			'mago' ,		
			'monge', 	
			'paladino', 	
			'ranger'
		];
		$escolhido = array_rand($classes);
		return $classes[$escolhido];
	}

	public function raca($raca = null){
		if ($raca == null) {
			$racas = ['Humano','Anão','Elfo','Gnomo','Meio-Elfo','Meio-Orc','Halfling'];
			$escolhido = array_rand($racas);
			$this->raca = $racas[$escolhido];
		} else {
			$this->raca = $raca;
		}
	}

	public function ajustes_raciais(){
		switch ($this->raca) {
			case 'Humano':
				$this->descricao_racial = '+1 talento add, +4 na perícia e +1 por nível (Não ajustados na ficha)';
			break;

			case 'Anão':
				$this->habilidades_basicas['con'] += 2;
				$this->habilidades_basicas['car'] -= 2;
				$this->descricao_racial = 'Visão no Escuro 18m, Desl. base 6m, Ligação com Pedras, Familiaridade com Armas, Estabilidade, +2 contra venenos, +2 TR contra magias ou efeitos similares,+1 de bônus racial nas jogadas de ataque contra orcs e goblinóides, +4 de bônus de esquiva na CA contra monstros do tipo gigante, +2 de bônus racial nos testes de Avaliação relacionados a objetos de metal ou pedra e +2 de bônus racial nos testes de Ofícios relacionados a objetos de metal ou pedra. ';
			break;

			case 'Elfo':
				$this->habilidades_basicas['des'] += 2;
				$this->habilidades_basicas['con'] -= 2;
				$this->descricao_racial = 'Imunidade à magias e efeitos de sono e +2 de bônus racial nos testes de resistência contra magias ou efeitos similares de Encantamento, Visão na Penumbra e +2 de Bônus racial nos testes de Ouvir, Procurar e Observar.';
			break;

			case 'Gnomo':
				$this->habilidades_basicas['con'] += 2;
				$this->habilidades_basicas['for'] -= 2;
				$this->descricao_racial = 'Tamanho Pequeno: +1 de bônus de tamanho na Classe de Armadura, +1 de bônus de tamanho nas jogadas de ataque e +4 de bônus de tamanho nos testes de Esconder-se, Desl. base 6m, Visão na Penumbra, +2 de bônus racial nos testes de resistência contra ilusões, +1 de bônus na Classe de Dificuldade dos testes de resistência contra as ilusões conjuradas por um gnomo, +1 de bônus racial nas jogadas de ataque contra kobolds e goblinóides, +4 de bônus de esquiva na CA contra monstros do tipo gigante, +2 de bônus racial nos testes de Ouvir,+2 de bônus racial nos testes de Ofícios (alquimia) e Habilidades Similares à Magia: l/dia – falar com animais (somente mamíferos terrestres, 1 minuto de duração). Um gnomo com Carisma 10, no mínimo, também possui as seguintes habilidades similares à magia: l/dia – globos de luz, som fantasma, prestidigitação. Nível de conjurador: 1° nível; teste de resistência CD 10 + modificador de Carisma + nível da magia.';
			break;

			case 'Meio-Elfo':
				$this->descricao_racial = 'Imunidade à magias e efeitos de sono e +2 de bônus racial nos testes de resistência contra magias ou efeitos similares de Encantamento, Visão na Penumbra, +1 de bônus racial nos testes de Ouvir, Procurar e Observar, +2 de bônus racial nos testes de Diplomacia e Obter Informação e Sangue Élfico. ';
			break;

			case 'Meio-Orc':
				$this->habilidades_basicas['for'] += 2;
				$this->habilidades_basicas['int'] -= 2;
				$this->habilidades_basicas['car'] -= 2;
				$this->descricao_racial = 'Visão no Escuro 18m e Sangue orc';
			break;

			case 'Halfling':
				$this->habilidades_basicas['des'] += 2;
				$this->habilidades_basicas['for'] -= 2;
				$this->descricao_racial = 'Desl. base 6m, +2 de bônus racial nos testes de Escalar, Saltar e Furtividade, +1 de bônus racial em todos os testes de resistência, +2 de bônus de moral nos testes de resistência contra medo, +1 de bônus racial nas jogadas de ataque com armas de arremesso e fundas e +2 de bônus racial nos testes de Ouvir.';
			break;
		}
	}

	public function classes_config(){
		$this->classes = [
			'barbaro' 	=> ['nome'=>'Bárbaro','dv'=>12,'pts_pericia'=>4],
			'bardo' 	=> ['nome'=>'Bardo','dv'=>6,'pts_pericia'=>6],
			'clerigo' 	=> ['nome'=>'Clérigo','dv'=>8,'pts_pericia'=>2],
			'druida' 	=> ['nome'=>'Druida','dv'=>8,'pts_pericia'=>4],
			'feiticeiro'=> ['nome'=>'Feiticeiro','dv'=>4,'pts_pericia'=>2],
			'guerreiro' => ['nome'=>'Guerreiro','dv'=>10,'pts_pericia'=>2],
			'ladino' 	=> ['nome'=>'Ladino','dv'=>6,'pts_pericia'=>8],
			'mago' 		=> ['nome'=>'Mago','dv'=>4,'pts_pericia'=>2],
			'monge' 	=> ['nome'=>'Monge','dv'=>8,'pts_pericia'=>4],
			'paladino' 	=> ['nome'=>'Paladino','dv'=>10,'pts_pericia'=>2],
			'ranger' 	=> ['nome'=>'Ranger','dv'=>8,'pts_pericia'=>4]
		];
	}

	public function rolar4d6_descartando_o_menor(){
		$rolagens = [mt_rand(1,6), mt_rand(1,6), mt_rand(1,6), mt_rand(1,6)]; 
		$total = 0;
		$menor = 6;
		foreach ($rolagens as $key => $value) {
			$total += $value;
			if ($value < $menor) {
				$menor = $value;
			}
		}

		$rolagem_final = ($total - $menor);

		if ($rolagem_final < 10) {
			$rolagem_final = mt_rand(10,11);
		}
		return $rolagem_final;
	}

	public function habilidades_basicas(){
		$this->habilidades_basicas = [
			'for' => $this->rolar4d6_descartando_o_menor(),
			'des' => $this->rolar4d6_descartando_o_menor(),
			'con' => $this->rolar4d6_descartando_o_menor(),
			'int' => $this->rolar4d6_descartando_o_menor(),
			'sab' => $this->rolar4d6_descartando_o_menor(),
			'car' => $this->rolar4d6_descartando_o_menor(),
			'nul' => 0
		];
	}

	public function investir_pts_habilidade_chave($classe){
		$pts = intval($this->nivel/4);

		if ($classe == 'barbaro') {
			$hab = ['for','des','con'];
			$this->habilidades_basicas[$hab[array_rand($hab)]] += $pts;
		}

		if ($classe == 'bardo') {
			$hab = ['car','des','int'];
			$this->habilidades_basicas[$hab[array_rand($hab)]] += $pts;
		}

		if ($classe == 'clerigo') {
			$hab = ['car','con','sab'];
			$this->habilidades_basicas[$hab[array_rand($hab)]] += $pts;
		}

		if ($classe == 'druida') {
			$hab = ['sab','des'];
			$this->habilidades_basicas[$hab[array_rand($hab)]] += $pts;
		}

		if ($classe == 'feiticeiro') {
			$hab = ['car','des','con'];
			$this->habilidades_basicas[$hab[array_rand($hab)]] += $pts;
		}

		if ($classe == 'guerreiro') {
			$hab = ['for','des','con'];
			$this->habilidades_basicas[$hab[array_rand($hab)]] += $pts;
		}

		if ($classe == 'ladino') {
			$hab = ['des','int','sab'];
			$this->habilidades_basicas[$hab[array_rand($hab)]] += $pts;
		}

		if ($classe == 'mago') {
			$hab = ['int','des','con'];
			$this->habilidades_basicas[$hab[array_rand($hab)]] += $pts;
		}

		if ($classe == 'monge') {
			$hab = ['sab','des','for'];
			$this->habilidades_basicas[$hab[array_rand($hab)]] += $pts;
		}

		if ($classe == 'paladino') {
			$hab = ['sab','car','for'];
			$this->habilidades_basicas[$hab[array_rand($hab)]] += $pts;
		}

		if ($classe == 'ranger') {
			$hab = ['des','for','sab'];
			$this->habilidades_basicas[$hab[array_rand($hab)]] += $pts;
		}


	}

	public function nivel($nivel){
		if ($nivel != null) {
			$this->nivel = $nivel;
		} else {
			$this->nivel = mt_rand(1, 20);
		}
	}

	public function tendencia($classe){
		$tendencias = [
			'barbaro' 		=> ['NB','CB','LN','N','CN', 'NM', 'CM'],
			'bardo' 		=> ['NB','CB','LN','N','CN', 'NM', 'CM'],
			'clerigo' 		=> ['LB','NB','CB','LN','N','CN','LM', 'NM', 'CM'],
			'druida' 		=> ['NB','LN','N','CN', 'NM'],
			'feiticeiro' 	=> ['LB','NB','CB','LN','N','CN','LM', 'NM', 'CM'],
			'guerreiro' 	=> ['LB','NB','CB','LN','N','CN','LM', 'NM', 'CM'],
			'ladino' 		=> ['LB','NB','CB','LN','N','CN','LM', 'NM', 'CM'],
			'mago' 			=> ['LB','NB','CB','LN','N','CN','LM', 'NM', 'CM'],
			'monge' 		=> ['LB','LN','LM'],
			'paladino' 		=> ['LB','LB']
		];
			
		$indice = array_rand($tendencias[$classe]);
		$this->tendencia = $tendencias[$classe][$indice];
	}

	public function tamanho_e_deslocamento(){
		$tamanhos = ['Pequeno','Médio','Grande'];
		$deslocamentos = [6,9,12];
		$indice = array_rand($tamanhos);
		$this->deslocamento = $deslocamentos[$indice];
		$this->tamanho = $tamanhos[$indice];
	}

	public function pv($classe){
		$dv = $this->classes[$classe]['dv'];
		$pv = $dv;
		for ($i=1; $i <= $this->nivel; $i++) { 
			if ($i == 1) {
				$pv = $dv + $this->modificador($this->habilidades_basicas['con']);
			} else {
				$pv += mt_rand(1,$dv) + $this->modificador($this->habilidades_basicas['con']);
			}
		}

		$this->pv = $pv;
	}

	public function talentos(){
		$qtd = intval(($this->nivel/3) + 1);
		$this->tabela_de_classe[$this->nivel-1]['especial'] .= "";
	}

	public function modificador($valor){
		if ($valor == 9 || $valor == 8) {
			return -1;
		} elseif ($valor == 7 || $valor == 6) {
			return -2;
		} elseif ($valor == 5 || $valor == 4) {
			return -3;
		} elseif ($valor == 3 || $valor == 2) {
			return -4;
		} elseif ($valor == 1 || $valor == 0) {
			return -5;
		} else {
			$mod = intval(($valor - 10)/2);
			if($mod > 0){
				return "+{$mod}";
			} else {
				return " {$mod}";
			}
		}
	}

	public function testes_de_resistencia(){
		$this->fortitude = $this->tabela_de_classe[$this->nivel-1]['fort']+$this->modificador($this->habilidades_basicas['con']);

		$this->reflexos = $this->tabela_de_classe[$this->nivel-1]['refl']+$this->modificador($this->habilidades_basicas['des']);

		$this->vontade = $this->tabela_de_classe[$this->nivel-1]['vont']+$this->modificador($this->habilidades_basicas['sab']);
	}

	public function ca($classe){
		$ca = 10;
		if ($classe == 'classe_monge' && $this->armaduras_equipadas == null) {
			$ca += $this->modificador($this->habilidades_basicas['sab']);
		}

		if ($this->armaduras_equipadas == null) {
			$ca += 3;
		}

		$ca += $this->modificador($this->habilidades_basicas['des']);

		if ($this->armaduras_equipadas != null) {
			$ca += $this->armaduras_equipadas['bonus_na_ca'];
		}

		if ($this->escudos_equipados != null) {
			$ca += $this->escudos_equipados['bonus_na_ca'];
		}

		$this->ca = $ca;
	}

	public function bonus_de_ataque_corpo_a_corpo_e_distancia(){
		$ataque_corporal = '';
		$ataque_distancia = '';
		if ($this->nivel > 5) {
			$bba = $this->tabela_de_classe[$this->nivel-1]['bba'];
			$bba = explode('/', $bba);
			foreach ($bba as $key => $value) {
				$atq_corporal = $value + $this->modificador($this->habilidades_basicas['for']);
				$atq_distancia = $value + $this->modificador($this->habilidades_basicas['des']);
				$ataque_corporal .= "+{$atq_corporal}/";
				$ataque_distancia .= "+{$atq_distancia}/";
			}
		} else {
			$ataque_corporal = $this->tabela_de_classe[$this->nivel-1]['bba']+$this->modificador($this->habilidades_basicas['for']);
			$ataque_distancia = $this->tabela_de_classe[$this->nivel-1]['bba']+$this->modificador($this->habilidades_basicas['des']);
			$ataque_corporal = "+{$ataque_corporal}";
			$ataque_distancia = "+{$ataque_distancia}";
		}

		$this->ataque_corporal = "{$ataque_corporal}";
		$this->ataque_distancia = "{$ataque_distancia}";
	}

	

	public function habilidades_de_classe($classe){
		$arquivo_de_classe = file_get_contents(PATH_BASE.'config/locale/pt-br/txt/ded3.5/classe_'.$classe.'.txt');
		$arquivo_de_classe = explode("\n", $arquivo_de_classe);
		foreach ($arquivo_de_classe as $key => $value) {
			$linha_de_habilidades[] = explode(' ', $value);
		}

		$tabela_de_classe = [];
		$linha['especial'] = '';

		foreach ($linha_de_habilidades as $key => $value) {
			foreach ($value as $key1 => $value1) {
				if ($key1 == 0) {
					$linha['nivel'] = $value1;		
				}

				if ($key1 == 1) {
					$linha['bba'] = str_replace('+', '', $value1);		
				}

				if ($key1 == 2) {
					$linha['fort'] = str_replace('+', '', $value1);		
				}

				if ($key1 == 3) {
					$linha['refl'] = str_replace('+', '', $value1);		
				}

				if ($key1 == 4) {
					$linha['vont'] = str_replace('+', '', $value1);		
				}

				if ($key1 > 4) {
					$linha['especial'] .= " {$value1}";		
				}
			}

			$tabela_de_classe[] = $linha;
		}

		$this->tabela_de_classe = $tabela_de_classe;
	}

	public function pericias($classe){
		$arquivo_de_pericia = file_get_contents(PATH_BASE.'config/locale/pt-br/txt/ded3.5/pericias_'.$classe.'.txt');
		$arquivo_de_pericia = explode("\n", $arquivo_de_pericia);
		
		$tabela_de_pericias = [];
		$numero_de_pericias = 0;

		foreach ($arquivo_de_pericia as $key => $value) {
			$numero_de_pericias += 1;
			$nome = explode('-', $value)[0];
			$hab_chave = explode('-', $value)[1];
			$tabela_de_pericias[] = ['nome' => $nome, 'hab_chave' => $hab_chave];
		}

		$pts_de_pericia_no_lv1 = ($this->classes[$classe]['pts_pericia'] + $this->modificador($this->habilidades_basicas['int'])) * 4;
		$pts_de_pericia_nos_niveis_subsequentes = (4 + $this->modificador($this->habilidades_basicas['int'])) * ($this->nivel -1);

		$pontos_de_pericia_totais = $pts_de_pericia_no_lv1 + $pts_de_pericia_nos_niveis_subsequentes;
		$grad_max = $this->nivel + 3;

		shuffle($tabela_de_pericias);

		$qtd_de_pericias = intval($pontos_de_pericia_totais/$grad_max);

		foreach ($tabela_de_pericias as $key => $value) {
			$hab_chave = trim(strtolower($value['hab_chave']));
			if (($qtd_de_pericias) > 0) {
				$tabela_de_pericias_com_valores[] = [
					'nome' => $value['nome'],
					'habilidade_chave' => $value['hab_chave'],
					'graduacao' => $grad_max,
					'mod_hab_chave' => $this->modificador($this->habilidades_basicas[$hab_chave]),
					'total' => $grad_max + $this->modificador($this->habilidades_basicas[$hab_chave])
				];
			}
			$qtd_de_pericias -= 1;
		}

		$this->pericias = $tabela_de_pericias_com_valores;
	}

	public function armas_equipadas(){
		$armas = $this->tabela_de_armas;
		$escolhido = array_rand($armas, mt_rand(1,3));

		if (is_array($escolhido)) {
			foreach ($escolhido as $key => $value) {
				$armas_escolhidas[] = $armas[$value];
			}
		} else {
			$armas_escolhidas[] = $armas[$escolhido];
		}

		$this->armas_equipadas = $armas_escolhidas;
	}

	public function escudo_equipado(){
		$escudo = $this->tabela_de_escudos;
		$tem_escudo = mt_rand(1,2);
		if ($tem_escudo == 1) {
			$escolhido = array_rand($escudo);
			$this->escudos_equipados = $escudo[$escolhido];
		} else {
			$this->escudos_equipados = null;
		}
	}

	public function armadura_equipada(){
		$armadura = $this->tabela_de_armaduras;
		$item_armadura = mt_rand(1,2);
		if ($item_armadura == 1) {
			$escolhido = array_rand($armadura);
			$this->armaduras_equipadas = $armadura[$escolhido];
		} else {
			$this->armaduras_equipadas = null;
		}
	}

	public function arma(){
		$armas = file_get_contents(PATH_BASE.'config/locale/pt-br/txt/ded3.5/armas.txt');
		$armas = explode("\n", $armas);

		foreach ($armas as $key => $value) {
			$linha_de_caracteristicas[] = explode(' ', $value);
		}

		$tabela_de_armas = [];

		foreach ($linha_de_caracteristicas as $key => $value) {
			foreach ($value as $key1 => $value1) {
				if ($key1 == 0) {
					$linha['nome'] = str_replace('_', ' ', $value1);		
				}

				if ($key1 == 1) {
					$linha['custo'] = str_replace('_', ' ', $value1);		
				}

				if ($key1 == 2) {
					$linha['dano_pequeno'] = $value1;		
				}

				if ($key1 == 3) {
					$linha['dano_medio'] = $value1;		
				}

				if ($key1 == 4) {
					$linha['decisivo'] = $value1;		
				}

				if ($key1 == 5) {
					$linha['distancia'] = str_replace('_', ' ', $value1);		
				}

				if ($key1 == 6) {
					$linha['peso'] = $value1;		
				}

				if ($key1 == 7) {
					$linha['tipo'] = str_replace('_', ' ', $value1);		
				}
				
				$linha['bonus_magico'] = 0;		
			}

			$tabela_de_armas[] = $linha;
		}

		$this->tabela_de_armas = $tabela_de_armas;
	}

	public function armadura(){
		$armaduras = file_get_contents(PATH_BASE.'config/locale/pt-br/txt/ded3.5/armaduras.txt');
		$armaduras = explode("\n", $armaduras);

		foreach ($armaduras as $key => $value) {
			$linha_de_caracteristicas[] = explode(' ', $value);
		}

		$tabela_de_armaduras = [];

		foreach ($linha_de_caracteristicas as $key => $value) {
			foreach ($value as $key1 => $value1) {
				if ($key1 == 0) {
					$linha['nome'] = str_replace('_', ' ', $value1);		
				}

				if ($key1 == 1) {
					$linha['custo'] = str_replace('_', ' ', $value1);		
				}

				if ($key1 == 2) {
					$linha['bonus_na_ca'] = $value1;		
				}

				if ($key1 == 3) {
					$linha['penalidade_des'] = $value1;		
				}

				if ($key1 == 4) {
					$linha['falha_armadura'] = $value1;		
				}

				if ($key1 == 5) {
					$linha['falha_magia'] = $value1;		
				}

				if ($key1 == 6) {
					$linha['deslocamento_9m'] = $value1;		
				}

				if ($key1 == 7) {
					$linha['deslocamento_6m'] = $value1;		
				}

				if ($key1 == 8) {
					$linha['peso'] = $value1;		
				}
				
				$linha['bonus_magico'] = 0;		
			}

			$tabela_de_armaduras[] = $linha;
		}

		$this->tabela_de_armaduras = $tabela_de_armaduras;
	}

	public function escudos(){
		$escudos = file_get_contents(PATH_BASE.'config/locale/pt-br/txt/ded3.5/escudos.txt');
		$escudos = explode("\n", $escudos);

		foreach ($escudos as $key => $value) {
			$linha_de_caracteristicas[] = explode(' ', $value);
		}

		$tabela_de_escudos = [];

		foreach ($linha_de_caracteristicas as $key => $value) {
			foreach ($value as $key1 => $value1) {
				if ($key1 == 0) {
					$linha['nome'] = str_replace('_', ' ', $value1);		
				}

				if ($key1 == 1) {
					$linha['custo'] = str_replace('_', ' ', $value1);		
				}

				if ($key1 == 2) {
					$linha['bonus_na_ca'] = $value1;		
				}

				if ($key1 == 3) {
					$linha['penalidade_des'] = $value1;		
				}

				if ($key1 == 4) {
					$linha['falha_armadura'] = $value1;		
				}

				if ($key1 == 5) {
					$linha['falha_magia'] = $value1;		
				}

				if ($key1 == 6) {
					$linha['deslocamento_9m'] = $value1;		
				}

				if ($key1 == 7) {
					$linha['deslocamento_6m'] = $value1;		
				}

				if ($key1 == 8) {
					$linha['peso'] = $value1;		
				}
				
				$linha['bonus_magico'] = 0;		
			}

			$tabela_de_escudos[] = $linha;
		}

		$this->tabela_de_escudos = $tabela_de_escudos;
	}

	public function talentos_de_classe_combatente(){
		$talentos = file_get_contents(PATH_BASE.'config/locale/pt-br/txt/ded3.5/talentos_de_combatentes.txt');
		$talentos = explode("\n", $talentos);
		foreach ($talentos as $key => $value) {
			$linha_de_caracteristicas[] = explode('|', $value);
		}

		$tabela_de_talentos_combatente = [];

		foreach ($linha_de_caracteristicas as $key => $value) {
			foreach ($value as $key1 => $value1) {
				if ($key1 == 0) {
					$linha['nome'] = $value1;		
				}

				if ($key1 == 1) {
					$linha['pre_requisito'] = $value1;		
				}

				if ($key1 == 2) {
					$linha['beneficios'] = $value1;		
				}	
			}

			$tabela_de_talentos_combatente[] = $linha;
		}

		$this->tabela_de_talentos_combatente = $tabela_de_talentos_combatente;
	}

	public function talentos_de_classe_conjurador(){
		$talentos = file_get_contents(PATH_BASE.'config/locale/pt-br/txt/ded3.5/talentos_de_conjurador.txt');
		$talentos = explode("\n", $talentos);
		foreach ($talentos as $key => $value) {
			$linha_de_caracteristicas[] = explode('|', $value);
		}

		$tabela_de_talentos_conjurador = [];

		foreach ($linha_de_caracteristicas as $key => $value) {
			foreach ($value as $key1 => $value1) {
				if ($key1 == 0) {
					$linha['nome'] = $value1;		
				}

				if ($key1 == 1) {
					$linha['pre_requisito'] = $value1;		
				}

				if ($key1 == 2) {
					$linha['beneficios'] = $value1;		
				}	
			}

			$tabela_de_talentos_conjurador[] = $linha;
		}

		$this->tabela_de_talentos_conjurador = $tabela_de_talentos_conjurador;
	}

	public function selecionar_talentos_de_classe($classe){
		$lista_de_talentos = [];

		$combatente = ['barbaro','guerreiro','ladino','monge','paladino','ranger'];			
		$conjurador = ['bardo','clerigo','druida','feiticeiro','mago'];

		if (in_array($classe, $combatente)) {
			$lista_de_talentos = $this->tabela_de_talentos_combatente;
		} elseif (in_array($classe, $conjurador)) {
			$lista_de_talentos = $this->tabela_de_talentos_conjurador;
		}

		$qtd = intval(($this->nivel/3) + 1);
		// $this->tabela_de_classe[$this->nivel-1]['especial'] .= " e + {$qtd} Talentos adicionais.";
		
		$niveis = ['7'=>1,'6'=>3,'5'=>6,'4'=>9,'3'=>12,'2'=>15,'1'=>18];
		
		$talento_por_nivel;
		$talentos_selecionados = [];

		while ($qtd != 0) {
			$nivel = $niveis[$qtd];
			$escolhido = array_rand($lista_de_talentos);
			$um_talento = $lista_de_talentos[$escolhido];
			$pre_requisitos = $um_talento['pre_requisito'];
			$status = $this->detectar_requisito('*'.$um_talento['pre_requisito'], $lista_de_talentos);
			
			$tipo = explode(',',$um_talento['pre_requisito']);
			
			if (count($tipo) == 1) {
				$tipo = 1;
			} else {
				$tipo = 2;
			}

			if ($tipo == 1) {
				$validado = 1;
				if (isset($status['status']) && $status['status'] == 'ok') {
					$validado = 1;
				} elseif (isset($status['habilidade'])) {
					if ($this->habilidades_basicas[strtolower($status['habilidade']['hab'])] >= $status['habilidade']['val']) {
						$validado = 1;
					} else {
						$validado = 2;
					}
				} elseif (isset($status['bba'])) {
					$bba = substr($this->tabela_de_classe[$nivel]['bba'], 0, 2);
					$bba = intval(trim(str_replace('/', '', $bba)));
					if ($bba >= $status['bba']['bba']) {
						$validado = 1;
					} else {
						$validado = 2;
					}
				} elseif (isset($status['nivel'])) {
					$tipo = trim($status['nivel']['tipo']);
					$classe = trim($this->classe);
					if ($tipo == trim('personagem')) {
						if ($nivel >= $status['nivel']['nivel']) {
							$validado = 1;
						} else {
							$validado = 2;
						}
					} elseif ($nivel >= $status['nivel']['nivel'] && $tipo == $classe) {
						$validado = 1;
					} else {
						$validado = 2;
					}
				}elseif (isset($status['pericia'])) {
					foreach ($this->pericias as $key => $value) {
						if ($value['nome'] == $status['pericia']['pericia'] && $value['graduacao'] >= $status['pericia']['grad']) {
							$validado = 1;
						} else {
							$validado = 2;
						}
					}
				}elseif (isset($status['talento'])) {
					$talento_procurado = array_search($status['talento']['nome'], $talentos_selecionados);
					if ($talento_procurado == null) {
						$validado = 2;
					} else {
						$validado = 1;
					}
				}

				if ($validado == 1) {
					$talentos_selecionados[] = $um_talento;
					unset($lista_de_talentos[$escolhido]);
					$qtd -= 1;
				}

			} else {
				$validado = [];
				foreach ($status as $key => $value) {
					foreach ($value as $key1 => $value1) {
						if (isset($value1['status']) and $value1['status'] == 'ok') {
							$validado[] = 1;
						
						}elseif (isset($value1['hab']) and isset($value1['val'])) {
							if ($this->habilidades_basicas[strtolower($value1['hab'])] >= $value1['val']) {
								$validado[] = 1;
							} else {
								$validado[] = 2;	
							}
						
						}elseif (isset($value1['nome'])) {
							$talento_procurado = array_search($value1['nome'], $talentos_selecionados);
							if ($talento_procurado == null) {
								$validado[] = 2;
							} else {
								$validado[] = 1;
							}
						
						}elseif (isset($value1['grad']) and isset($value1['pericia'])) {
							foreach ($this->pericias as $key => $value) {
								if ($value['nome'] == $value1['pericia'] && $value['graduacao'] >= $value1['grad']) {
									$validado[] = 1;
								} else {
									$validado[] = 2;
								}
							}
						
						}elseif (isset($value1['nivel']) and isset($value1['tipo'])) {
							$tipo = trim($value1['tipo']);
							$classe = trim($this->classe);
							if ($tipo == 'personagem') {
								if ($nivel >= $value1['nivel']) {
									$validado[] = 1;
								} else {
									$validado[] = 2;
								}
							} elseif ($nivel >= $value1['nivel'] && $tipo == $classe) {
								$validado[] = 1;
							} else {
								$validado[] = 2;
							}

						}elseif (isset($value1['valor'])) {
							$bba = substr($this->tabela_de_classe[$nivel]['bba'], 0, 2);
							$bba = intval(trim(str_replace('/', '', $bba)));
							if ($bba >= $value1['valor']) {
								$validado[] = 1;
							} else {
								$validado[] = 2;
							}
						}
					}
				}

				$validacao_do_talento = 1;
				foreach ($validado as $key => $value) {
					if ($value == 2) {
						$validacao_do_talento = 2;
					} 	
				}
				
				if ($validacao_do_talento == 1) {
					$talentos_selecionados[] = $um_talento;
					unset($lista_de_talentos[$escolhido]);
					$qtd -= 1;
				}
			}
		}

		$this->talentos_selecionados = $talentos_selecionados;
	}

	public function detectar_requisito($requisito, $lista_de_talentos){
		if (str_replace('*','',$requisito) == '-') {
			$status['status'] = ['sucesso' => 'ok'];
		} else{
			$virgula = strpos($requisito,',');
			if ($virgula == 0) {
				if (str_replace('*','',explode(' ',$requisito)[0]) == 'For') {
					$checar_req = explode(' ', $requisito);
					$status['habilidade'] = ['hab' => str_replace('*','',$checar_req[0]), 'val' => $checar_req[1]];	
				
				} elseif (str_replace('*','',explode(' ',$requisito)[0]) ==  'Des') {
					$checar_req = explode(' ', $requisito);
					$status['habilidade'] = ['hab' => str_replace('*','',$checar_req[0]), 'val' => $checar_req[1]];	
				
				} elseif (str_replace('*','',explode(' ',$requisito)[0]) ==  'Con') {
					$checar_req = explode(' ', $requisito);
					$status['habilidade'] = ['hab' => str_replace('*','',$checar_req[0]), 'val' => $checar_req[1]];	
				
				} elseif (str_replace('*','',explode(' ',$requisito)[0]) ==  'Int') {
					$checar_req = explode(' ', $requisito);
					$status['habilidade'] = ['hab' => str_replace('*','',$checar_req[0]), 'val' => $checar_req[1]];	
				
				} elseif (str_replace('*','',explode(' ',$requisito)[0]) ==  'Sab') {
					$checar_req = explode(' ', $requisito);
					$status['habilidade'] = ['hab' => str_replace('*','',$checar_req[0]), 'val' => $checar_req[1]];
				
				} elseif (str_replace('*','',explode(' ',$requisito)[0]) ==  'Car') {
					$checar_req = explode(' ', $requisito);
					$status['habilidade'] = ['hab' => str_replace('*','',$checar_req[0]), 'val' => $checar_req[1]];
				
				} elseif (strpos($requisito,'bônus base de ataque') > 0) {
					$checar_req = explode('+', $requisito);
					$status['bba'] = ['bba' => str_replace('*','',$checar_req[1])];
				
				} elseif (strpos($requisito,'° nível de') > 0) {
					$checar_req = explode('° nível de', $requisito);
					$status['nivel'] = ['nivel' => str_replace('*','',$checar_req[0]), 'tipo' => $checar_req[1]];
				
				} elseif (strpos($requisito,'graduação em') > 0) {
					$checar_req = explode('graduação em', $requisito);
					$status['pericia'] =  ['grad' => str_replace('*','',$checar_req[0]), 'pericia' => $checar_req[1]];
					
				} else {
					$status['talento'] =  ['nome' => str_replace('*','',$requisito)];
				}
			} else {
				$requisitos = explode(',',$requisito);
				foreach ($requisitos as $key => $value) {
					if ($key > 0) {
						$value = '*'.trim($value);
					}
					
					if (str_replace('*','',explode(' ',$value)[0]) == 'For') {
						$checar_req = explode(' ', $value);
						$status['habilidade'][] = ['hab' => str_replace('*','',$checar_req[0]), 'val' => $checar_req[1]];	
					
					} elseif (str_replace('*','',explode(' ',$value)[0]) == 'Des') {
						$checar_req = explode(' ', $value);
						$status['habilidade'][] = ['hab' => str_replace('*','',$checar_req[0]), 'val' => $checar_req[1]];	
					
					} elseif (str_replace('*','',explode(' ',$value)[0]) == 'Con') {
						$checar_req = explode(' ', $value);
						$status['habilidade'][] = ['hab' => str_replace('*','',$checar_req[0]), 'val' => $checar_req[1]];	
					
					} elseif (str_replace('*','',explode(' ',$value)[0]) == 'Int') {
						$checar_req = explode(' ', $value);
						$status['habilidade'][] = ['hab' => str_replace('*','',$checar_req[0]), 'val' => $checar_req[1]];	
					
					} elseif (str_replace('*','',explode(' ',$value)[0]) == 'Sab') {
						$checar_req = explode(' ', $value);
						$status['habilidade'][] = ['hab' => str_replace('*','',$checar_req[0]), 'val' => $checar_req[1]];
					
					} elseif (str_replace('*','',explode(' ',$value)[0]) == 'Car') {
						$checar_req = explode(' ', $value);
						$status['habilidade'][] = ['hab' => str_replace('*','',$checar_req[0]), 'val' => $checar_req[1]];
					
					} elseif (strpos($value,'bônus base de ataque') > 0) {
						$checar_req = explode('+', $value);
						$status['bba'][] =  ['valor' => str_replace('*','',$checar_req[1])];
					
					} elseif (strpos($value,'° nível de') > 0) {
						$checar_req = explode('° nível de', $value);
						$status['nivel'][] =  ['nivel' => str_replace('*','',$checar_req[0]), 'tipo' => $checar_req[1]];

					} elseif (strpos($value,'graduação em') > 0) {
						$checar_req = explode('graduação em', $value);
						$status['pericia'][] =  ['grad' => str_replace('*','',$checar_req[0]), 'pericia' => $checar_req[1]];

					} else {
						$status['talento'][] =  ['nome' => str_replace('*','',$value)];
					}
				}
			}
		}

		return $status;
	}

	public function magias($classe){
		$path = PATH_BASE.'config/locale/pt-br/txt/ded3.5/';
		$classes = ['bardo','clerigo','druida','feiticeiro','mago','paladino','ranger'];

		$habilidade_chave = [
			'bardo' => 'car',
			'clerigo' => 'sab',
			'druida' => 'sab',
			'feiticeiro' => 'car',
			'mago' => 'int',
			'paladino' => 'sab',
			'ranger' => 'sab'
		];

		if (in_array($classe, $classes)) {
			if ($classe == 'bardo' || $classe == 'feiticeiro') {
				$magias_conhecidas = file_get_contents($path.'magias_conhecidas_'.$classe.'.txt');
				$magias = explode("\n", $magias_conhecidas);
				$magias_conhecidas_a_exibir = '';
				$valores_por_nivel = explode(' ', $magias[$this->nivel-1]);
				foreach ($valores_por_nivel as $key => $value) {
					if (trim($value) != '-') {
						$magias_conhecidas_a_exibir .= "{$value}/";
					}
				}

				$this->magias_conhecidas = "{$magias_conhecidas_a_exibir}";
			} else {
				$this->magias_conhecidas = '';
			}

			$magias_por_dia = file_get_contents($path.'magias_por_dia_'.$classe.'.txt');
			$magias = explode("\n", $magias_por_dia);
			
			$magias_por_dia = explode(' ', $magias[$this->nivel-1]);
			$magias_selecionadas = [];
			
			foreach ($magias_por_dia as $key => $value) {
				$magias_escolhidas = null;
				if (trim($value) != '-') {
					$value = intval($value);
					$value += $this->modificador($this->habilidades_basicas[$habilidade_chave[$classe]]);
					
					if ($value > 0) {
						if ($classe == 'paladino' || $classe == 'ranger') {
							$key +=1;
						}
						$lista_de_magias = explode("\n", file_get_contents($path.'ded3.5-magias/'.$classe.'/'.$key.'.txt'));
						$selecionados = array_rand($lista_de_magias, $value);
						
						if (is_array($selecionados)) {
							foreach ($selecionados as $key1 => $value1) {
								$magias_escolhidas[] = $lista_de_magias[$value1];
							}
						} else {
							$magias_escolhidas[] = $lista_de_magias[$selecionados];
						}
					}

					$magias_selecionadas[] = $magias_escolhidas;
				}
			}

			$this->magias_selecionadas = $magias_selecionadas;
		
		} else {
			$this->magias_selecionadas = [];
		}
	}
}