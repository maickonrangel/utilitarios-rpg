<?php

class Pdms_Model {

	public $file_path;

	function __construct(){
		$this->file_path = PATH_BASE.'config/locale/pt-br/txt/livro_do_mestre/pdm/';
		$this->tracos_pdm = [
			'aparencia' => 'Aparência',
			'defeito_ou_segredos' => 'Defeito ou Segredos',
			'dom' => 'Dom',
			'habilidade_alta' => 'Habilidade alta',
			'habilidade_baixa' => 'Habilidade baixa',
			'um_ideal' => 'Um ideal',
			'maneirismo' => 'Maneirismo',
			'tracos_de_interacao' => 'Traços de interação',
			'vinculos' => 'Vínculos'
		];
	}

	public function pdm(){
		$hab_alta = $this->hab_alta();
		$hab_baixa = $this->hab_baixa(1, $hab_alta);

		$descricao = [
			'aparencia' => $this->aparencia(),
			'defeito_ou_segredos' => $this->defeito_ou_segredos(),
			'dom' => $this->dom(),
			'habilidade_alta' => $hab_alta,
			'habilidade_baixa' => $hab_baixa,
			'um_ideal' => $this->ideais(),
			'maneirismo' => $this->maneirismo(),
			'tracos_de_interacao' => $this->tracos_de_interacao(),
			'vinculos' => $this->vinculos()
		];

		return $descricao;
	}

	function aparencia($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'aparencia.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function defeito_ou_segredos($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'defeito_ou_segredos.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function dom($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'dom.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function hab_alta($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'hab_alta.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function hab_baixa($qtd=1, $hab_alta){
		$status = true;
		$hab = '';
		$hab_alta_file = $hab_alta;
		while ($status == true) {
			$hab_baixa_file = (new Raffleitemfile_Core($this->file_path.'hab_baixa.txt', $qtd, 'FILE'))->getRaffleItens();
			$hab_baixa = explode('-', $hab_baixa_file);
			$hab_alta = explode('-',$hab_alta_file);
			if ($hab_baixa[0] != $hab_alta[0]) {
				$status = false;
				$hab = $hab_baixa_file;
			}
		}
		return $hab;
	}

	function ideais($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'ideais.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function maneirismo($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'maneirismo.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function tracos_de_interacao($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'tracos_de_interacao.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function vinculos($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'vinculos.txt', $qtd, 'FILE'))->getRaffleItens();
	}
}