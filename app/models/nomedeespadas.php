<?php

class Nomedeespadas_Model {
	
	public $first_name;
	public $second_name;
	public $file_path;

	function __construct(){
		$this->file_path = PATH_BASE.'config/locale/pt-br/txt/nome_espadas/';
	}

	public function get_sword_name(){
		$this->first_name 	= new Raffleitemfile_Core($this->file_path.'primeiro-nome.txt', 10);
		$this->second_name 	= new Raffleitemfile_Core($this->file_path.'segundo-nome.txt', 10);
	}
}