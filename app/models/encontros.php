<?php

class Encontros_Model {

	public $file_path;

	function __construct(){
		$this->file_path = PATH_BASE.'config/locale/pt-br/txt/encontro_aleatorio/viagem/';
		$this->ambientes = [
			'cidade' => 'Cidade',
			'deserto' => 'Deserto',
			'estrada' => 'Estrada',
			'floresta' => 'Floresta',
			'maritima' => 'Viagem Maritima',
			'montanha' => 'Montanhas'
		];
	}

	function sortear_encontro($ambiente, $qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'viagem-'.$ambiente.'.txt', $qtd, 'FILE'))->getRaffleItens();
	}
}