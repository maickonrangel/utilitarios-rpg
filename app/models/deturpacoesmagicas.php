<?php

class Deturpacoesmagicas_Model {

	public $file_path;

	function __construct(){
		$this->file_path = PATH_BASE.'config/locale/pt-br/txt/deturpacoes_magicas/';
	}

	function sortear($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'deturpacoes_magicas.txt', $qtd, 'FILE'))->getRaffleItens();
	}
}