<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel - maickon4developers@gmail.com
	Prodigio Framework - 2018
	Model: usuarios
*/

class Usuarios_Model extends Dbrecord_Core {

	private $permit;

	public function __construct(){
		parent::__construct();
		$this->permit = [
			'id',
			'email',
			'senha',
			'plano',
			'status',
			'nivel',
			'data_ativacao',
			'nick',
			'created_at'
		];
	}

	public function get_permit(){
		return $this->permit;
	}
}