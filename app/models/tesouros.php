<?php

class Tesouros_Model {

	public $file_path;

	function __construct(){
		$this->file_path = PATH_BASE.'config/locale/pt-br/txt/livro_do_mestre/tesouro/';
	}

	public function rolar($qtd, $dado){
		$total = 0;
		for ($i=0; $i < $qtd; $i++) { 
			$total += mt_rand(1, intval($dado));
		}
		return $total;
	}

	public function tesouro_individual($nd){
		$nd04 = [
			'pc'=>['valor'=>$this->rolar(5,6), 'moeda'=>'PC', 'nivel'=>'0-4'], 
			'pp'=>['valor'=>$this->rolar(4,6), 'moeda'=>'PP', 'nivel'=>'0-4'], 
			'pe'=>['valor'=>$this->rolar(3,6), 'moeda'=>'PE', 'nivel'=>'0-4'], 
			'po'=>['valor'=>$this->rolar(3,6), 'moeda'=>'PO', 'nivel'=>'0-4'], 
			'pl'=>['valor'=>$this->rolar(1,6), 'moeda'=>'PL', 'nivel'=>'0-4']
		];

		$nd510 = [
			'pc'=>['valor'=>$this->rolar(4,6)*100, 'moeda'=>'PC', 'nivel'=>'5-10'], 
			'pp'=>['valor'=>$this->rolar(6,6)*10, 'moeda'=>'PP', 'nivel'=>'5-10'], 
			'pe'=>['valor'=>$this->rolar(1,6)*10, 'moeda'=>'PE', 'nivel'=>'5-10'], 
			'pe'=>['valor'=>$this->rolar(3,6)*10, 'moeda'=>'PE', 'nivel'=>'5-10'], 
			'po'=>['valor'=>$this->rolar(2,6)*10, 'moeda'=>'PE', 'nivel'=>'5-10'],
			'po'=>['valor'=>$this->rolar(2,6)*10, 'moeda'=>'PO', 'nivel'=>'5-10'],
			'po'=>['valor'=>$this->rolar(4,6)*10, 'moeda'=>'PO', 'nivel'=>'5-10'],
			'po'=>['valor'=>$this->rolar(2,6)*10, 'moeda'=>'PO', 'nivel'=>'5-10'],
			'pl'=>['valor'=>$this->rolar(3,6), 'moeda'=>'PL', 'nivel'=>'5-10']
		];

		$nd1116 = [ 
			'pp'=>['valor'=>$this->rolar(4,6)*100, 'moeda'=>'PP', 'nivel'=>'11-16'], 
			'pe'=>['valor'=>$this->rolar(1,6)*100, 'moeda'=>'PE', 'nivel'=>'11-16'],  
			'po'=>['valor'=>$this->rolar(1,6)*100, 'moeda'=>'PO', 'nivel'=>'11-16'],
			'po'=>['valor'=>$this->rolar(1,6)*100, 'moeda'=>'PO', 'nivel'=>'11-16'],
			'po'=>['valor'=>$this->rolar(2,6)*100, 'moeda'=>'PO', 'nivel'=>'11-16'],
			'po'=>['valor'=>$this->rolar(2,6)*100, 'moeda'=>'PO', 'nivel'=>'11-16'],
			'pl'=>['valor'=>$this->rolar(1,6)*10, 'moeda'=>'PL', 'nivel'=>'11-16'],
			'pl'=>['valor'=>$this->rolar(2,6)*10, 'moeda'=>'PL', 'nivel'=>'11-16']
		];

		$nd17Maior = [ 
			'pe'=>['valor'=>$this->rolar(2,6)*1000, 'moeda'=>'PE', 'nivel'=>'17+'],  
			'po'=>['valor'=>$this->rolar(8,6)*100, 'moeda'=>'PO', 'nivel'=>'17+'],
			'po'=>['valor'=>$this->rolar(1,6)*1000, 'moeda'=>'PO', 'nivel'=>'17+'],
			'po'=>['valor'=>$this->rolar(1,6)*1000, 'moeda'=>'PO', 'nivel'=>'17+'],
			'pl'=>['valor'=>$this->rolar(1,6)*100, 'moeda'=>'PL', 'nivel'=>'17+'],
			'pl'=>['valor'=>$this->rolar(2,6)*100, 'moeda'=>'PL', 'nivel'=>'17+']
		];

		$tesouro = null;

		if ($nd == '0_4') {
			$indice = array_rand($nd04);
			$tesouro = $nd04[$indice];
		} elseif ($nd == '5_10') {
			$indice = array_rand($nd510);
			$tesouro = $nd510[$indice];
		} elseif ($nd == '11_16') {
			$indice = array_rand($nd1116);
			$tesouro = $nd1116[$indice];
		} elseif ($nd == '17_maior') {
			$indice = array_rand($nd17Maior);
			$tesouro = $nd17Maior[$indice];
		}

		return $tesouro;
	}

	public function pilha_de_tesouros($nd){
		if ($nd == '0_4') {
			$tesouro = $this->pilha($nd);
		} elseif ($nd == '5_10') {
			$tesouro = $this->pilha($nd);
		} elseif ($nd == '11_16') {
			$tesouro = $this->pilha($nd);
		} elseif ($nd == '17_maior') {
			$tesouro = $this->pilha($nd);
		}
		return $tesouro;
	}

	public function pilha($tipo){
		$arquivo = explode("\n",file_get_contents($this->file_path.'pilha_de_tesouros_nd_'.$tipo.'.txt'));
		$pilha = $arquivo[array_rand($arquivo)];
		$arquivo_itens = [];
		$pilha_de_tesouros;

		$linha = explode('-', $pilha);
		$dado = explode('d',$linha[0]);
		$qtd = $this->rolar($dado[0],$dado[1]);
		$item = $linha[1];
		if (!empty($linha[2])) {	
			$itens_dados = explode('_',$linha[2]);
			$qtd_itens = substr($itens_dados[0], -1);
			$arquivo_itens = (new Raffleitemfile_Core(PATH_BASE.'config/locale/pt-br/txt/itens/itens_magicos_ded5e_tabela-'.trim(strtolower($itens_dados[1])).'.txt', $qtd_itens))->getRaffleItens();	
		}
		
		$itens = [];
		if (is_array($arquivo_itens)) {
			foreach ($arquivo_itens as $key => $pilha) {
				$itens[] = $pilha;
			}
		} else {
			$itens[] = $arquivo_itens;
		}
		$pilha_de_tesouros = ['qtd'=> $qtd, 'itens'=> $item, 'itens_magicos'=> $itens];	

		return $pilha_de_tesouros;
	}

	function objetos_de_arte($qtd=5){
		return (new Raffleitemfile_Core($this->file_path.'objetos_de_arte.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function gemas($qtd=5){
		return (new Raffleitemfile_Core($this->file_path.'gemas.txt', $qtd, 'FILE'))->getRaffleItens();
	}
}