<?php
	// Help RPG 2016
	// @author Maickon Rangel
	// Classe de modelo gerada no automatico

	class Formulariofichas_Model {

		public $path_upload_fichas;

		public function __construct(){
			$this->path_upload_fichas = PATH_BASE.'config/locale/pt-br/xml/fichas/';
			$this->fichas = [
			'ded35' => 'D&D 3.5',
			'ded5e' => 'D&D 5e',
			'3det_defensores' => '3D&T - Defensores de Tóquio',
			'daemon_supers' => 'Daemon Supers',
			'mutantes_e_malfeitores' => 'Mutantes & Malfeitores',
			'sevage_worlds' => 'Sevage Worlds',
			'gurps4e' => 'GURPS 4e',
			'guerra_dos_tronos' => 'Guerra dos Tronos RPG',
			'tormentad20_rpg' => 'Tormenta RPG',
			'kids_e_dragon' => 'Kids & Dragons'
			];
		}

		public function unique_code(){
			return uniqid();
		}

		public function ficha_to_xml($request, $acao = 'cadastro'){
			unset($request['url']);
			unset($request['salvar']);
			$xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<ficha>\n";
			foreach ($request as $key => $value) {
				$xml .= "\t<{$key}>{$value}</{$key}>\n";
			}
			$xml .= "</ficha>";
			$erro = 1;
			if (!file_exists($this->path_upload_fichas.$request['sistema'].'/'.$request['chave'].'.xml')) {
				if(file_put_contents($this->path_upload_fichas.$request['sistema'].'/'.$request['chave'].'.xml', $xml)){
					$erro = 0;
					@session_start();
					$_SESSION['personagens'][] = ['chave'=>$request['chave'], 'nome'=>$request['nome'], 'sistema'=>$request['sistema']];
					$cookie_nome = 'Personagem_'.$request['chave'];
					$cookie_value = $request['chave'].'-'.$request['sistema'].'-'.$request['nome'];
					@setcookie($cookie_nome, $cookie_value, time()+2592000, '/', "");
				}
			} else {
				if ($acao == 'atualizar') {
					if(file_put_contents($this->path_upload_fichas.$request['sistema'].'/'.$request['chave'].'.xml', $xml)){
						$erro = 0;
					}
				}
			}
			
			return $erro;
		}

		public function get_ficha($chave, $sistema){
			if (file_exists($this->path_upload_fichas.$sistema.'/'.$chave.'.xml')) {
				return simplexml_load_file($this->path_upload_fichas.$sistema.'/'.$chave.'.xml');
			} else {
				return null;
			}
		}

		// public function deletar_ficha($chave, $sistema){
		// 	if (file_exists($this->path_upload_fichas.$sistema.'/'.$chave.'.xml')) {
		// 		// unlink($this->path_upload_fichas.$sistema.'/'.$chave.'.xml');
		// 		if (isset($_SESSION)) {
		// 			foreach ($_SESSION as $key => $value) {
		// 				if ($_SESSION['personagens']['chave'] == $chave) {
		// 					echo 'deletar - '.$_SESSION['personagens']['chave'];
		// 				}
		// 			}
		// 		}
		// 	} else {
		// 		return null;
		// 	}
		// }
	}