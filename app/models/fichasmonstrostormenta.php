<?php


class Fichasmonstrostormenta_Model {

	public $nivel;
	public $tendencia;
	public $deslocamento;
	public $tamanho;

	public $fortitude;
	public $reflexos;
	public $vontade;
	public $pericias;
	public $ca;
	public $tipo_pele;
	public $tipo;
	public $tipo_bq;
	public $armas_naturais;

	public function __construct($nivel = null, $tipo = null, $tamanho = null, $tipo_bq = null){
		$this->monstros_nivel($nivel);
		$this->monstros_config($tipo);
		$this->monstros_tamanho($tamanho);
		$this->monstros_deslocamento_terrestre($tipo_bq);
		$this->monstros_deslocamento_voador();
		$this->monstros_deslocamento_escalador();
		$this->monstros_deslocamento_escavador();
		$this->monstros_deslocamento_nadador();
		$this->monstros_pv($this->tipo['nome']);
		$this->monstros_ca();
		$this->tendencia($this->tipo['nome']);
		$this->monstros_testes_resistencia($this->tipo['nome']);
		$this->monstros_armas_naturais($this->tamanho['tipo']);
		$this->monstros_ataque();
		$this->monstros_tesouro();
		$this->monstros_caracteristicas($this->tipo['nome']);
		$this->monstros_talentos();
		$this->habilidades_especiais();
	}

	public function modificador($valor){
		if ($valor == '-'){
			$mod = '';
		} elseif ($valor == 9 || $valor == 8) {
			return -1;
		} elseif ($valor == 7 || $valor == 6) {
			return -2;
		} elseif ($valor == 5 || $valor == 4) {
			return -3;
		} elseif ($valor == 3 || $valor == 2) {
			return -4;
		} elseif ($valor == 1 || $valor == 0) {
			return -5;
		} else {
			$mod = intval(($valor - 10)/2);
			if($mod > 0){
				return "+{$mod}";
			} else {
				return " {$mod}";
			}
		}
	}

	public function monstros_nivel($nivel = null){
		if ($nivel == null) {
			$this->nivel = mt_rand(1,20);
		} else {
			$this->nivel = $nivel;
		}
	}

	public function tendencia($tipo){
		$tendencias = ['Leal e Bom','Neutro e Bom','Caótico e Bom','Leal e Neutro','Neutro','Caótico e Neutro','Leal e Mal', 'Neutro e Mal', 'Caótico e Mal'];
		$indice = array_rand($tendencias);
		if ($tipo == 'animais') {
			$this->tendencia = 'Neutro';
		} else {
			$this->tendencia = $tendencias[$indice];
		}
	}

	public function monstro_tipos(){
		return ['animais'=>'Animal','constructos'=>'Constructo','humanoides'=>'Humanoide','espiritos'=>'Espírito','monstros'=>'Monstro','mortos-vivos'=>'Morto-vivo'];
	}

	public function monstro_tamanhos(){
		return ['noinfimome'=>'Ínfimo','dimiuto'=>'Dimiuto','minimo'=>'Mínimo','pequeno'=>'Pequeno','medio'=>'Médio','grande_alto'=>'Grande Alto','grande_comprido'=>'Grande comprido','enorme_alto'=>'Enorme alto','enorme_comprido'=>'Enorme comprido','descomunal_alto'=>'Descomunal alto','descomunal_comprido'=>'Descomunal comprido','colossal_alto'=>'Colossal alto','colossal_comprido'=>'Colossal comprido'];
	}

	public function monstros_config($tipo = null){
		$monstros = [

			'animais' => ['nome'=>'animais','tipo'=>'Animal','pv'=>4, 'bba'=>intval(($this->nivel/4))*3, 'pericias'=>2, 'talentos'=>'Fortitude Maior, Reflexos Rápidos', 'int'=>mt_rand(1,2), 'sab'=>mt_rand(1,18), 'car'=>mt_rand(1,18)],

			'constructos' => ['nome'=>'constructos','tipo'=>'Constructo','pv'=>5, 'bba'=>intval(($this->nivel/4))*3, 'pericias'=>2, 'talentos'=>'', 'int'=>mt_rand(1,20), 'sab'=>mt_rand(1,20), 'car'=>mt_rand(1,20)],

			'humanoides' => ['nome'=>'humanoides','tipo'=>'Humanoide','pv'=>3, 'bba'=>intval(($this->nivel/4))*3, 'pericias'=>2, 'talentos'=>'', 'int'=>mt_rand(1,20), 'sab'=>mt_rand(1,20), 'car'=>mt_rand(1,20)],

			'espiritos' => ['nome'=>'espiritos','tipo'=>'Espírito','pv'=>4, 'bba'=>$this->nivel, 'pericias'=>4, 'talentos'=>'Fortitude Maior, Reflexos Rápidos, Vontade de Ferro', 'int'=>mt_rand(1,20), 'sab'=>mt_rand(1,20), 'car'=>mt_rand(1,20)],

			'monstros' => ['nome'=>'monstros','tipo'=>'Monstro','pv'=>6, 'bba'=>$this->nivel, 'pericias'=>2, 'talentos'=>'Fortitude Maior, Reflexos Rápidos', 'int'=>mt_rand(1,20), 'sab'=>mt_rand(1,20), 'car'=>mt_rand(1,20)],

			'mortos-vivos' => ['nome'=>'mortos-vivos','tipo'=>'Morto-Vivo','pv'=>6, 'bba'=>intval($this->nivel/2), 'pericias'=>4, 'talentos'=>'Vontade de Ferro', 'int'=>mt_rand(1,20), 'sab'=>mt_rand(1,20), 'car'=>mt_rand(1,20)],

		];

		if ($tipo == null) {
			$this->tipo = $monstros[array_rand($monstros)];
		} else {
			$this->tipo = $monstros[$tipo];
		}
	}

	public function monstros_tesouro(){
		$tesouros = ['nenhum','metade','padrão','dobro','triplo'];
		$this->tesouro = $tesouros[array_rand($tesouros)];
	}

	public function monstros_tamanho($tamanho = null){
		$tamanhos = [
			'infimo' => ['tipo'=>'infimo','nome'=>'Ínfimo','exemplo'=>'Mosca','espaco'=>'15cm','alcance_nat'=>'1,5m','modificador'=>8,'mod_furtividade'=>16, 'for'=>1, 'des'=>mt_rand(16,27), 'con'=>mt_rand(2,11)],

			'dimiuto' => ['tipo'=>'dimiuto','nome'=>'Diminuto','exemplo'=>'Sapo','espaco'=>'30cm','alcance_nat'=>'1,5m','modificador'=>4,'mod_furtividade'=>12, 'for'=>1, 'des'=>mt_rand(14,25), 'con'=>mt_rand(4,13)],

			'minimo' => ['tipo'=>'minimo','nome'=>'Mínimo','exemplo'=>'Gato, Sprite','espaco'=>'75cm','alcance_nat'=>'1,5m','modificador'=>2,'mod_furtividade'=>8, 'for'=>mt_rand(2,5), 'des'=>mt_rand(12,23), 'con'=>mt_rand(4,15)],

			'pequeno' => ['tipo'=>'pequeno','nome'=>'Pequeno','exemplo'=>'Halfling, Goblin','espaco'=>'1,5m','alcance_nat'=>'1,5m','modificador'=>1,'mod_furtividade'=>4, 'for'=>mt_rand(4,11), 'des'=>mt_rand(10,21), 'con'=>mt_rand(6,17)],

			'medio' => ['tipo'=>'medio','nome'=>'Médio','exemplo'=>'Humano','espaco'=>'1,5m','alcance_nat'=>'1,5m','modificador'=>0,'mod_furtividade'=>0, 'for'=>mt_rand(8,19), 'des'=>mt_rand(8,19), 'con'=>mt_rand(8,19)],

			'grande_alto' => ['tipo'=>'grande_alto','nome'=>'Grande (Alto)','exemplo'=>'Ogro, Troll','espaco'=>'3m','alcance_nat'=>'3m','modificador'=>-1,'mod_furtividade'=>-4, 'for'=>mt_rand(18,27), 'des'=>mt_rand(6,17), 'con'=>mt_rand(10,23)],

			'grande_comprido' => ['tipo'=>'grande_comprido','nome'=>'Grande (Comprido)','exemplo'=>'Centauro, Trobo','espaco'=>'3m','alcance_nat'=>'1,5m','modificador'=>-1,'mod_furtividade'=>-4, 'for'=>mt_rand(18,27), 'des'=>mt_rand(6,17), 'con'=>mt_rand(10,23)],

			'enorme_alto' => ['tipo'=>'enorme_alto','nome'=>'Enorme (alto)','exemplo'=>'Ente','espaco'=>'4,5m','alcance_nat'=>'4,5m','modificador'=>-2,'mod_furtividade'=>-8, 'for'=>mt_rand(24,33), 'des'=>mt_rand(4,15), 'con'=>mt_rand(12,27)],
			
			'enorme_comprido' => ['tipo'=>'enorme_comprido','nome'=>'Enorme (Comprido)','exemplo'=>'Dragão adulto','espaco'=>'4,5m','alcance_nat'=>'3m','modificador'=>-2,'mod_furtividade'=>-8, 'for'=>mt_rand(24,33), 'des'=>mt_rand(4,15), 'con'=>mt_rand(12,27)],

			'descomunal_alto' => ['tipo'=>'descomunal_alto','nome'=>'Descomunal (alto)','exemplo'=>'Divina serpente','espaco'=>'6m','alcance_nat'=>'6m','modificador'=>-4,'mod_furtividade'=>-12, 'for'=>mt_rand(30,41), 'des'=>mt_rand(4,13), 'con'=>mt_rand(14,31)],

			'descomunal_comprido' => ['tipo'=>'descomunal_comprido','nome'=>'Descomunal (comprido)','exemplo'=>'Dragão venerável','espaco'=>'6m','alcance_nat'=>'4,5m','modificador'=>-4,'mod_furtividade'=>-12, 'for'=>mt_rand(30,41), 'des'=>mt_rand(4,13), 'con'=>mt_rand(14,31)],

			'colossal_alto' => ['tipo'=>'colossal_alto','nome'=>'Colossal (alto)','exemplo'=>'Kishin','espaco'=>'9m','alcance_nat'=>'9m','modificador'=>-8,'mod_furtividade'=>-16, 'for'=>mt_rand(36,49), 'des'=>mt_rand(2,11), 'con'=>mt_rand(18,39)],

			'colossal_comprido' => ['tipo'=>'colossal_comprido','nome'=>'Colossal (comprido)','exemplo'=>'Senhor das Profundezas','espaco'=>'9m','alcance_nat'=>'6m','modificador'=>-8,'mod_furtividade'=>-16, 'for'=>mt_rand(36,49), 'des'=>mt_rand(2,11), 'con'=>mt_rand(18,39)],
		];

		if ($tamanho == null) {
			$sorteado = $tamanhos[array_rand($tamanhos)];
			$this->tamanho = $sorteado;
		} else {
			$this->tamanho = $tamanhos[$tamanho];
		}
	}

	public function monstros_pv($tipo){
		if ($tipo == 'mortos-vivos' || $tipo == 'constructos') {
			$this->tamanho['con'] = '-';
			$this->pv = $this->nivel*($this->tipo['pv']+$this->nivel);
		} else {
			$this->pv = $this->nivel*($this->tipo['pv'])+$this->nivel*intval($this->modificador($this->tamanho['con']));
		}
		
	}

	public function monstros_ataque(){
		$atk = $this->tipo['bba']+$this->modificador($this->tamanho['for']);
		if ($atk > 0) {
			$this->ataque = "+{$atk}";
		} elseif ($atk < 0) {
			$this->ataque = "-{$atk}";
		} else {
			$this->ataque = $atk;
		}
	}

	public function monstros_talentos(){
		$this->qtd_talentos = intval($this->nivel/2);
	}

	public function monstros_ca($tipo = null) {
		$peles = [
				'pele_normal'=>['nome'=>'Pele normal', 'bonus'=>0],
				'pele_grossa'=>['nome'=>'Pele grossa', 'bonus'=>mt_rand(1,3)],
				'pelo'=>['nome'=>'Pêlo', 'bonus'=>mt_rand(1,3)],
				'couro'=>['nome'=>'Couro', 'bonus'=>mt_rand(4,6)],
				'escamas'=>['nome'=>'Escamas', 'bonus'=>mt_rand(4,8)],
				'exoesqueleto'=>['nome'=>'Exoesqueleto', 'bonus'=>mt_rand(2,10)],
				'casco'=>['nome'=>'Casco', 'bonus'=>mt_rand(8,10)],
				'carapaca'=>['nome'=>'Carapaça', 'bonus'=>mt_rand(8,10)],
				'couraca'=>['nome'=>'Couraça', 'bonus'=>mt_rand(6,12)]
			];

		if ($tipo == null) {
			$sorteado = $peles[array_rand($peles)];
			$this->ca += 10+$sorteado['bonus']+$this->tamanho['modificador'];
			$this->tipo_pele = $sorteado['nome'];
		} else {
			$this->ca += 10+$peles[$tipo]['bonus']+$this->tamanho['modificador'];
			$this->tipo_pele = $peles[$tipo]['nome'];
		}
	}

	public function inserir_sinal($valor){
		if ($valor > 0) {
			return "+{$valor}";
		}
	}

	public function monstros_testes_resistencia($tipo){
		if ($tipo == 'mortos-vivos' || $tipo == 'constructos') {
			$this->fortitude = '-';
		} else {
			$this->fortitude = $this->inserir_sinal(intval($this->nivel/2)+$this->modificador($this->tamanho['con']));
		}
		$this->vontade = $this->inserir_sinal(intval($this->nivel/2)+$this->modificador($this->tipo['sab']));
		$this->reflexos = $this->inserir_sinal(intval($this->nivel/2)+$this->modificador($this->tamanho['des']));
	}

	public function monstros_armas_naturais($tamanho = null) {
		$tamanhos = [
			'infimo' => [
						'mordida'=>['nome'=>'Mordida','dano'=>'1'],
						'garra_ferrao'=>['nome'=>'Garra ou Ferrão','dano'=>'0'],
						'chifres_cauda'=>['nome'=>'Chifres ou cauda','dano'=>'0'],
						'pancada'=>['nome'=>'Pancada','dano'=>'0']
					],
			'dimiuto' => [
						'mordida'=>['nome'=>'Mordida','dano'=>'1d2'],
						'garra_ferrao'=>['nome'=>'Garra ou Ferrão','dano'=>'1'],
						'chifres_cauda'=>['nome'=>'Chifres ou cauda','dano'=>'-1'],
						'pancada'=>['nome'=>'Pancada','dano'=>'1']
					],
			'minimo' => [
						'mordida'=>['nome'=>'Mordida','dano'=>'1d3'],
						'garra_ferrao'=>['nome'=>'Garra ou Ferrão','dano'=>'1d2'],
						'chifres_cauda'=>['nome'=>'Chifres ou cauda','dano'=>'1d3'],
						'pancada'=>['nome'=>'Pancada','dano'=>'1d2']
					],
			'pequeno' => [
						'mordida'=>['nome'=>'Mordida','dano'=>'1d4'],
						'garra_ferrao'=>['nome'=>'Garra ou Ferrão','dano'=>'1d3'],
						'chifres_cauda'=>['nome'=>'Chifres ou cauda','dano'=>'1d4'],
						'pancada'=>['nome'=>'Pancada','dano'=>'1d3']
					],
			'medio' => [
						'mordida'=>['nome'=>'Mordida','dano'=>'1d6'],
						'garra_ferrao'=>['nome'=>'Garra ou Ferrão','dano'=>'1d4'],
						'chifres_cauda'=>['nome'=>'Chifres ou cauda','dano'=>'1d6'],
						'pancada'=>['nome'=>'Pancada','dano'=>'1d4']
					],
			'grande_alto' => [
						'mordida'=>['nome'=>'Mordida','dano'=>'1d8'],
						'garra_ferrao'=>['nome'=>'Garra ou Ferrão','dano'=>'1d6'],
						'chifres_cauda'=>['nome'=>'Chifres ou cauda','dano'=>'1d8'],
						'pancada'=>['nome'=>'Pancada','dano'=>'1d6']
					],
			'grande_comprido' => [
						'mordida'=>['nome'=>'Mordida','dano'=>'1d8'],
						'garra_ferrao'=>['nome'=>'Garra ou Ferrão','dano'=>'1d6'],
						'chifres_cauda'=>['nome'=>'Chifres ou cauda','dano'=>'1d8'],
						'pancada'=>['nome'=>'Pancada','dano'=>'1d6']
					],
			'enorme_alto' => [
						'mordida'=>['nome'=>'Mordida','dano'=>'2d6'],
						'garra_ferrao'=>['nome'=>'Garra ou Ferrão','dano'=>'1d8'],
						'chifres_cauda'=>['nome'=>'Chifres ou cauda','dano'=>'1d12'],
						'pancada'=>['nome'=>'Pancada','dano'=>'1d8']
					],
			'enorme_comprido' => [
						'mordida'=>['nome'=>'Mordida','dano'=>'2d6'],
						'garra_ferrao'=>['nome'=>'Garra ou Ferrão','dano'=>'1d8'],
						'chifres_cauda'=>['nome'=>'Chifres ou cauda','dano'=>'1d12'],
						'pancada'=>['nome'=>'Pancada','dano'=>'1d8']
					],
			'descomunal_alto' => [
						'mordida'=>['nome'=>'Mordida','dano'=>'3d6'],
						'garra_ferrao'=>['nome'=>'Garra ou Ferrão','dano'=>'2d6'],
						'chifres_cauda'=>['nome'=>'Chifres ou cauda','dano'=>'2d8'],
						'pancada'=>['nome'=>'Pancada','dano'=>'2d6']
					],
			'descomunal_comprido' => [
						'mordida'=>['nome'=>'Mordida','dano'=>'3d6'],
						'garra_ferrao'=>['nome'=>'Garra ou Ferrão','dano'=>'2d6'],
						'chifres_cauda'=>['nome'=>'Chifres ou cauda','dano'=>'2d8'],
						'pancada'=>['nome'=>'Pancada','dano'=>'2d6']
					],
			'colossal_alto' => [
						'mordida'=>['nome'=>'Mordida','dano'=>'4d6'],
						'garra_ferrao'=>['nome'=>'Garra ou Ferrão','dano'=>'2d8'],
						'chifres_cauda'=>['nome'=>'Chifres ou cauda','dano'=>'3d8'],
						'pancada'=>['nome'=>'Pancada','dano'=>'2d8']
					],
			'colossal_comprido' => [
						'mordida'=>['nome'=>'Mordida','dano'=>'4d6'],
						'garra_ferrao'=>['nome'=>'Garra ou Ferrão','dano'=>'2d8'],
						'chifres_cauda'=>['nome'=>'Chifres ou cauda','dano'=>'3d8'],
						'pancada'=>['nome'=>'Pancada','dano'=>'2d8']
					]
		];

		if ($tamanho == null) {
			$sorteado = array_rand($tamanhos);
			$this->armas_naturais = $tamanhos[$sorteado];
		} else {
			$this->armas_naturais = $tamanhos[$tamanho];
		}
	}

	public function monstros_deslocamento_terrestre($tipo_bq = null){
		$tipo = ['bipide'=>'Bípede','quadrupede'=>'Quadrúpede'];
		if ($tipo_bq == null) {
			$this->tipo_bq = $tipo[array_rand($tipo)];
		} else {
			$this->tipo_bq = $tipo[$tipo_bq];
		}

		if ($this->tipo_bq == 'Bípede') {
			$this->deslocamento_terrestre = mt_rand(4,15);
		} else {
			$this->deslocamento_terrestre = mt_rand(6,18);
		}
	}

	public function monstros_deslocamento_voador(){
		$this->deslocamento_voador = mt_rand(12,36);
	}

	public function monstros_deslocamento_escalador(){
		$this->deslocamento_escalador = mt_rand(4,12);
	}

	public function monstros_deslocamento_escavador(){
		$this->deslocamento_escavador = mt_rand(4,9);
	}

	public function monstros_deslocamento_nadador(){
		$this->deslocamento_nadador = mt_rand(12,24);
	}

	public function monstros_caracteristicas($tipo = null){
		$monstros_caracteristicas = [
			'animais' => ['Valor de Inteligência 1 ou 2','Tendência sempre Neutra','Visão na penumbra'],
			'constructos' => ['Valor nulo de Constituição','Recebe um número de PV adicionais de acordo com seu tamanho: Pequeno, 10; Médio, 20; Grande, 30; Enorme 40; Descomunal 60; Colossal, 80','Imunidade a atordoamento, dano de habilidade, dano não-letal, doença, encantamento, fadiga, paralisia, necromancia, sono e veneno', 'Não precisam respirar, se alimentar e dormir', 'Não recuperam pontos de vida normalmente', 'Destruídos quando seus PV chegam a 0. Um construto destruído não pode ser revivido (pois nem estava vivo em primeiro lugar)', 'Visão no escuro'],
			'humanoides' => ['Nenhuma'],
			'espiritos' => ['Magias como reviver os mortos não afetam espíritos. Apenas desejo ou milagre podem trazer um espírito de volta à vida', 'Visão no escuro'],
			'monstros' => ['Visão no escuro'],
			'mortos-vivos' => ['Valor nulo de Constituição','Imunidade a atordoamento, dano de habilidade (apenas Força, Destreza ou Constituição), dano nãoletal, doença, encantamento, fadiga, paralisia, necromancia, sono e veneno', 'Não precisam respirar, se alimentar e dormir', 'Não recuperam pontos de vida normalmente', 'Sofrem dano com magias de cura e recuperam pontos de vida com magias de necromancia', 'Destruídos quando seus PV chegam a 0', 'Um morto-vivo não é afetado pelas magias reviver os mortos e ressurreição. A magia ressurreição verdadeira o transforma na criatura que era quando vivo', 'Independente de sua tendência verdadeira, mortosvivos reagem a habilidades (como destruir o mal dos paladinos) e magias (como proteção contra o mal) como se fossem Malignos', 'Visão no escuro'],
		];

		if ($tipo == null) {
			$sorteado = $monstros_caracteristicas[array_rand($monstros_caracteristicas)];
			$this->caracteristicas = $sorteado;
		} else {
			$this->caracteristicas = $monstros_caracteristicas[$tipo];
		}
	}

	public function habilidades_especiais(){
		$this->qtd_habilidades = mt_rand(0,4);
		$ajuste = 0;
		if ($this->qtd_habilidades == 1) {
			$ajuste = 0.1;
		} elseif ($this->qtd_habilidades == 2) {
			$ajuste = 0.2;
		} elseif ($this->qtd_habilidades == 3) {
			$ajuste = 0.3;
		} elseif ($this->qtd_habilidades == 4) {
			$ajuste = 0.4;
		} else {
			$ajuste = 0.2;
		}

		$ajuste_final = intval($this->nivel*$ajuste);

		if ($this->qtd_habilidades == 0) {
			$this->nd = $this->nivel-$ajuste_final;
		} else {
			$this->nd = $this->nivel+$ajuste_final;
		}
		
		$habilidades_especiais = [];

		if ($this->qtd_habilidades > 0) {
			$hab = file_get_contents(PATH_BASE.'config/locale/pt-br/txt/monstros_tormenta/habilidades_especiais.txt');
			$hab_array = explode("\n", $hab);
			$hab_escolhidas = array_rand($hab_array, $this->qtd_habilidades);
			if (is_array($hab_escolhidas)) {
				foreach ($hab_escolhidas as $key => $value) {
					$habilidades_especiais[] = $hab_array[$value];
				}
				$this->habilidades_especiais = $habilidades_especiais;
			} else {
				$this->habilidades_especiais = [$hab_array[$hab_escolhidas]];
			}
		} else {
			$this->habilidades_especiais = ['-'];
		}
	}
}