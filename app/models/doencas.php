<?php

class Doencas_Model {

	public $file_path;

	function __construct(){
		$this->file_path = PATH_BASE.'config/locale/pt-br/txt/doencas/doencas.txt';
	}

	public function sortear_doenca(){
		$file = file_get_contents($this->file_path);
		$doencas = explode("\n", $file);
		$doenca_escolhida = $doencas[array_rand($doencas)];
		$descricao_doenca = explode('|', $doenca_escolhida);
		$doenca = [
			'Efeito' => $descricao_doenca[1],
			'Nome' => $descricao_doenca[0],
			'Teste_de_Dificuldade' => $descricao_doenca[2]
		];

		return $doenca;
	}
}
