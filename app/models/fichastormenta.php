<?php


class Fichastormenta_Model{

	public $nivel;
	public $classe;
	public $raca;
	public $habilidades_basicas;
	public $pericias;
	public $tabela_de_classe;
	public $detalhes_de_magias;
	public $magias_conhecidas;
	public $magias_selecionadas;
	public $ajustes_raciais;

	public $tabela_de_talentos_combatente;
	public $tabela_de_talentos_conjurador;
	public $talentos_selecionados;
	public $tabela_de_armaduras;
	public $tabela_de_armas;

	public $armas_equipadas;
	public $escudos_equipados;
	public $armaduras_equipadas;

	public $pv;
	public $ca;
	public $tendencia;
	public $testes_resistencia;
	public $ataques;
	public $nome;
	public $lista_de_racas;

	function __construct($nivel = null, $classe = null, $raca = null, $tipo = 'padrao'){
		$this->path_base = PATH_BASE.'config/locale/pt-br/txt/tormenta_rpg/';
		if ($nivel == null) {
			$this->nivel = mt_rand(1, 20);
		} else {
			$this->nivel = $nivel;
		}

		$this->classes_config();
		$this->lista_de_racas();

		if ($classe == null) {
			$this->classe = array_rand($this->classes);
		} else {
			$this->classe = $classe;
		}

		if ($raca == null) {
			$this->raca = $this->lista_de_racas[array_rand($this->lista_de_racas)]['arquivo'];
		} else {
			$this->raca = $raca;
		}

		$this->nome();
		$this->tendencia();
		$this->habilidades_basicas($tipo);
		$this->ajustes_raciais($this->raca);
		$this->pericias();
		$this->habilidades_de_classe($this->classe);
		$this->detalhes_de_magias($this->classe);
		$this->magias_selecionadas($this->classe);
		$this->selecionar_magias($this->classe);
		$this->talentos_de_classe_combatente();
		$this->talentos_de_classe_conjurador();
		$this->selecionar_talentos_de_classe($this->classe);
		$this->armas_equipadas();
		$this->escudo_equipado();
		$this->armadura_equipada();
		$this->pontos_de_vida($this->classe);
		$this->ca();
		$this->testes_de_resistencia();
		$this->distribuir_pts_de_habilidade();
		$this->ataques();
	}

	public function nome(){
		$this->nome = (new Raffleitemfile_Core(PATH_BASE.'config/locale/pt-br/txt/nomes/neerlandes.txt', 1, 'FILE'))->getRaffleItens();
	}

	public function classes_config(){
		$this->classes = [
		
			'barbaro' 	=> ['nome'=>'Bárbaro','pv_base'=>24, 'pv_por_lv' => 6, 'pts_pericia'=>4, 'talentos_add' => 'Usar Armaduras Leves, Usar Armaduras Médias, Usar Armas Simples, Usar Armas Marciais, Usar Escudos, Fortitude Maior'],

			'bardo' 	=> ['nome'=>'Bardo','pv_base'=>12, 'pv_por_lv' => 3, 'pts_pericia'=>6, 'talentos_add' => 'Usar Armaduras Leves, Usar Armas Simples, Usar Armas Marciais, Usar Escudos, Reflexos Rápidos, Vontade de Ferro'],

			'clerigo' 	=> ['nome'=>'Clérigo','pv_base'=>16, 'pv_por_lv' => 4, 'pts_pericia'=>2, 'talentos_add' => 'Usar Armaduras Leves, Usar Armaduras Médias, Usar Armaduras Pesadas, Usar Armas Simples, Usar Escudos, Fortitude Maior, Vontade de Ferro'],

			'druida' 	=> ['nome'=>'Druida','pv_base'=>16, 'pv_por_lv' => 4, 'pts_pericia'=>4, 'talentos_add' => 'Usar Armaduras Leves, Usar Armaduras Médias, Usar Armas Simples, Usar Escudos, Fortitude Maior, Vontade de Ferro, Senso da Natureza'],

			'feiticeiro' 	=> ['nome'=>'Feiticeiro','pv_base'=>8, 'pv_por_lv' => 2, 'pts_pericia'=>2, 'talentos_add' => 'Usar Armas Simples, Vontade de Ferro'],

			'guerreiro' 	=> ['nome'=>'Guerreiro','pv_base'=>20, 'pv_por_lv' => 5, 'pts_pericia'=>2, 'talentos_add' => 'Usar Armaduras Leves, Usar Armaduras Médias, Usar Armaduras Pesadas, Usar Armas Simples, Usar Armas Marciais, Usar Escudos, Fortitude Maior'],

			'ladino' 	=> ['nome'=>'Ladino','pv_base'=>12, 'pv_por_lv' => 3, 'pts_pericia'=>8, 'talentos_add' => 'Usar Armaduras Leves, Usar Usar Armas Simples, Usar Armas Marciais, Usar Escudos, Reflexos Rápidos'],

			'mago' 	=> ['nome'=>'Mago','pv_base'=>8, 'pv_por_lv' => 2, 'pts_pericia'=>2, 'talentos_add' => 'Usar Armas Simples, Vontade de Ferro'],

			'monge' 	=> ['nome'=>'Monge','pv_base'=>16, 'pv_por_lv' => 4, 'pts_pericia'=>4, 'talentos_add' => 'Usar Armas Simples, Usar Arma Exótica (nunchaku, sai, shuriken), Fortitude Maior, Reflexos Rápidos, Vontade de Ferro, Ataque Desarmado Aprimorado'],

			'paladino' 	=> ['nome'=>'Paladino','pv_base'=>20, 'pv_por_lv' => 5, 'pts_pericia'=>2, 'talentos_add' => 'Usar Armaduras Leves, Usar Armaduras Médias, Usar Armaduras Pesadas, Usar Armas Simples, Usar Armas Marciais, Usar Escudos, Fortitude Maior'],

			'ranger' 	=> ['nome'=>'Ranger','pv_base'=>16, 'pv_por_lv' => 4, 'pts_pericia'=>6, 'talentos_add' => 'Usar Armaduras Leves, Usar Armaduras Médias, Usar Armas Simples, Usar Armas Marciais, Usar Escudos, Fortitude Maior, Reflexos Rápidos, Rastrear'],

			'samurai' 	=> ['nome'=>'Samurai','pv_base'=>20, 'pv_por_lv' => 5, 'pts_pericia'=>4, 'talentos_add' => 'Usar Armaduras Leves, Usar Armaduras Médias, Usar Armaduras Pesadas, Usar Armas Simples, Usar Armas Marciais, Fortitude Maior, Vontade de Ferro'],

			'swashbuckler' 	=> ['nome'=>'Swashbuckler','pv_base'=>16, 'pv_por_lv' => 4, 'pts_pericia'=>4, 'talentos_add' => 'Usar Armaduras Leves, Usar Armas Simples, Usar Armas Marciais, Reflexos Rápidos'],

			'ninja' 	=> ['nome'=>'Ninja','pv_base'=>16, 'pv_por_lv' => 4, 'pts_pericia'=>6, 'talentos_add' => 'Ataque Desarmado Aprimorado, Usar Armas Simples, Usar Armas Marciais, Usar armas exóticas (nunchaku, sai, shuriken), Reflexos Rápidos'],

			'miko' 	=> ['nome'=>'Miko','pv_base'=>12, 'pv_por_lv' => 3, 'pts_pericia'=>4, 'talentos_add' => 'Vontade de Ferro, Reflexos Rápidos, Fortitude Maior, Ataque Desarmado Aprimorado, Usar Armas Simples, Usar Armas Marciais'],

			'arqueiro' 	=> ['nome'=>'Arqueiro','pv_base'=>12, 'pv_por_lv' => 4, 'pts_pericia'=>2, 'talentos_add' => 'Usar Armas Simples, Usar Armas Marciais, Usar Armaduras Leves, Usar Armaduras Médias, Reflexos Rápidos'],

			'onmyouji' 	=> ['nome'=>'Onmyouji','pv_base'=>8, 'pv_por_lv' => 2, 'pts_pericia'=>2, 'talentos_add' => 'Usar Armas Simples, Vontade de Ferro']
		];
	}

	public function qtd_talentos(){
		return intval((($this->nivel - 1)/2) + 1);
	}

	public function aumento_habilidade(){
		return intval($this->nivel/2);
	}

	public function bonus(){
		return intval($this->nivel/2);
	}

	public function distribuir_pts_de_habilidade(){
		$pts = $this->aumento_habilidade();
		if ($pts > 0) {
			while ($pts > 0) {
				$hab_sorteada = array_rand($this->habilidades_basicas);
				$this->habilidades_basicas[$hab_sorteada] += 1;
				$pts--;
			}
		}
	}

	public function ataques(){
		$ataques = [
			'corpo_a_corpo' => intval($this->tabela_de_classe[$this->nivel-1]['bba']) + $this->modificador($this->habilidades_basicas['for']),
			'distancia' => intval($this->tabela_de_classe[$this->nivel-1]['bba']) + $this->modificador($this->habilidades_basicas['des']),
			'dano' => $this->bonus() + $this->modificador($this->habilidades_basicas['for'])
		];

		$this->ataques = $ataques;
	}

	public function pontos_de_vida($classe){
		$nivel = $this->nivel-1;
		$pv_inicial = $this->classes[$classe]['pv_base']+$this->modificador($this->habilidades_basicas['con']);
		$pv_por_niveis = ($this->classes[$classe]['pv_por_lv']+$this->modificador($this->habilidades_basicas['con']))*$nivel;
		$this->pv = ($pv_inicial+$pv_por_niveis);
	}

	public function ca(){
		$ca = 10 + $this->modificador($this->habilidades_basicas['des'])+$this->bonus();
		
		if ($this->escudos_equipados != null) {
			$ca += intval($this->escudos_equipados['bonus_na_ca']);
		}

		if ($this->armaduras_equipadas != null) {
			$ca += intval($this->armaduras_equipadas['bonus_na_ca']);
		}

		$this->ca = $ca;
	}

	public function testes_de_resistencia(){
		$testes = [
			'fort' => $this->bonus() + $this->modificador($this->habilidades_basicas['con']),
			'refl' => $this->bonus() + $this->modificador($this->habilidades_basicas['des']),
			'vont' => $this->bonus() + $this->modificador($this->habilidades_basicas['con'])
		];

		$this->testes_resistencia = $testes;
	}

	public function tendencia(){
		$tendencias = ['LB','NB','CB','LN','N','CN','LM', 'NM', 'CM'];
		$indice = array_rand($tendencias);
		$this->tendencia = $tendencias[$indice];
	}

	public function lista_de_racas(){
		$racas_arquivo = scandir($this->path_base.'racas');
		unset($racas_arquivo[0]);
		unset($racas_arquivo[1]);
		$racas = [];
		foreach ($racas_arquivo as $key => $value) {
			$racas_arquivo = explode("\n", file_get_contents($this->path_base.'racas/'.$value));
			$nome_raca = explode(':',$racas_arquivo[0])[0];
			$racas[] = ['arquivo'=>trim(str_replace('.txt', '', $value)), 'nome'=>$nome_raca];
		}

		$this->lista_de_racas = $racas;
	}

	public function ajustes_raciais($raca){
		$racas_arquivo = scandir($this->path_base.'racas');
		unset($racas_arquivo[0]);
		unset($racas_arquivo[1]);
		$racas = [];
		foreach ($racas_arquivo as $key => $value) {
			$racas[] = trim(str_replace('.txt', '', $value));
		}

		$racas_arquivo = explode("\n", file_get_contents($this->path_base.'racas/'.$raca.'.txt'));
		$nome_raca = explode(':',$racas_arquivo[0])[0];
		$ajustes_hab = explode(',', explode(':',$racas_arquivo[0])[1]);

		$ajustes = [];
		$ajustes['raca'] = $nome_raca;
		$ajustes['descricao'] = $racas_arquivo[1];
		if (is_array($ajustes_hab)) {
			foreach ($ajustes_hab as $key => $value) {
				$hab = explode(' ', $value)[1];
				$val = explode(' ', $value)[2];
				$ajustes['ajustes'][] = ['hab'=>strtolower($hab), 'bonus'=>$val];
			}
		} else {
			$hab = explode(' ', $ajustes_hab)[1];
			$val = explode(' ', $ajustes_hab)[2];
			$ajustes['ajustes'][] = ['hab'=>strtolower($hab), 'bonus'=>$val];
		}

		$this->ajustes_raciais = $ajustes;
		$this->aplicar_ajustes_raciais($ajustes);
	}

	public function aplicar_ajustes_raciais($ajustes){
		foreach ($ajustes['ajustes'] as $key => $value) {
			if ($value['hab'] == 'xxx') {
				$hab = array_rand($this->habilidades_basicas);
				$this->habilidades_basicas[$hab] += intval($value['bonus']);
			} elseif (strlen($value['hab']) == 4) {
				$habilidades = ['for','des','con','int','sab','car'];
				$hab_descartada = strtolower(substr($value['hab'], 1, 3));
				unset($habilidades[array_search($hab_descartada, $habilidades)]);
				$hab = $habilidades[array_rand($habilidades)];
				$this->habilidades_basicas[$hab] += intval($value['bonus']);
			} elseif (strlen($value['hab']) >= 5) {
				$habilidades = ['for','des','con','int','sab','car'];
				$habilidades_descartadas = explode('-', $value['hab']);
				foreach ($habilidades_descartadas as $key_hab => $value_hab) {
					$hab_descartada = substr($value_hab, 1, 3);
					unset($habilidades[array_search($hab_descartada, $habilidades)]);
				}
				$hab = $habilidades[array_rand($habilidades)];
				$this->habilidades_basicas[$hab] += intval($value['bonus']);
			} else {
				$this->habilidades_basicas[$value['hab']] += intval($value['bonus']);
			}
		}
	}

	public function rolar4d6_descartando_o_menor(){
		$rolagens = [mt_rand(1,6), mt_rand(1,6), mt_rand(1,6), mt_rand(1,6)]; 
		$total = 0;
		$menor = 6;
		foreach ($rolagens as $key => $value) {
			$total += $value;
			if ($value < $menor) {
				$menor = $value;
			}
		}

		$rolagem_final = ($total - $menor);

		if ($rolagem_final < 10) {
			$rolagem_final = mt_rand(10,11);
		}
		return $rolagem_final;
	}

	public function rolar3d6(){
		$rolagens = [mt_rand(1,6), mt_rand(1,6), mt_rand(1,6)]; 
		$total = 0;
		foreach ($rolagens as $key => $value) {
			$total += $value;
		}

		if ($total < 8) {
			$total = mt_rand(8,10);
		}
		return $total;
	}

	public function rolar2d6mais6(){
		$rolagens = [mt_rand(1,6), mt_rand(1,6)]; 
		$total = 0;
		foreach ($rolagens as $key => $value) {
			$total += $value+6;
		}

		if ($total < 8) {
			$total = mt_rand(8,10);
		}
		return $total;
	}

	public function rolar1d20(){
		$rolagem_final = mt_rand(1,20);
		if ($rolagem_final < 8) {
			$rolagem_final = mt_rand(8,10);
		}
		return $rolagem_final;
	}

	public function rolagem_elite(){
		$rolagens = [17,15,13,12,10,8];
		shuffle($rolagens);
		return $rolagns;
	}


	public function habilidades_basicas($tipo = 'padrao'){
		if ($tipo == 'elite') {
			$rolagens = $this->rolagem_elite();
			$this->habilidades_basicas = [
				'for' => $rolagens[0],
				'des' => $rolagens[1],
				'con' => $rolagens[2],
				'int' => $rolagens[3],
				'sab' => $rolagens[4],
				'car' => $rolagens[5],
				'nul' => 0
			];
		} else {
			$tipos = [
				'padrao' => 'rolar4d6_descartando_o_menor',
				'classica' => 'rolar3d6',
				'heroica' => 'rolar2d6mais6',
				'audaciosa' => 'rolar1d20'
			];

			$funcao = $tipos[$tipo];
			$this->habilidades_basicas = [
				'for' => $this->$funcao(),
				'des' => $this->$funcao(),
				'con' => $this->$funcao(),
				'int' => $this->$funcao(),
				'sab' => $this->$funcao(),
				'car' => $this->$funcao(),
				'nul' => 0
			];
		}
	}

	public function modificador($valor){
		if ($valor == 9 || $valor == 8) {
			return -1;
		} elseif ($valor == 7 || $valor == 6) {
			return -2;
		} elseif ($valor == 5 || $valor == 4) {
			return -3;
		} elseif ($valor == 3 || $valor == 2) {
			return -4;
		} elseif ($valor == 1 || $valor == 0) {
			return -5;
		} else {
			$mod = intval(($valor - 10)/2);
			if($mod > 0){
				return "+{$mod}";
			} else {
				return " {$mod}";
			}
		}
	}

	public function pericias(){
		$tabela_de_pericias = [];
		$arquivo_de_pericia = explode("\n", file_get_contents($this->path_base.'pericias/'.$this->classe.'.txt'));
		
		foreach ($arquivo_de_pericia as $key => $value) {
			$nome = explode('-', $value)[0];
			$hab_chave = explode('-', $value)[1];
			$tabela_de_pericias[] = ['nome' => $nome, 'hab_chave' => $hab_chave, 'de_classe' => true];
		}

		$qtd_pericias = $this->classes[$this->classe]['pts_pericia'];

		if ($qtd_pericias > count($arquivo_de_pericia)) {
			$arquivo_de_pericia_gerais = explode("\n", file_get_contents($this->path_base.'pericias/pericias_gerais.txt'));
			foreach ($arquivo_de_pericia_gerais as $key => $value) {
				foreach ($arquivo_de_pericia as $key1 => $value1) {
					if ($value == $value1) {
						unset($arquivo_de_pericia_gerais[$key]);
					}
				}
			}
			$pericias_adicionais = $qtd_pericias - count($arquivo_de_pericia);
			// echo $pericias_adicionais;
			$pericias_adicionais_sorteadas = array_rand($arquivo_de_pericia_gerais, $pericias_adicionais);
			foreach ($pericias_adicionais_sorteadas as $key => $value) {
				$nome = explode('-', $arquivo_de_pericia_gerais[$value])[0];
				$hab_chave = explode('-', $arquivo_de_pericia_gerais[$value])[1];
				$tabela_de_pericias[] = ['nome' => $nome, 'hab_chave' => $hab_chave, 'de_classe' => false];
				
			}
		}

		shuffle($tabela_de_pericias);

		foreach ($tabela_de_pericias as $key => $value) {
			$hab_chave = trim(strtolower($value['hab_chave']));
			$grad = 0;
			if ($value['de_classe'] == true) {
				$grad = $this->nivel+3;
			} else {
				$grad = intval($this->nivel/2);
			}
			$tabela_de_pericias_com_valores[] = [
					'nome' => $value['nome'],
					'habilidade_chave' => $value['hab_chave'],
					'graduacao' => $grad,
					'mod_hab_chave' => $this->modificador($this->habilidades_basicas[$hab_chave]),
					'total' => $grad + $this->modificador($this->habilidades_basicas[$hab_chave])
				];
		}

		$this->pericias = $tabela_de_pericias_com_valores;
	}

	public function habilidades_de_classe($classe){
		$arquivo_de_classe = file_get_contents($this->path_base.'classe/'.$classe.'.txt');
		$arquivo_de_classe = explode("\n", $arquivo_de_classe);
		foreach ($arquivo_de_classe as $key => $value) {
			$linha_de_habilidades[] = explode(' ', $value);
		}

		$tabela_de_classe = [];
		$linha['especial'] = '';

		foreach ($linha_de_habilidades as $key => $value) {
			foreach ($value as $key1 => $value1) {
				if ($key1 == 0) {
					$linha['nivel'] = $value1;		
				}

				if ($key1 == 1) {
					$linha['bba'] = str_replace('+', '', $value1);		
				}

				if ($key1 > 1) {
					$linha['especial'] .= " {$value1} ";
				}
			}

			$tabela_de_classe[] = $linha;
		}

		$this->tabela_de_classe = $tabela_de_classe;
	}

	public function pm($classe){
		$pm = 0;
		switch ($classe) {
			case 'bardo':$pm = 1 + $this->modificador($this->habilidades_basicas['car']) + (($this->nivel-1) * 2);
			break;

			case 'clerigo':$pm = 1 + $this->modificador($this->habilidades_basicas['sab']) + (($this->nivel-1) * 3);
			break;

			case 'druida':$pm = 1 + $this->modificador($this->habilidades_basicas['sab']) + (($this->nivel-1) * 3);
			break;

			case 'feiticeiro':$pm = 3 + $this->modificador($this->habilidades_basicas['car']) + (($this->nivel-1) * 3);
			break;

			case 'mago':$pm = 1 + $this->modificador($this->habilidades_basicas['int']) + (($this->nivel-1) * 3);
			break;

			case 'paladino':$pm = 1 + $this->modificador($this->habilidades_basicas['sab']) + (($this->nivel-1));
			break;

			case 'ranger':$pm = 1 + $this->modificador($this->habilidades_basicas['sab']) + (($this->nivel-1));
			break;

			case 'miko':$pm = 1 + $this->modificador($this->habilidades_basicas['car']) + (($this->nivel-1) * 3);
			break;

			case 'onmyouji':$pm = 1 + $this->modificador($this->habilidades_basicas['int']) + (($this->nivel-1) * 3);
			break;
		}

		return $pm;
	}

	public function magias_conhecidas($classe){
		$magias_conhecidas = [];
		switch ($classe) {
			case 'bardo':$magias_conhecidas = ['0'=>4, '1'=>$this->modificador($this->habilidades_basicas['car'])+1, 'x'=>$this->nivel-1];
			break;

			case 'clerigo':$magias_conhecidas = ['0'=>4, '1'=>$this->modificador($this->habilidades_basicas['sab'])+1, 'x'=>($this->nivel-1)*2];
			break;

			case 'druida':$magias_conhecidas = ['0'=>4, '1'=>$this->modificador($this->habilidades_basicas['sab'])+1, 'x'=>($this->nivel-1)*2];
			break;

			case 'feiticeiro':$magias_conhecidas = ['0'=>4, '1'=>$this->modificador($this->habilidades_basicas['car'])+1, 'x'=>($this->nivel-1)];
			break;

			case 'mago':$magias_conhecidas = ['0'=>5, '1'=>$this->modificador($this->habilidades_basicas['int'])+3, 'x'=>($this->nivel-1)*2];
			break;

			case 'paladino':$magias_conhecidas = ['x'=>1+$this->modificador($this->habilidades_basicas['sab'])+($this->nivel-1)];
			break;

			case 'ranger':$magias_conhecidas = ['x'=>1+$this->modificador($this->habilidades_basicas['sab'])+($this->nivel-1)];
			break;

			case 'miko':$magias_conhecidas = ['0'=>4, '1'=>$this->modificador($this->habilidades_basicas['car'])+1, 'x'=>($this->nivel-1)];
			break;

			case 'onmyouji':$magias_conhecidas = ['0'=>4, '1'=>$this->modificador($this->habilidades_basicas['int'])+1, 'x'=>($this->nivel-1)];
			break;
		}

		return $magias_conhecidas;
	}

	public function circulo_magico_maximo($classe){
		$circulo_maximo = 0;
		if ($classe == 'bardo') {
			if ($this->nivel >= 16) {
				$circulo_maximo = 6;
			} else {
				$circulo_maximo = (intval(($this->nivel-1)/3)+1);
			}
		}

		if ($classe == 'clerigo' || $classe == 'druida' || $classe == 'feiticeiro' || $classe == 'mago' || $classe == 'miko' || $classe == 'onmyouji') {
			$circulo_maximo = (intval(($this->nivel-1)/2)+1);
		}

		if ($classe == 'paladino' || $classe == 'ranger') {
			if ($this->nivel <= 4) {
				$circulo_maximo = 0;
			} else {
				$circulo_maximo = (intval(($this->nivel-1)/4));
			}
		}

		if ($circulo_maximo == 10) {
			$circulo_maximo = 9;
		}
		
		return $circulo_maximo;
	}

	public function detalhes_de_magias($classe){
		$classes_conj = ['bardo','clerigo','druida','feiticeiro','mago','paladino','ranger','miko','onmyouji'];
		if (in_array($classe, $classes_conj)) {
			$detalhes_de_magias = [
				'bardo' => ['hab_chave'=>'car', 'pm'=> $this->pm($classe), 'magias_conhecidas'=>$this->magias_conhecidas($classe), 'tipo' => 'arcana', 'circulo_maximo'=> $this->circulo_magico_maximo($classe)],

				'clerigo' => ['hab_chave'=>'sab', 'pm'=> $this->pm($classe), 'magias_conhecidas'=>$this->magias_conhecidas($classe), 'tipo' => 'divina', 'circulo_maximo'=> $this->circulo_magico_maximo($classe)],
				
				'druida' => ['hab_chave'=>'sab', 'pm'=> $this->pm($classe), 'magias_conhecidas'=>$this->magias_conhecidas($classe), 'tipo' => 'divina', 'circulo_maximo'=> $this->circulo_magico_maximo($classe)],
				
				'feiticeiro' => ['hab_chave'=>'car', 'pm'=> $this->pm($classe), 'magias_conhecidas'=>$this->magias_conhecidas($classe), 'tipo' => 'arcana', 'circulo_maximo'=> $this->circulo_magico_maximo($classe)],
				
				'mago' => ['hab_chave'=>'int', 'pm'=> $this->pm($classe), 'magias_conhecidas'=>$this->magias_conhecidas($classe), 'tipo' => 'arcana', 'circulo_maximo'=> $this->circulo_magico_maximo($classe)],
				
				'paladino' => ['hab_chave'=>'sab', 'pm'=> $this->pm($classe), 'magias_conhecidas'=>$this->magias_conhecidas($classe), 'tipo' => 'divina', 'circulo_maximo'=> $this->circulo_magico_maximo($classe)],
				
				'ranger' => ['hab_chave'=>'sab', 'pm'=> $this->pm($classe), 'magias_conhecidas'=>$this->magias_conhecidas($classe), 'tipo' => 'divina', 'circulo_maximo'=> $this->circulo_magico_maximo($classe)],

				'miko' => ['hab_chave'=>'car', 'pm'=> $this->pm($classe), 'magias_conhecidas'=>$this->magias_conhecidas($classe), 'tipo' => 'divina', 'circulo_maximo'=> $this->circulo_magico_maximo($classe)],

				'onmyouji' => ['hab_chave'=>'int', 'pm'=> $this->pm($classe), 'magias_conhecidas'=>$this->magias_conhecidas($classe), 'tipo' => 'divina', 'circulo_maximo'=> $this->circulo_magico_maximo($classe)]
				];

			$this->detalhes_de_magias = $detalhes_de_magias;
		} else {
			$this->detalhes_de_magias = null;
		}
	}

	public function magias_selecionadas($classe){
		if ($this->detalhes_de_magias != null) {
			$magias_conhecidas = $this->detalhes_de_magias[$classe]['magias_conhecidas'];
			$circulo_maximo = $this->detalhes_de_magias[$classe]['circulo_maximo'];
			if ($circulo_maximo > 0 and $magias_conhecidas > 0) {
				if ($classe != 'paladino' && $classe != 'ranger') {
					$qtd_de_magias = $magias_conhecidas['x'];
					unset($magias_conhecidas['x']);

					for ($i=0; $i < $circulo_maximo-1; $i++) { 
						$indice = $i+2;
						$magias_conhecidas["{$indice}"] = 0;
					}

					while ($qtd_de_magias > 0) {
						$circulo_escolhido = mt_rand(1, $circulo_maximo);
						$magias_conhecidas[$circulo_escolhido] += 1;
						$qtd_de_magias--;
					}
				} elseif ($classe == 'paladino' || $classe == 'ranger') {
					$qtd_de_magias = $magias_conhecidas['x'];
					unset($magias_conhecidas['x']);

					for ($i=1; $i <= $circulo_maximo; $i++) { 
						$magias_conhecidas["{$i}"] = 0;
					}

					while ($qtd_de_magias > 0) {
						$circulo_escolhido = mt_rand(1, $circulo_maximo);
						$magias_conhecidas[$circulo_escolhido] += 1;
						$qtd_de_magias--;
					}
				}
				
				$this->magias_conhecidas = $magias_conhecidas;
			} else {
				$this->magias_conhecidas = null;
			}
		} else {
			$this->magias_conhecidas = null;
		}
	}

	public function selecionar_magias($classe){
		$magias_selecionadas = [];
		if (!empty($this->magias_conhecidas)) {
			foreach ($this->magias_conhecidas as $key => $value) {
				$tipo = $this->detalhes_de_magias[$classe]['tipo'];
				$magias = explode("\n", file_get_contents($this->path_base.'magias/'.$tipo.'/'.$key.'.txt'));
				if ($value >= 1) {
					$indices = array_rand($magias, $value);
					if (is_array($indices)) {
						foreach ($indices as $key1 => $value1) {
							$magias_selecionadas[$key][] = $magias[$value1];
						}
					} else {
						$magias_selecionadas[$key][] = $magias[$indices];
					}
				} else {
					$magias_selecionadas[$key] = 'Nenhuma magia foi selecionada para este círculo.';
				}
			}

			$this->magias_selecionadas = $magias_selecionadas;
		} else {
			$this->magias_selecionadas = $magias_selecionadas;
		}
	}

	public function talentos_de_classe_combatente(){
		$talentos = file_get_contents($this->path_base.'talentos/talentos_de_combate.txt');
		$talentos = explode("\n", $talentos);
		foreach ($talentos as $key => $value) {
			$linha_de_caracteristicas[] = explode('|', $value);
		}

		$tabela_de_talentos_combatente = [];

		foreach ($linha_de_caracteristicas as $key => $value) {
			foreach ($value as $key1 => $value1) {
				if ($key1 == 0) {
					$linha['nome'] = $value1;		
				}

				if ($key1 == 1) {
					$linha['pre_requisito'] = $value1;		
				}

				if ($key1 == 2) {
					$linha['beneficios'] = $value1;		
				}	
			}

			$tabela_de_talentos_combatente[] = $linha;
		}

		$this->tabela_de_talentos_combatente = $tabela_de_talentos_combatente;
	}

	public function talentos_de_classe_conjurador(){
		$talentos = file_get_contents($this->path_base.'talentos/talentos_de_magia.txt');
		$talentos = explode("\n", $talentos);
		foreach ($talentos as $key => $value) {
			$linha_de_caracteristicas[] = explode('|', $value);
		}

		$tabela_de_talentos_conjurador = [];

		foreach ($linha_de_caracteristicas as $key => $value) {
			foreach ($value as $key1 => $value1) {
				if ($key1 == 0) {
					$linha['nome'] = $value1;		
				}

				if ($key1 == 1) {
					$linha['pre_requisito'] = $value1;		
				}

				if ($key1 == 2) {
					$linha['beneficios'] = $value1;		
				}	
			}

			$tabela_de_talentos_conjurador[] = $linha;
		}

		$this->tabela_de_talentos_conjurador = $tabela_de_talentos_conjurador;
	}

	public function selecionar_talentos_de_classe($classe){
		$lista_de_talentos = [];

		$combatente = ['barbaro','guerreiro','ladino','monge','paladino','ranger','samurai','swashbuckler','ninja','arqueiro'];		
		$conjurador = ['bardo','clerigo','druida','feiticeiro','mago','miko','onmyouji'];

		if (in_array($classe, $combatente)) {
			$lista_de_talentos = $this->tabela_de_talentos_combatente;
		} elseif (in_array($classe, $conjurador)) {
			$lista_de_talentos = $this->tabela_de_talentos_conjurador;
		}

		$qtd = intval(($this->nivel/2));
		// $this->tabela_de_classe[$this->nivel-1]['especial'] .= " e + {$qtd} Talentos adicionais.";
		
		$niveis = ['7'=>1,'6'=>3,'5'=>6,'4'=>9,'3'=>12,'2'=>15,'1'=>18];
		
		$talento_por_nivel;
		$talentos_selecionados = [];

		// adicionar novos talentos sem repetir os talentos base

		$talentos_add = explode(',', $this->classes[$classe]['talentos_add']);

		foreach ($talentos_add as $key => $value) {
			$talentos_selecionados[] = ['nome'=>trim($value), 'pre_requisito'=>'-', 'beneficios'=>''];
		}

		$this->qtd_talentos_add = count($talentos_add);

		while ($qtd != 0) {
			// $nivel = $niveis[$qtd];
			$nivel = $this->nivel;
			$escolhido = array_rand($lista_de_talentos);
			$um_talento = $lista_de_talentos[$escolhido];
			$pre_requisitos = $um_talento['pre_requisito'];
			$status = $this->detectar_requisito('*'.$um_talento['pre_requisito'], $lista_de_talentos);
			
			$tipo = explode(',',$um_talento['pre_requisito']);
			
			if (count($tipo) == 1) {
				$tipo = 1;
			} else {
				$tipo = 2;
			}

			if ($tipo == 1) {
				$validado = 1;
				if (isset($status['status']) && $status['status'] == 'ok') {
					$validado = 1;
				} elseif (isset($status['habilidade'])) {
					if ($this->habilidades_basicas[strtolower($status['habilidade']['hab'])] >= $status['habilidade']['val']) {
						$validado = 1;
					} else {
						$validado = 2;
					}
				} elseif (isset($status['bba'])) {
					$bba = substr($this->tabela_de_classe[$nivel]['bba'], 0, 2);
					$bba = intval(trim(str_replace('/', '', $bba)));
					if ($bba >= $status['bba']['bba']) {
						$validado = 1;
					} else {
						$validado = 2;
					}
				} elseif (isset($status['nivel'])) {
					$tipo = trim($status['nivel']['tipo']);
					$classe = trim($this->classe);
					if ($tipo == trim('personagem')) {
						if ($nivel >= $status['nivel']['nivel']) {
							$validado = 1;
						} else {
							$validado = 2;
						}
					} elseif ($nivel >= $status['nivel']['nivel'] && $tipo == $classe) {
						$validado = 1;
					} else {
						$validado = 2;
					}
				} elseif (isset($status['pericia'])) {
					foreach ($this->pericias as $key => $value) {
						if ($value['nome'] == $status['pericia']['pericia'] && $value['graduacao'] >= $status['pericia']['grad']) {
							$validado = 1;
						} else {
							$validado = 2;
						}
					}
				} elseif (isset($status['talento'])) {
					$talento_procurado = array_search($status['talento']['nome'], $talentos_selecionados);
					if ($talento_procurado == null) {
						$validado = 2;
					} else {
						$validado = 1;
					}
				} elseif (isset($status['habilidade_de_classe'])) {
					$hab_procurado = array_search($status['habilidade_de_classe']['nome'], $this->tabela_de_classe['especial']);
					if ($hab_procurado == null) {
						$validado = 2;
					} else {
						$validado = 1;
					}
				} elseif (isset($status['tendencia'])) {
					$tendencias = [
						'Caótica' => ['','CB','CN','CM'],
						'Bondosa' => ['','LB','NB','CB'],
						'não Bondosa' => ['','LN','N','CN','LM', 'NM', 'CM']
					];
					$tendencia_de_pre_requisito = trim($status['tendencia']['nome']);
					$tendencia_aceita = $tendencias[$tendencia_de_pre_requisito];
					$hab_procurado = array_search($this->tendencia, $tendencia_aceita);
					
					if ($hab_procurado == null) {
						$validado = 2;
					} else {
						$validado = 1;
					}
				}

				if ($validado == 1) {
					$talentos_selecionados[] = $um_talento;
					unset($lista_de_talentos[$escolhido]);
					$qtd -= 1;
				}

			} else {
				$validado = [];
				foreach ($status as $key => $value) {
					foreach ($value as $key1 => $value1) {
						if (isset($value1['status']) and $value1['status'] == 'ok') {
							$validado[] = 1;
						
						}elseif (isset($value1['hab']) and isset($value1['val'])) {
							if ($this->habilidades_basicas[strtolower($value1['hab'])] >= $value1['val']) {
								$validado[] = 1;
							} else {
								$validado[] = 2;	
							}
						
						}elseif (isset($value1['nome'])) {
							$talento_procurado = array_search($value1['nome'], $talentos_selecionados);
							if ($talento_procurado == null) {
								$validado[] = 2;
							} else {
								$validado[] = 1;
							}
						
						}elseif (isset($value1['grad']) and isset($value1['pericia'])) {
							foreach ($this->pericias as $key => $value) {
								if ($value['nome'] == $value1['pericia'] && $value['graduacao'] >= $value1['grad']) {
									$validado[] = 1;
								} else {
									$validado[] = 2;
								}
							}
						
						}elseif (isset($value1['nivel']) and isset($value1['tipo'])) {
							$tipo = trim($value1['tipo']);
							$classe = trim($this->classe);
							if ($tipo == 'personagem') {
								if ($nivel >= $value1['nivel']) {
									$validado[] = 1;
								} else {
									$validado[] = 2;
								}
							} elseif ($nivel >= $value1['nivel'] && $tipo == $classe) {
								$validado[] = 1;
							} else {
								$validado[] = 2;
							}

						}elseif (isset($value1['valor'])) {
							$bba = substr($this->tabela_de_classe[$nivel-1]['bba'], 0, 2);
							$bba = intval(trim(str_replace('/', '', $bba)));
							if ($bba >= $value1['valor']) {
								$validado[] = 1;
							} else {
								$validado[] = 2;
							}
						}
					}
				}

				$validacao_do_talento = 1;
				foreach ($validado as $key => $value) {
					if ($value == 2) {
						$validacao_do_talento = 2;
					} 	
				}
				
				if ($validacao_do_talento == 1) {
					$talentos_selecionados[] = $um_talento;
					unset($lista_de_talentos[$escolhido]);
					$qtd -= 1;
				}
			}
		}

		$this->talentos_selecionados = $talentos_selecionados;
	}

	public function detectar_requisito($requisito, $lista_de_talentos){
		if (str_replace('*','',$requisito) == '-') {
			$status['status'] = ['sucesso' => 'ok'];
		} else{
			$virgula = strpos($requisito,',');
			if ($virgula == 0) {
				if (str_replace('*','',explode(' ',$requisito)[0]) == 'For') {
					$checar_req = explode(' ', $requisito);
					$status['habilidade'] = ['hab' => str_replace('*','',$checar_req[0]), 'val' => $checar_req[1]];	
				
				} elseif (str_replace('*','',explode(' ',$requisito)[0]) ==  'Des') {
					$checar_req = explode(' ', $requisito);
					$status['habilidade'] = ['hab' => str_replace('*','',$checar_req[0]), 'val' => $checar_req[1]];	
				
				} elseif (str_replace('*','',explode(' ',$requisito)[0]) ==  'Con') {
					$checar_req = explode(' ', $requisito);
					$status['habilidade'] = ['hab' => str_replace('*','',$checar_req[0]), 'val' => $checar_req[1]];	
				
				} elseif (str_replace('*','',explode(' ',$requisito)[0]) ==  'Int') {
					$checar_req = explode(' ', $requisito);
					$status['habilidade'] = ['hab' => str_replace('*','',$checar_req[0]), 'val' => $checar_req[1]];	
				
				} elseif (str_replace('*','',explode(' ',$requisito)[0]) ==  'Sab') {
					$checar_req = explode(' ', $requisito);
					$status['habilidade'] = ['hab' => str_replace('*','',$checar_req[0]), 'val' => $checar_req[1]];
				
				} elseif (str_replace('*','',explode(' ',$requisito)[0]) ==  'Car') {
					$checar_req = explode(' ', $requisito);
					$status['habilidade'] = ['hab' => str_replace('*','',$checar_req[0]), 'val' => $checar_req[1]];
				
				} elseif (strpos($requisito,'bônus base de ataque') > 0) {
					$checar_req = explode('+', $requisito);
					$status['bba'] = ['bba' => str_replace('*','',$checar_req[1])];
				
				} elseif (strpos($requisito,'° nível de') > 0) {
					$checar_req = explode('° nível de', $requisito);
					$status['nivel'] = ['nivel' => str_replace('*','',$checar_req[0]), 'tipo' => $checar_req[1]];
				
				} elseif (strpos($requisito,'graduação em') > 0) {
					$checar_req = explode('graduação em', $requisito);
					$status['pericia'] =  ['grad' => str_replace('*','',$checar_req[0]), 'pericia' => $checar_req[1]];
					
				} elseif (strpos($requisito,'habilidade de') > 0) {
					$checar_req = explode(' ', $requisito);
					foreach ($checar_req as $key => $value) {
						if ($key >= 2) {
							$nome_habilidade .= " {$value} ";
						}
					}
					$status['habilidade_de_classe'] =  ['nome' => $nome_habilidade];
					
				} elseif (strpos($requisito,'Tendência') > 0) {
					$checar_req = explode(' ', $requisito);
					$nome_tendencia = '';
					foreach ($checar_req as $key => $value) {
						if ($key >= 1) {
							$nome_tendencia .= " {$value} ";
						}
					}
					$status['tendencia'] =  ['nome' => $nome_tendencia];

				} else {
					$status['talento'] =  ['nome' => str_replace('*','',$requisito)];
				}
			} else {
				$requisitos = explode(',',$requisito);
				foreach ($requisitos as $key => $value) {
					if ($key > 0) {
						$value = '*'.trim($value);
					}
					
					if (str_replace('*','',explode(' ',$value)[0]) == 'For') {
						$checar_req = explode(' ', $value);
						$status['habilidade'][] = ['hab' => str_replace('*','',$checar_req[0]), 'val' => $checar_req[1]];	
					
					} elseif (str_replace('*','',explode(' ',$value)[0]) == 'Des') {
						$checar_req = explode(' ', $value);
						$status['habilidade'][] = ['hab' => str_replace('*','',$checar_req[0]), 'val' => $checar_req[1]];	
					
					} elseif (str_replace('*','',explode(' ',$value)[0]) == 'Con') {
						$checar_req = explode(' ', $value);
						$status['habilidade'][] = ['hab' => str_replace('*','',$checar_req[0]), 'val' => $checar_req[1]];	
					
					} elseif (str_replace('*','',explode(' ',$value)[0]) == 'Int') {
						$checar_req = explode(' ', $value);
						$status['habilidade'][] = ['hab' => str_replace('*','',$checar_req[0]), 'val' => $checar_req[1]];	
					
					} elseif (str_replace('*','',explode(' ',$value)[0]) == 'Sab') {
						$checar_req = explode(' ', $value);
						$status['habilidade'][] = ['hab' => str_replace('*','',$checar_req[0]), 'val' => $checar_req[1]];
					
					} elseif (str_replace('*','',explode(' ',$value)[0]) == 'Car') {
						$checar_req = explode(' ', $value);
						$status['habilidade'][] = ['hab' => str_replace('*','',$checar_req[0]), 'val' => $checar_req[1]];
					
					} elseif (strpos($value,'bônus base de ataque') > 0) {
						$checar_req = explode('+', $value);
						$status['bba'][] =  ['valor' => str_replace('*','',$checar_req[1])];
					
					} elseif (strpos($value,'° nível de') > 0) {
						$checar_req = explode('° nível de', $value);
						$status['nivel'][] =  ['nivel' => str_replace('*','',$checar_req[0]), 'tipo' => $checar_req[1]];

					} elseif (strpos($value,'graduação em') > 0) {
						$checar_req = explode('graduação em', $value);
						$status['pericia'][] =  ['grad' => str_replace('*','',$checar_req[0]), 'pericia' => $checar_req[1]];

					} elseif (strpos($value,'habilidade de') > 0) {
						$checar_req = explode(' ', $value);
						$nome_habilidade = '';
						foreach ($checar_req as $key1 => $value1) {
							if ($key1 >= 2) {
								$nome_habilidade .= " {$value1} ";
							}
						}
						$status['habilidade_de_classe'][] =  ['nome' => $nome_habilidade];
						
					} elseif (strpos($value,'Tendência') > 0) {
						$checar_req = explode(' ', $value);
						foreach ($checar_req as $key1 => $value1) {
							if ($key1 >= 1) {
								$nome_tendencia .= " {$value1} ";
							}
						}
						$status['tendencia'][] =  ['nome' => $nome_tendencia];
						
					} else {
						$status['talento'][] =  ['nome' => str_replace('*','',$value)];
					}
				}
			}
		}

		return $status;
	}

	public function escudos(){
		$escudos = file_get_contents($this->path_base.'equipamentos/escudos.txt');
		$escudos = explode("\n", $escudos);

		foreach ($escudos as $key => $value) {
			$linha_de_caracteristicas[] = explode(' ', $value);
		}

		$tabela_de_escudos = [];

		foreach ($linha_de_caracteristicas as $key => $value) {
			foreach ($value as $key1 => $value1) {
				if ($key1 == 0) {
					$linha['nome'] = str_replace('_', ' ', $value1);		
				}

				if ($key1 == 1) {
					$linha['custo'] = str_replace('_', ' ', $value1);		
				}

				if ($key1 == 2) {
					$linha['bonus_na_ca'] = $value1;		
				}

				if ($key1 == 3) {
					$linha['bonus_maximo'] = $value1;		
				}

				if ($key1 == 4) {
					$linha['penalidade'] = $value1;		
				}

				if ($key1 == 5) {
					$linha['peso'] = $value1;		
				}
				
				$linha['bonus_magico'] = 0;		
			}

			$tabela_de_escudos[] = $linha;
		}

		$this->tabela_de_escudos = $tabela_de_escudos;
	}

	public function armadura(){
		$armaduras = file_get_contents($this->path_base.'equipamentos/armaduras.txt');
		$armaduras = explode("\n", $armaduras);

		foreach ($armaduras as $key => $value) {
			$linha_de_caracteristicas[] = explode(' ', $value);
		}

		$tabela_de_armaduras = [];

		foreach ($linha_de_caracteristicas as $key => $value) {
			foreach ($value as $key1 => $value1) {
				if ($key1 == 0) {
					$linha['nome'] = str_replace('_', ' ', $value1);		
				}

				if ($key1 == 1) {
					$linha['custo'] = str_replace('_', ' ', $value1);		
				}

				if ($key1 == 2) {
					$linha['bonus_na_ca'] = $value1;		
				}

				if ($key1 == 3) {
					$linha['bonus_maximo'] = $value1;		
				}

				if ($key1 == 4) {
					$linha['penalidade'] = $value1;		
				}

				if ($key1 == 5) {
					$linha['peso'] = $value1;		
				}
				
				$linha['bonus_magico'] = 0;		
			}

			$tabela_de_armaduras[] = $linha;
		}

		$this->tabela_de_armaduras = $tabela_de_armaduras;
	}

	public function arma(){		
		$conjurador = ['bardo','clerigo','druida','feiticeiro','mago','miko','onmyouji'];
		if (in_array($this->classe, $conjurador)) {
			$armas = explode("\n", file_get_contents($this->path_base.'equipamentos/armas_simples.txt'));
		} else {
			$armas_exoticas = explode("\n", file_get_contents($this->path_base.'equipamentos/armas_exoticas.txt'));
			$armas_simples = explode("\n", file_get_contents($this->path_base.'equipamentos/armas_simples.txt'));
			$armas = array_merge($armas_exoticas, $armas_simples);
		}

		foreach ($armas as $key => $value) {
			$linha_de_caracteristicas[] = explode(' ', $value);
		}

		$tabela_de_armas = [];

		foreach ($linha_de_caracteristicas as $key => $value) {
			foreach ($value as $key1 => $value1) {
				if ($key1 == 0) {
					$linha['nome'] = str_replace('_', ' ', $value1);		
				}

				if ($key1 == 1) {
					$linha['custo'] = str_replace('_', ' ', $value1);		
				}

				if ($key1 == 2) {
					$linha['dano'] = $value1;		
				}

				if ($key1 == 3) {
					$linha['critico'] = $value1;		
				}

				if ($key1 == 4) {
					$linha['distancia'] = $value1;		
				}

				if ($key1 == 6) {
					$linha['peso'] = $value1;		
				}

				if ($key1 == 7) {
					$linha['tipo'] = str_replace('_', ' ', $value1);		
				}
				
				$linha['bonus_magico'] = 0;		
			}

			$tabela_de_armas[] = $linha;
		}

		$this->tabela_de_armas = $tabela_de_armas;
	}

	public function armas_equipadas(){
		$this->arma();
		$armas = $this->tabela_de_armas;
		$escolhido = array_rand($armas, mt_rand(1,3));

		if (is_array($escolhido)) {
			foreach ($escolhido as $key => $value) {
				$armas_escolhidas[] = $armas[$value];
			}
		} else {
			$armas_escolhidas[] = $armas[$escolhido];
		}

		$this->armas_equipadas = $armas_escolhidas;
	}

	public function escudo_equipado(){
		$this->escudos();
		$escudo = $this->tabela_de_escudos;
		$tem_escudo = mt_rand(1,2);
		if ($tem_escudo == 1) {
			$escolhido = array_rand($escudo);
			$this->escudos_equipados = $escudo[$escolhido];
		} else {
			$this->escudos_equipados = null;
		}
	}

	public function armadura_equipada(){
		$this->armadura();
		$armadura = $this->tabela_de_armaduras;
		$item_armadura = mt_rand(1,2);
		if ($item_armadura == 1) {
			$escolhido = array_rand($armadura);
			$this->armaduras_equipadas = $armadura[$escolhido];
		} else {
			$this->armaduras_equipadas = null;
		}
	}
}