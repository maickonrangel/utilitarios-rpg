<?php

class Assentamentos_Model {

	public $file_path;

	function __construct(){
		$this->file_path = PATH_BASE.'config/locale/pt-br/txt/livro_do_mestre/assentamento/';
	}

	function armazem($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'armazem.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function calamidade_atual($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'calamidade_atual.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function conhecido_por($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'conhecido_por.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function construcao_religiosa($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'construcao-religiosa.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function loja($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'loja.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function posicao_do_governante($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'posicao_do_governante.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function primeiro_nome_taverna($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'primeiro_nome_taverna.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function segundo_nome_taverna($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'segundo_nome_taverna.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function relacoes_raciais($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'relacoes_raciais.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function residencia($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'residencia.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function taverna($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'taverna.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function tipo_de_construcao($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'tipo_de_construcao.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function tracos_notaveis($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'tracos_notaveis.txt', $qtd, 'FILE'))->getRaffleItens();
	}

	function encontros($qtd=1){
		return (new Raffleitemfile_Core($this->file_path.'encontros.txt', $qtd, 'FILE'))->getRaffleItens();
	}
}