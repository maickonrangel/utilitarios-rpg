<?php

require_once 'routes.php';

foreach ($_ROUTERS as $key => $value) {

	echo "
	<url>
    	<loc>https://utilitariosrpg.com.br/{$value['name']}</loc>
    	<lastmod>2018-06-24</lastmod>
    	<changefreq>monthly</changefreq>
    	<priority>1.0</priority>
  	</url>
  	<br><br>";
}