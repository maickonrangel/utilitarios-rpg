<?php
// ambiente
if (isset($_SERVER['HTTP_HOST']) && substr_count($_SERVER['HTTP_HOST'], '127.0.0.1')) {
	$_ENVIRONMENT 	= 'local';
	$_DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	$_SERVER_NAME 	= 'https://'.$_SERVER['SERVER_NAME'];

} elseif (isset($_SERVER['HTTP_HOST']) && substr_count($_SERVER['HTTP_HOST'], 'utilitariosrpg.com.br')) {
	$_ENVIRONMENT = 'production';
	$_DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'].'/';
	$_SERVER_NAME 	= "https://utilitariosrpg.com.br/";
} else {
	$_ENVIRONMENT 	= 'local';
	$_DOCUMENT_ROOT = 'C:/xampp/htdocs';
	$_SERVER_NAME 	= 'http://127.0.0.1/';
}

// Configuracao
$_CONFIG = [

	// aplicacao local
	'app' => [
		// local
		'local' => [
			'project_dir' 	=> $_DOCUMENT_ROOT,
			'file_path' 	=> '/utilitarios-rpg/',
			'url_path'		=> $_SERVER_NAME
			],
		
		// producao
		'production' => [
			'project_dir' 	=> $_DOCUMENT_ROOT,
			'file_path' 	=> '', //vazio
			'url_path'		=> $_SERVER_NAME
			],
	],
	
	// banco de dados
	'db' => [
		// local
		'local' => [
			'db_name' 		=> 'utilitarios_database',
			'db_host' 		=> '127.0.0.1',
			'db_user' 		=> 'root',
			'db_pass' 		=> '',
		],

		// producao
		'production' => [
			'db_name' 		=> 'utilitar_database',
			'db_host' 		=> 'localhost',
			'db_user' 		=> 'utilitar_maickon',
			'db_pass' 		=> 'p6sP5+FbS;o$',
		]
	],

	'lenguage' => [
		// local
		'local' => 'pt-br',
		// producao
		'production' => 'pt-br'
	]
];