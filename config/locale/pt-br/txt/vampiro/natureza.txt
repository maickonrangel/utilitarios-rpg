Excêntrico: Você existe apenas para o seu próprio prazer.
Arquiteto: Você constrói um futuro melhor.
Autocrata: Você precisa controlar.
Bon Vivant: A não-vida é para ser vivida.
Caçador de Emoções: A emoção é tudo o que importa.
Celebrante: Você existe para sua paixão.
Competidor: Você precisa ser o melhor.
Conformista: Você segue e assiste.
Criança: Quem estará lá para ajudá-lo?
Diretor: Você supervisiona o que tem que ser feito.
Durão: Aqueles que podem, vencem. Aqueles que não podem, perdem. Você pode.
Esperto: Os outros existem para o seu benefício.
Fanático: A causa é tudo o que importa.
Filantropo: Todos precisam ser nutridos.
Galante: Você não assiste o espetáculo, você é o próprio espetáculo!
Gozador: Os risos obscurecem a dor.
Juiz: A verdade está lá fora.
Mártir: Você sofre pelo bem maior.
Masoquista: Você testa seus limites todas as noites.
Monstro: Você é um Amaldiçoado, aja como um.
Pedagogo: Você salva os outros através do conhecimento.
Penitente: A não-vida é uma maldição com a qual se acostumar.
Perfeccionista: Nada está bom o suficiente.
Ranzinza: Nada vale a pena.
Rebelde: Você não segue a liderança de ninguém.
Sobrevivente: Nada pode te manter caído.
Solitário: Você traça o seu próprio caminho.
Tradicionalista: Como sempre tem sido, assim deve ser.
Valentão: Força é tudo que importa.
Visionário: Existe algo por trás de tudo isso.