Abençoado: Enquanto estiver de posse do item, o portador sente-se afortunado e otimista sobre o que o futuro trará. Borboletas e outras criaturas inofensivas poderiam aparecer na presença do item.
Confidente: O item ajuda seu portador a se sentir seguro de si.
Avarento: O usuário do item torna-se obcecado por bens materiais.
Quebradiço: O item racha, se despedaça, se parte ou quebra levemente quando o usuário o empunha, veste ou ativa. Esse sofisma não afeta as propriedades do item, mas se o item tiver sido muito usado, ele parecerá decrepito.
Faminto: As propriedades mágicas deste item funcionam apenas se sangue fresco de um humanoide tiver sido aplicado nele nas últimas 24 horas. Ele precisa de apenas uma gota para se ativar.
Barulhento: O item emite um barulho alto – como um som estridente, um grito ou o ressoar de um gongo – quando usado.
Metamórfico: O item periodicamente e aleatoriamente altera sua aparência de formas suaves. O portador não tem controle sobre essas pequenas alterações, que não tem quaisquer efeitos no uso do item.
Balbuciante: O item resmunga e balbucia. Uma criatura que ouça atentamente o item pode aprender algo útil.
Doloroso: O portador experimenta um rápido momento inofensivo de dor quando usa o item.
Possessivo: O item requer sintonização quando é empunhado ou vestido pela primeira vez e ele não permite que seu usuário se sintonize a outros itens. (Outros itens já sintonizados ao usuário continuam assim até que sua sintonização termine).
Repulsivo: O portador têm uma sensação de desastre quando está em contato com o item e continua a sentir desconforto enquanto o portar.
Preguiçoso: O portador deste item sente-se preguiçoso e letárgico. Enquanto estiver sintonizado com o item, o portador precisa de 10 horas para terminar um descanso longo.