Sobreviventes/Mutantes - Um andarilho contador de histórias
Sobreviventes/Mutantes - Um homem-rato trapaceiro
Sobreviventes/Mutantes - Um bárbaro/mercenário espirituoso
Sobreviventes/Mutantes - Um Sacerdote de um culto benigno
Sobreviventes/Mutantes - Um feiticeiro estranho com olho biônico
Sobreviventes/Mutantes - Um mecânico com quatro braços e antenas de barata
Um andarilho contador de histórias
Um homem-rato trapaceiro
Um bárbaro/mercenário espirituoso
Um Sacerdote de um culto benigno
Um feiticeiro estranho com olho biônico
Um mecânico com quatro braços e antenas de barata
Pessoas Poderosas - O líder de uma facção diferente
Pessoas Poderosas - O ancião local
Pessoas Poderosas - O eremita misterioso
Pessoas Poderosas - O bruxo androide
Pessoas Poderosas - O Rei dos homens-sapos
Pessoas Poderosas - O lorde dos androides
Organização - Tribo de mutantes andarilhos
Organização - Ordem religiosa/seita
Organização - Gangue de motoqueiros mutantes
Organização - Sociedade secreta da Era passada
Organização - Comunidade local
Organização - Grupo de androides
Monstros - Mutante deformado com boas intenções
Monstros - Alienígena curioso
Monstros - Raça ancestral tida como extinta
Monstros - Besta mutante afetuosa
Monstros - Animal inteligente com poderes psiônicos
Monstros - Morto-vivo cibernético
Entidades - Deus esquecido e renascido
Entidades - Seres de outra dimensão
Entidades - Inteligência artificial libertada
Entidades - Um Imortal com empatia para com os mortais
Entidades - Fantasmas da era passada
Entidades - Energia viva e senciente
Artefatos - Espada mágica de luz
Artefatos - Elmo que fala com seu usuário
Artefatos - Tecnologia alienígena
Artefatos - Formação de cristal inteligente
Artefatos - Armadura de tecnologia avançada e senciente
Artefatos - Uma orbe preateada com inteligência alienígena