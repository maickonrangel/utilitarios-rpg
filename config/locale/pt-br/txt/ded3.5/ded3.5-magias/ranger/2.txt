Agilidade do Gato: O alvo recebe +4 Des durante 1 min/nível.
Armadilha: Cria uma armadilha de laço mágica.
Crescer Espinhos: As criaturas na área sofrem 1d4 de dano, podem ficar lentas.
Curar Ferimentos Leves: Cura 1d8 +l/nível de dano (máx. +5).
Falar Com Plantas: Você pode conversar com plantas normais e criaturas plantas.
Imobilizar Animal: Paralisa um animal, 1 rodada/nível.
Invocar Aliado da Natureza II: Invoca animais para auxiliar o conjurador.
Muralha de Vento: Desvia disparos, criaturas pequenas e gases.
Pele de Árvore: Concede +2 (ou mais) de bônus de bônus de melhoria na armadura natural.
Proteção Contra Elementos: Absorve 12 de dano/nível de um tipo de energia.
Sabedoria da Coruja: O alvo ganha +4 Sab por 1 min/nível.
Vigor do Urso: O alvo ganha +4 Con por 1 min/nível.