Abrir/Fechar: Abre/fecha objetos pequenos ou leves.
Brilho: Ofusca uma criatura (-1 nas jogadas de ataque).
Canção de Ninar: O alvo fica sonolento (-1 nos testes de Observar e Ouvir e -2 contra sono).
Consertar: Faz pequenos reparos em um objeto.
Detectar Magia: Detecta magias é itens mágicos a menos de 18 m.
Globos de Luz: Cria tochas ou outras luzes ilusórias.
Intuir Direção: Você sabe onde fica o Norte.
Invocar Instrumento: Invoca um instrumento a escolha do conjurador.
Ler Magia: Decifra pergaminhos ou grimórios.
Luz: Um objeto brilha como uma tocha.
Mãos Mágicas: Telecinésia de 2,5 kg.
Mensagem: Conversação em voz baixa á distância.
Pasmar: Um humanóide (4 DV ou menos) perde sua próxima ação.
Prestidigitação: Realiza pequenas ilusões.
Resistência: O alvo recebe +1 para lestes de resistência.
Som Fantasma: Imita sons.