Alterar Forma: Transforma você em qualquer criatura, pode mudar de forma uma vez por rodada.
Antipatia: Objeto ou local afetado repele certas criaturas.
Curar Ferimentos Críticos em Massa: Cura 4d8+l/nível PV de diversas criaturas.
Grupo de elementais: Invoca vários dementais.
Homens Vegetais: Invoca 1d4+2 homens planta para auxiliarem o conjurador.
Invocar Aliado da Natureza IX: Invoca animais para auxiliar o conjurador.
Regeneração: Membros decepados do alvo crescem novamente, cura 4d8 de dano +l/nível (máx. 35).
Sexto Sentido: Sexto sentido lhe avisa sobre perigo iminente
Simpatia: Objeto ou local atrai cenas criaturas.
Tempestade da Vingança: Tempestade de ácido, granizo e pedras.