Círculo Mágico Contra o Caos/Mal/Bem/Ordem: Como as magias de proteção, mas com 3 m de raio e 10 min/nível.
Dificultar Detecção: Esconde alvo de adivinhações e observação.
Dissipar Magia: Cancela magias e efeitos mágicos.
Proteção Contra Elementos: Absorve 12 de dano/nível de um tipo de energia.
Runas Explosivas: Causam 6d6 de dano caso decifradas.
Clarividência/Clariaudiência: Ouve ou enxerga a distância durante 1 min/nível.
Idiomas: Fala qualquer idioma.
Visão Arcana: Auras mágicas se tornam visíveis para o conjurador.
Invocar Criaturas III: Invoca um ser extra-planar para auxiliar o conjurador.
Montaria Fantasmagórica: Cria cavalo mágico permanece durante 1 hora/nível.
Nevasca: Atrapalha a visão e o movimento.
Névoa Fétida: Vapores nauseantes, 1 rodada/nível.
Selo da Serpente Sépia: Cria símbolo no texto que imobilizará o leitor.
Fúria: Concede +2 For e Con, +1 em testes de resistência de Vontade e -2 CA.
Heroísmo: Concede +2 nas jogadas de ataque, testes de resistência e perícias.
Imobilizar Pessoa: Imobiliza uma pessoa durante 1 rodada/nível.
Sono Profundo: Coloca 10 DV de criaturas para dormir.
Sugestão: Força o alvo a seguir um curso de ação.
Bola de Fogo: 1d6 de dano por nível, 6 m de raio.
Luz do Dia: Ilumina 18 m de raio com uma luz brilhante.
Muralha de Vento: Desvia disparos, criaturas pequenas e gases.
Pequeno Refúgio de Leomund: Cria um abrigo para 10 criaturas.
Relâmpago: Eletricidade causa 1d6 de dano/nível.
Deslocamento: Os ataques têm 50% de chance de fracassar.
Escrita Ilusória: Somente o leitor designado pode entendê-la.
Esfera de Invisibilidade: Torna todos dentro de uma área de 3 m invisíveis.
Imagem Maior: Como imagem silenciosa, mas com som, cheiro e temperatura.
Descanso Tranqüilo: Preserva um corpo.
Imobilizar Mortos-Vivos: Imobiliza mortos-vivos durante 1 rodada/nível.
Raio de Exaustão: Raio torna o alvo exausto.
Toque Vampírico: Toque causa 1d6/2 níveis, o conjurador recebe o dano como PV.
Arma Mágica Maior: +1/4 níveis, máx. +5.
Encolher Item: Objeto encolhe para um 1/16 de seu tamanho.
Flecha de Chamas: Flechas causam +1d6 de dano de fogo.
Forma Gasosa: O alvo fica incorpóreo e pode voar lentamente.
Lâmina Afiada: Dobra a margem de ameaça normal da arma.
Lentidão: 1 alvo/nível pode realizar apenas 1 ação/rodada, -2 na CA, -2 nas jogadas de ataque.
Página Secreta: Altera uma página para esconder seu verdadeiro conteúdo.
Piscar: Você desaparece e reaparece aleatoriamente durante 1 rodada/nível.
Respirar na Água: Os alvos podem respirar sob a água.
Velocidade: 1 criatura/nível se move com mais rapidez, +1 no ataque, CA e testes de resistência de Reflexos.
Vôo: O alvo voa (deslocamento de 18 m).