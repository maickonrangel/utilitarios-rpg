Abençoar Água: Cria água benta.
Abençoar Arma: Uma arma ataca com precisão contra inimigos malignos.
Arma Mágica: Uma arma recebe +1 de bônus.
Auxilio Divino: Você recebe +1 de bônus/3 níveis para ataques e dano.
Benção: Aliados recebem +1 para ataques e testes contra medo.
Criar Água: Cria 8 litros/nível de água pura.
Cotar Ferimentos Leves: Cura 1d8 + l/nível de dano (máx. +5).
Detectar Mortos-Vivos: Revela mortos-vivos que estejam a menos de 18 m.
Detectar Venenos: Detecta veneno em uma criatura ou objeto pequeno.
Ler Magias: Decifra pergaminhos ou grimórios.
Proteção Contra o Caos/Mal: +2 na CA e testes de resistência, impede controle mental, isola dementais e seres planares.
Resistência: O alvo recebe +1 para testes de resistência.
Restauração Menor: Dissipa penalidades mágicas de habilidade ou recupera 1d4 de dano de habilidade.
Suportar Elementos: Mantém uma criatura confortável dentro de ambientes áridos.
Virtude: O alvo ganha 1 PV temporário.