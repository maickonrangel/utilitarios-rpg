1º +1 Espadas ancestrais +1
2º +2 Estilo de combate, grito de kiai
3º +3 Espadas ancestrais +2
4º +4 Técnica de luta
5º +5 Espadas ancestrais +3
6º +6 Estilo de combate aprimorado
7º +7 Espadas ancestrais +4, olhar assustador
8º +8 Técnica de luta
9º +9 Espadas ancestrais +5
10º +10 Grito de kiai aprimorado
11º +11 Domínio do estilo de combate, espadas ancestrais +6
12º +12 Técnica de luta
13º +13 Espadas ancestrais +7
14º +14 Olhar assustador em massa
15º +15 Espadas ancestrais +8
16º +16 Técnica de luta
17º +17 Espadas ancestrais +9
18º +18 Grito de kiai maior
19º +19 Espadas ancestrais +10
20º +20 Técnica de luta