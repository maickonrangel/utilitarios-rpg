Alarme: avisa quando alguém invadir uma área protegida.
Armadura arcana: você recebe +4 na CA.
Cerrar portas: tranca magicamente uma porta, portão ou janela.
Escudo arcano: você recebe +4 na CA e se torna imune a mísseis mágicos.
Proteção contra o mal/bem/caos/ordem: fornece +2 na CA e nos testes de resistência contra criaturas da tendência escolhida.
Suportar elementos: protege o alvo de temperaturas extremas.
Ataque certeiro: você recebe +20 na próxima jogada de ataque.
Compreender idiomas: você entende qualquer coisa escrita ou falada.
Detectar mortos-vivos: detecta a presença, quantidade e poder de mortos-vivos na área.
Detectar portas secretas: detecta portas e salas escondidas.
Identifi cação: determina habilidades de um item mágico.
Área escorregadia: criaturas na área podem cair no chão.
Chuva quente: produz água quente para se banhar, não para beber.
Névoa obscurecente: névoa obscurece toda a visão.
Toque chocante: causa 2d8 pontos de dano.
Enfeitiçar pessoa: alvo se torna prestativo em relação a você.
Gagueira de Raviollius: alvo não consegue falar corretamente, sofrendo –4 de testes baseados em Carisma.
Hipnotismo: deixa alvos imóveis e melhora a atitude deles em relação a você.
Sono: criaturas na área totalizando 2d4 níveis adormecem.
Disco flutuante: disco de energia flutua e carrega até 100kg de carga.
Mísseis mágicos: dispara duas esferas de energia que causa, cada uma, 1d4+1 pontos de dano.
Mãos flamejantes: cone causa 2d6 pontos de dano.
Aura mágica: faz um objeto mundano parecer mágico ou vice-versa.
Disfarce ilusório: você recebe +10 em testes de Enganação para disfarce.
Imagem silenciosa: cria uma ilusão visual, como uma miragem.
Leque cromático: criaturas na área fi cam inconscientes, atordoadas ou cegas, de acordo com seu nível.
Ventriloquismo: projeta sua voz de outro lugar.
Invocar monstro I: invoca um monstro Pequeno sob seu comando.
Montaria arcana: invoca um cavalo ou pônei para servir como montaria (mas não para lutar).
Servo invisível: criatura invisível realiza tarefas para você.
Causar medo: criatura de até 4º nível fi ca apavorada.
Raio do enfraquecimento: impõe –2 nas jogadas de ataque e dano.
Toque macabro: alvo sofre 1d6 pontos de dano e –1 nas jogadas.
Animar corda: anima uma corda, que pode se enrolar, fazer um laço ou dar um nó.
Apagar: apaga texto mundano ou mágico.
Arma mágica: arma recebe +1 nos ataques e danos.
Aumentar pessoa: um humanoide aumenta uma categoria de tamanho.
Queda suave: alvo cai lentamente.
Recuo acelerado: seu deslocamento aumenta em 9m.
Reduzir pessoa: diminui a altura do alvo pela metade.
Salto: alvo recebe +30 nos testes de Atletismo para saltar.