Campo antimagia: barreira com 3m de raio anula todos efeitos mágicos.
Dissipar magia maior: como dissipar magia, mas com maior limite no teste de dissipar.
Globo de invulnerabilidade: como globo de invulnerabilidade menor, mas detém qualquer magia de até 4º nível.
Proteger fortalezas: protege uma estrutura com diversos efeitos.
Repulsão: campo de força impede criaturas de se aproximarem de você.
Névoa ácida: como névoa sólida, mas causa dano às criaturas em seu interior.
Analisar encantamento: analisa uma criatura ou objeto e descobre as magias lhe afetando.
Lendas e histórias: descobre informações sobre o alvo.
Visão da verdade: revela a forma real das coisas.
Âncora planar: invoca um espírito de até 12º nível.
Invocar monstro VI: invoca um monstro Grande sob seu comando.
Heroísmo maior: alvo recebe +4 nos ataques e testes de resistência, 10 PV temporários e fi ca imune a medo.
Missão: como missão menor, mas pode afetar uma criatura de qualquer nível.
Símbolo da persuasão: como símbolo da dor, mas as criaturas ficam enfeitiçadas.
Sugestão em massa: como sugestão, mas afeta até cinco criaturas.
Corrente de relâmpagos: relâmpago causa 12d6 pontos de dano em um alvo, e 6d6 pontos de dano em até 10 outros alvos.
Contingência: mantenha uma magia pronta para ser ativada em condição pré-determinada.
Mão vigorosa de Talude: como mão interposta, mas também afasta o oponente.
Esfera gélida: explosão com 3m de raio causa 12d6 pontos de dano e congela líquidos.
Andar nas sombras: alvos entram no Plano das Sombras, onde viajam mais rápido.
Despistar: você fi ca invisível e uma cópia ilusória surge em seu lugar.
Imagem permanente: como imagem silenciosa, mas inclui ilusões sonoras, olfativas e térmicas e é permanente.
Imagem programada: como imagem silenciosa, mas inclui ilusões sonoras, olfativas e térmicas e é ativada por uma condição específi ca.
Mundo dos sonhos: alvos adormecem e vivem uma realidade controlada por você.
Véu: muda a aparência de até cinco criaturas.
Necro Ataque visual: seu olhar apavora criaturas.
Círculo da morte: mata criaturas totalizando 10d4 níveis.
Criar mortos-vivos: como criar mortos-vivos menor, mas pode criar um carniçal, lívido ou múmia.
Destruir mortos-vivos: destrói mortos-vivos criaturas totalizando 10d4 níveis.
Símbolo do medo: como símbolo da dor, mas as criaturas ficam apavoradas.
Agilidade do gato em massa: como agilidade do gato, mas afeta até cinco alvos.
Animar objetos: concede vida a objetos inanimados.
Astúcia da raposa em massa: como astúcia da raposa, mas afeta até cinco alvos.
Carne para pedra: criatura vira uma estátua.
Controlar água: reduz ou aumenta o nível de água.
Controlar o clima: muda o clima da área.
Desintegrar: causa 20d6 pontos de dano.
Elucubração do mago: recupera uma magia de até 5º nível que você tenha lançado recentemente.
Esplendor da águia em massa: como esplendor da águia, mas afeta até cinco alvos.
Força do touro em massa: como força do touro, mas afeta até cinco alvos.
Pedra em carne: restaura uma criatura petrifi cada ao estado normal.
Sabedoria da coruja em massa: como sabedoria da coruja, mas afeta até cinco criaturas.
Transformação de guerra: você recebe habilidades de combate, mas não pode lançar magias.
Vigor do urso em massa: como vigor do urso, mas afeta até cinco criaturas.