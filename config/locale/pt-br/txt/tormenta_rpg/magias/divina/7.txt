Animar plantas: concede movimento a plantas.
Blasfêmia: criaturas não Malignas na área sofrem efeitos variados.
Cajado vivo: transforma um cajado em um ente.
Controlar o clima: muda o clima da área.
Curar ferimentos graves em massa: recupera 3d8+13 pontos de vida de até cinco criaturas.
Destruição: alvo tocado é consumido em chamas.
Destruição rastejante: invoca uma massa negra de insetos que causa 6d6 pontos de dano por rodada.
Ditado: criaturas não Leais na área sofrem efeitos variados.
Infligir ferimentos graves em massa: causa 3d8+13 pontos de dano em até cinco criaturas.
Invocar monstro VII: invoca um monstro Enorme sob seu comando.
Metal em madeira: todos os objetos de metal na área se transformam em madeira.
Palavra do caos: criaturas não Caóticas na área sofrem efeitos variados.
Palavra sagrada: criaturas não Bondosas na área sofrem efeitos variados.
Passeio etéreo: você é transportado para o Plano Etéreo.
Raio de sol: raios causam 4d6 pontos de dano e cegueira.
Refúgio: objeto preparado transporta criatura lhe segurando para um lugar determinado.
Regeneração: cura 4d8+13 pontos de dano e recupera partes arrancadas, ossos quebrados, etc.
Repulsão: campo de força impede criaturas de se aproximarem de você.
Ressurreição: como reviver os mortos, mas só precisa de uma parte do corpo da criatura.
Restauração maior: como restauração, mas pode recuperar um nível perdido há até um ano.
Símbolo do atordoamento: como símbolo da dor, mas as criaturas na área fi cam atordoadas.
Símbolo da fraqueza: como símbolo da dor, mas as criaturas na área sofrem 3d6 de dano de Força.
Vidência maior: como vidência, mas dura mais e permite lançar magias.