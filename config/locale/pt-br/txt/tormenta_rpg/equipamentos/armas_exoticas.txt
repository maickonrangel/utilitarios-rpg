Nunchaku 2_TO 1d6 x3 — 1kg Esmagamento
Sai 5_TO 1d6 x2 3m 0,5kg Esmagamento
Wakizashi 350_TO 1d8 19-20 — 1kg Corte
Chicote 1_TO 1d3 x2 — 1kg Corte
Espada bastarda 35_TO 1d10 19-20 — 3kg Corte
Katana 400_TO 1d10 19-20 — 3kg Corte
Machado anão 30_TO 1d10 x3 — 4kg Corte
Sabre_serrilhado 500_TO 1d8 19-20/especial — 2kg Corte
Corrente_com_cravos 25_TO 2d4 x2 — 5kg Perfuração
Espada_de_duas_lâminas 100_TO 1d8/1d8 19-20 — 5kg Corte
Espada_táurica 50_TO 2d8 x2 — 7,5kg Corte
Marreta_estilhaçadora 50_TO 1d12 19-20/especial — 7kg Esmagamento
Mosquete 500_TO 2d8 19-20/x3 45m 5kg Perfuração
Pistola 250_TO 2d6 19-20/x3 15m 1,5kg Perfuração
Rede 20_TO — — 3m 3kg —
Shuriken 1_TO 1d4 x2 3m 250g Perfuração
Tai-tai 40_TO 2d4 x2 24m 2kg Esmagamento