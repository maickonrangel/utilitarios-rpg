Em uma área montanhosa
Em um pântano
Em uma floresta
Em um vilarejo
Em um grande centro urbano
Em um monastério ou templo clerical
Durante uma viagem
Em uma ilha
Você não sabe nada sobre seu nascimento
Em uma caverna
Em um cemitério
Em uma cidade de porte médio
Na capital do reino
No palácio real
Em um deserto
Em um casebre no meio do nada
Em um navio
Em uma cidade portuária
Em uma fortaleza anã
uma aldeia élfica
Em um reino distante
Em uma fazenda
Em uma comunidade halfling
Em um acampamento de salteadores
Em um circo
Em um campo de batalha
No subterrâneo
Em uma cela de prisão
Em um acampamento cigano
Em um local completamente destruído