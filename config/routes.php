<?php
/*
	routes.php
	Crie url personalizadas atraves da configuracao abaixo

	name: Nome da rota
	controller: Nome do controler a ser usado
	Action: Nome do metodo a ser chamado pelo controller
*/

$_ROUTERS = [

	['name'=>'criar-uma-conta', 'controller'=>'login', 'action'=>'criar_conta'],
	['name'=>'sair', 'controller'=>'login', 'action'=>'sair'],

	['name'=>'output', 'controller'=>'cardrpg', 'action'=>'output'],

	['name'=>'home', 'controller'=>'init', 'action'=>'index'],
	['name'=>'salvar_ficha', 'controller'=>'formulariofichas', 'action'=>'salvar'],
	['name'=>'atualizar_ficha', 'controller'=>'formulariofichas', 'action'=>'atualizar'],
	['name'=>'deletar_ficha', 'controller'=>'formulariofichas', 'action'=>'deletar'],
	['name'=>'cadastrar-arquivos', 'controller'=>'dashboard', 'action'=>'cadastrar_arquivos'],
	['name'=>'home-cadastro', 'controller'=>'dashboard', 'action'=>'home_cadastro'],
	
	['name'=>'Gerador-de-Cartas-para-RPG', 'controller'=>'cardrpg', 'action'=>'index'],
	['name'=>'Monstros-para-Tormenta-RPG', 'controller'=>'fichasmonstrostormenta', 'action'=>'index'],
	['name'=>'Salvar-fichas-de-RPG-online', 'controller'=>'formulariofichas', 'action'=>'index'],
	['name'=>'Rolar-dados-para-RPG', 'controller'=>'dados', 'action'=>'index'],
	['name'=>'Gerador-de-Doencas-para-NPC-ou-PJ', 'controller'=>'doencas', 'action'=>'index'],
	['name'=>'Gerador-de-Missoes-Para-Mercenarios', 'controller'=>'mercenarios', 'action'=>'index'],
	['name'=>'Gerador-de-Fichas-Dungeons-and-Dragons-3.5', 'controller'=>'fichasded35', 'action'=>'index'],
	['name'=>'Gerador-de-Fichas-para-Tormenta-RPG', 'controller'=>'fichastormenta', 'action'=>'index'],
	['name'=>'Gerador-de-Fichas-para-Vampiro', 'controller'=>'fichasvampiro', 'action'=>'index'],
	['name'=>'Gerador-de-tavernas-para-RPG', 'controller'=>'tavernas', 'action'=>'index'],
	['name'=>'Gerador-de-itens-para-Tormenta-RPG', 'controller'=>'tormentarpgitens', 'action'=>'index'],
	['name'=>'Gerador-de-encontros-aleatorios-para-RPG', 'controller'=>'encontros', 'action'=>'index'],
	['name'=>'Gerador-de-descricao-de-personagens-para-RPG', 'controller'=>'personalidades', 'action'=>'index'],
	['name'=>'Frases-epicas-para-RPG', 'controller'=>'frases', 'action'=>'index'],
	['name'=>'Gerador-de-Deturpacoes-magicas-para-RPG', 'controller'=>'deturpacoesmagicas', 'action'=>'index'],
	['name'=>'Gerador-de-Corrupcoes-para-RPG', 'controller'=>'corrupcao', 'action'=>'index'],
	['name'=>'Gerador-de-sorte-para-RPG', 'controller'=>'sorte', 'action'=>'index'],
	['name'=>'Gerador-de-nome-de-espadas-para-RPG', 'controller'=>'nomedeespadas', 'action'=>'index'],
	['name'=>'Gerador-de-nomes-para-RPG', 'controller'=>'nomes', 'action'=>'index'],
	['name'=>'Gerador-de-aventuras-para-RPG', 'controller'=>'aventuras', 'action'=>'index'],
	['name'=>'Gerador-de-masmorras-para-RPG', 'controller'=>'masmorras', 'action'=>'index'],
	['name'=>'Gerador-de-cidades-para-RPG', 'controller'=>'cidades', 'action'=>'index'],
	['name'=>'Gerador-de-itens-para-RPG', 'controller'=>'itens', 'action'=>'index'],
	['name'=>'Gerador-de-tempo-climatico-para-RPG', 'controller'=>'tempo', 'action'=>'index'],
	['name'=>'Geradores-do-Livro-do-mestre-de-Dungeons-and-Dragons-5e', 'controller'=>'ferramentasded5e', 'action'=>'index'],
	['name'=>'Geradores-assentamentos-para-Dungeons-and-Dragons-5e', 'controller'=>'assentamentos', 'action'=>'index'],
	['name'=>'Gerador-de-tesouros-para-Dungeons-and-Dragons-5e', 'controller'=>'tesouros', 'action'=>'index'],
	['name'=>'Gerador-de-PdMs-para-Dungeons-and-Dragons-5e', 'controller'=>'pdms', 'action'=>'index'],
	['name'=>'Gerador-de-encontros-de-monstros-por-ambiente-para-Dungeons-and-Dragons-5e', 'controller'=>'monstros', 'action'=>'index'],
	['name'=>'Cards-para-RPG', 'controller'=>'cardrpg', 'action'=>'index'],
	['name'=>'Fluxovida-Old-Dragon', 'controller'=>'fluxovida', 'action'=>'index'],

	['name'=>'Utilitarios-de-terceiros', 'controller'=>'init', 'action'=>'utilitariosterceiros'],
	// rotas para outros sites
	['name'=>'Gerador-de-cidades-medievais-com-imagem', 'controller'=>'init', 'action'=>'redirect_site', 'params'=>'Gerador-de-cidades-medievais-com-imagem'],
	['name'=>'Gerador-de-cidades-medievais-em-3d', 'controller'=>'init', 'action'=>'redirect_site', 'params'=>'Gerador-de-cidades-medievais-em-3d'],
	['name'=>'Gerador-de-mapas-mundo-medieval', 'controller'=>'init', 'action'=>'redirect_site', 'params'=>'Gerador-de-mapas-mundo-medieval'],
	['name'=>'Criar-mapas-para-tavernas-e-masmorras', 'controller'=>'init', 'action'=>'redirect_site', 'params'=>'Criar-mapas-para-tavernas-e-masmorras'],
	['name'=>'Visualizador-de-cena-para-RPG', 'controller'=>'init', 'action'=>'redirect_site', 'params'=>'Visualizador-de-cena-para-RPG'],
	['name'=>'Gerador-de-mapas-para-masmorra', 'controller'=>'init', 'action'=>'redirect_site', 'params'=>'Gerador-de-mapas-para-masmorra'],
	['name'=>'Gerador-de-mapas-para-cavernas', 'controller'=>'init', 'action'=>'redirect_site', 'params'=>'Gerador-de-mapas-para-cavernas'],
	['name'=>'Gerador-de-mapas-para-regioes-selvagens', 'controller'=>'init', 'action'=>'redirect_site', 'params'=>'Gerador-de-mapas-para-regioes-selvagens'],
	['name'=>'Galeria-de-mapas', 'controller'=>'init', 'action'=>'redirect_site', 'params'=>'Galeria-de-mapas'],
	['name'=>'Gerador-de-masmorras-do-donjon', 'controller'=>'init', 'action'=>'redirect_site', 'params'=>'Gerador-de-masmorras-do-donjon'],
	['name'=>'Gerador-de-aventuras-usando-cartas-de-magic', 'controller'=>'init', 'action'=>'redirect_site', 'params'=>'Gerador-de-aventuras-usando-cartas-de-magic'],
	['name'=>'Criar-fichas-para-o-um-anel-RPG', 'controller'=>'init', 'action'=>'redirect_site', 'params'=>'Criar-fichas-para-o-um-anel-RPG'],
	['name'=>'Gerador-de-mapas-do-daves-mapper', 'controller'=>'init', 'action'=>'redirect_site', 'params'=>'Gerador-de-mapas-do-daves-mapper'],
	['name'=>'Catalogo-de-mapas-do-paratime', 'controller'=>'init', 'action'=>'redirect_site', 'params'=>'Catalogo-de-mapas-do-paratime'],
	['name'=>'Gerador-de-mapas-de-cidade-inkwellideas', 'controller'=>'init', 'action'=>'redirect_site', 'params'=>'Gerador-de-mapas-de-cidade-inkwellideas'],
	['name'=>'Gerador-de-mapas-de-vilarejo-inkwellideas', 'controller'=>'init', 'action'=>'redirect_site', 'params'=>'Gerador-de-mapas-de-vilarejo-inkwellideas'],
	['name'=>'Gerador-de-mapas-de-masmorras-inkwellideas', 'controller'=>'init', 'action'=>'redirect_site', 'params'=>'Gerador-de-mapas-de-masmorras-inkwellideas'],
	['name'=>'Gerador-de-mapas-de-estalagem-inkwellideas', 'controller'=>'init', 'action'=>'redirect_site', 'params'=>'Gerador-de-mapas-de-estalagem-inkwellideas'],
	['name'=>'Planilha-de-personagem-para-Dungeons-and-Dragons-3.5', 'controller'=>'init', 'action'=>'redirect_site', 'params'=>'Planilha-de-personagem-para-Dungeons-and-Dragons-3.5'],
	['name'=>'Gerador-de-fichas-para-Dungeons-and-Dragons-5e', 'controller'=>'init', 'action'=>'redirect_site', 'params'=>'Gerador-de-fichas-para-Dungeons-and-Dragons-5e'],
	['name'=>'Ferramenta-de-audio-para-jogos-de-RPG', 'controller'=>'init', 'action'=>'redirect_site', 'params'=>'Ferramenta-de-audio-para-jogos-de-RPG'],
	['name'=>'Editor-de-mapas-2D-para-RPG', 'controller'=>'init', 'action'=>'redirect_site', 'params'=>'Editor-de-mapas-2D-para-RPG'],
	['name'=>'Gerador-de-mapa-poligonal', 'controller'=>'init', 'action'=>'redirect_site', 'params'=>'Gerador-de-mapa-poligonal'],
	['name'=>'Ficha-epica-web', 'controller'=>'init', 'action'=>'redirect_site', 'params'=>'Ficha-epica-web'],
	['name'=>'Ficha-epica-mobile', 'controller'=>'init', 'action'=>'redirect_site', 'params'=>'Ficha-epica-mobile']
];