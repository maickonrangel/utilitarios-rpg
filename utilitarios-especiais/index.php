<?php require_once 'define-urls.php'; ?>
<html>
<head>
	<title>Utilitários Especiais</title>
	<link href='http://www.helprpg.com.br/favicon.ico' rel='icon' type='image/x-icon'/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="description" content="Utilitários especias para RPG" />
	<meta name="keywords" content="utilitários especiais, ferramentas para RPG, Help RPG" />
	<meta name="author" content="<?php echo 'Maickon Rangel'; ?>" />
	
	<meta property="og:title" content="Utilitários Especiais">
	<meta property="og:site_name" content="Help RPG">
	<meta property="og:locale" content="pt_BR">
	<meta property="og:url" content="<?php echo "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>">
	<meta property="og:description" content="Utilitários especias para RPG">
	<meta property="og:type" content="website">
	<meta property="og:image" content="<?php echo URL_BASE.'img/logo-principal.jpg' ?>">
	<meta property="og:image:type" content="image/jpeg">
	<meta property="og:image:width" content="500">
	<meta property="og:image:height" content="340">
</head>
<body>
	<style type="text/css">
		body{
			font:normal normal 16px Arial, Tahoma, Helvetica, FreeSans, sans-serif;
			background: #e1eaf3 none repeat scroll top left;
			margin: 0px;
		}
		#utilitarios-box{
			background-color: #FFF;
			width: 80%;
			margin: 0 auto;
			height: 84%;
		}
		#header{
			width: 100%;
			height: 100px;
			background-color: #000;
			color: FFF;
			margin: 0px;
		}
		.texto-margem{
			width: 80%;
			margin: 0 auto;
			padding: 15px;
			padding-top: 30px;
		}
		.menu-item{
			font-size: 24px; 
			width: auto; 
			float:left;
			margin-right: 5px;
			padding: 10px;
			cursor: pointer;
		}
		.menu-item:hover{
			background-color: #cb3a3c;
		}

		img{
			background-size: 100% 100%;
			width: 100px;
			height: 100px;
		}

		.utilitario{
			width: 100px;
			height: 120px;
			float: left;
			padding: 4px;
			margin: 10px;
			border: 1px solid #000;
		}
		.utilitario:hover{
			background-color: #cb3a3c;
		}
		.nome{
			width: 100px;
			float: left;
			font-size: 11px;
			margin-top: 2px;
			text-align: center;
		}
		a{
			text-decoration: none;
			color: #000;
		}
		.menu-item a{
			color: #FFF;
		}
		#utilitarios-center{
			width: 96%;
			margin: 0 auto;
		}
		#msg{
			padding: 10px;
			width: 98%;
		}
	</style>
	
	<div id="header">
		<div class="texto-margem">
			<div class="menu-item">
				<a href="<?php echo URL_BASE_ORIGINAL; ?>">
					Utilitários Padrão
				</a>
			</div>
			<div class="menu-item">
				<a href="<?php echo URL_BASE_ORIGINAL.'dashboard' ?>">
					Dashboard
				</a>
			</div>
		</div>
	</div>
	<?php require_once 'menu.php'; ?>
	<div id="utilitarios-box">
		<div id="utilitarios-center">
			<div id="msg">
				<h3>Sobre os utilitários especiais</h3>
				<p style="text-align:justify;">Estes utilitários são ferramentas que acabei desenvolvendo em situações especiais para o meu uso pessoal e para que não fique esquecido no meu PC eu decidi adicionar aqui para os usuários patrocinadores. Sempre que eu desenvolver alguma coisa nova que normalmente eu não compartilharia, eu passarei a deixar aqui. Quando algo novo chegar os usuários serão avisados no painel dashboard.</p>
			</div>
			<?php foreach ($_MENU as $item) { ?>
				<a href="<?php echo $item['pasta']; ?>" target="blank">
					<div class="utilitario">
						<img src="img/util/<?php echo $item['img']; ?>">
						<span class="nome"><?php echo $item['nome']; ?></span>
					</div>
				</a>
			<?php } ?>
		</div>
	</div>
</body>
</html>