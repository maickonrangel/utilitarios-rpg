<?php require_once '../check-session.php'; ?>
<html>
<head>
	<title>Loja de Armas D&D</title>
	<link href='http://www.helprpg.com.br/favicon.ico' rel='icon' type='image/x-icon'/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="description" content="Utilitários especias para RPG" />
	<meta name="keywords" content="utilitários especiais, loja de armas D&D, D&D, armas, utilitários, Help RPG" />
	<meta name="author" content="<?php echo 'Maickon Rangel'; ?>" />
	
	<meta property="og:title" content="Utilitários Especiais">
	<meta property="og:site_name" content="Help RPG">
	<meta property="og:locale" content="pt_BR">
	<meta property="og:url" content="<?php echo "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>">
	<meta property="og:description" content="Utilitários especias para RPG">
	<meta property="og:type" content="website">
	<meta property="og:image" content="<?php echo URL_BASE.'img/logo-principal.jpg' ?>">
	<meta property="og:image:type" content="image/jpeg">
	<meta property="og:image:width" content="500">
	<meta property="og:image:height" content="340">
</head>
<style type="text/css">
	body{
		background-image: url('img/loja-1.jpg');
		background-size: 100% 100%;
		background-repeat: no-repeat;
		font-family: 'impact';
		color: #FFF;
	}

	.title{
		padding-top: 5px;
		margin-left: 15px;
		color: #FFF;
		font-size: 45;
		font-family: 'impact';
	}

	.iventario{
		color: #FFF;
		font-size: 18;
		font-family: 'impact';
		width: 28%;
		height: 150px;
		border: 4px solid #FFF;
		float: left;
		padding: 15px;
		margin: 15px;
		background-color: #000;
		font-weight: 100;
	}

	.iventario:hover{
		background-color: #FE2E2E;
		opacity: 0.8;
	}
	.btn-itens{
		padding: 15px;
		margin-left: 15px;
		margin-right: 15px;
		margin-bottom: 20px;
		border: 2px solid #FFF;
		text-decoration: none;
		color: #FFF;
		font-family: 'impact';
	}
	.btn-itens:hover{
		color: red;
	}
	.nome{
		color: silver;
		border-bottom: 1px solid silver;
	}
</style>
<body>
<?php require 'script.php'; ?>
<span class="title">Loja de Armas Especiais</span>
<a href="index.php" class="btn-itens">Ver Itens</a>
<br>
<br>
<br>
<?php 

	$armas1 = array_rand($armas_a_venda1, mt_rand(1,2)); 
	if (!is_array($armas1)) {
			echo '<div class="iventario">';
			echo '<span class="nome">'.($armas_a_venda1[$armas1]['nome']).'</span><br>';
			echo 'Tipo: Arma de uma mão<br>';
			echo 'Poder: '.($armas_a_venda1[$armas1]['poder']).'<br>';
			echo 'Dano: '.($armas_a_venda1[$armas1]['dano']).'+'.($armas_a_venda1[$armas1]['variante']).'<br>';
			echo 'Mágica: '.($armas_a_venda1[$armas1]['magica']).'<br>';
			echo 'Preço: '.number_format(($armas_a_venda1[$armas1]['preco'])).' PO<br>';
			echo '</div>';
	} else {
		foreach ($armas1 as $key => $value) {
			echo '<div class="iventario">';
			echo '<span class="nome">'.($armas_a_venda1[$value]['nome']).'</span><br>';
			echo 'Tipo: Arma de uma mão<br>';
			echo 'Poder: '.($armas_a_venda1[$value]['poder']).'<br>';
			echo 'Dano: '.($armas_a_venda1[$value]['dano']).'+'.($armas_a_venda1[$value]['variante']).'<br>';
			echo 'Mágica: '.($armas_a_venda1[$value]['magica']).'<br>';
			echo 'Preço: '.number_format(($armas_a_venda1[$value]['preco'])).' PO<br>';
			echo '</div>';
		}
	}

	$armas2 = array_rand($armas_a_venda2, mt_rand(1,2)); 
	if (!is_array($armas2)) {
			echo '<div class="iventario">';
			echo '<span class="nome">'.($armas_a_venda2[$armas2]['nome']).'</span><br>';
			echo 'Tipo: Arma de duas mãos<br>';
			echo 'Poder: '.($armas_a_venda2[$armas2]['poder']).'<br>';
			echo 'Dano: '.($armas_a_venda2[$armas2]['dano']).'+'.($armas_a_venda2[$armas2]['variante']).'<br>';
			echo 'Mágica: '.($armas_a_venda2[$armas2]['magica']).'<br>';
			echo 'Preço: '.number_format(($armas_a_venda2[$armas2]['preco'])).' PO<br>';
			echo '</div>';
	} else {
		foreach ($armas2 as $key => $value) {
			echo '<div class="iventario">';
			echo '<span class="nome">'.($armas_a_venda2[$value]['nome']).'</span><br>';
			echo 'Tipo: Arma de duas mãos<br>';
			echo 'Poder: '.($armas_a_venda2[$value]['poder']).'<br>';
			echo 'Dano: '.($armas_a_venda2[$value]['dano']).'+'.($armas_a_venda2[$value]['variante']).'<br>';
			echo 'Mágica: '.($armas_a_venda2[$value]['magica']).'<br>';
			echo 'Preço: '.number_format(($armas_a_venda2[$value]['preco'])).' PO<br>';
			echo '</div>';
		}
	}

	$armaduras1 = array_rand($armaduras_a_venda, mt_rand(1,2));
	if (!is_array($armaduras1)) {
			echo '<div class="iventario">';
			echo '<span class="nome">'.($armaduras_a_venda[$armaduras1]['nome']).'</span><br>';
			echo 'Tipo: '.($armaduras_a_venda[$armaduras1]['tipo']).'<br>';
			echo 'Poder: '.($armaduras_a_venda[$armaduras1]['poder']).'<br>';
			echo 'Bônus: '.($armaduras_a_venda[$armaduras1]['bonus']).'<br>';
			echo 'Redução de dano: '.($armaduras_a_venda[$armaduras1]['rd']).'<br>';
			echo 'Mágica: '.($armaduras_a_venda[$armaduras1]['magica']).'<br>';
			echo 'Preço: '.number_format(($armaduras_a_venda[$armaduras1]['preco'])).' PO<br>';
			echo '</div>';
	} else {
		foreach ($armaduras1 as $key => $value) {
			echo '<div class="iventario">';
			echo '<span class="nome">'.($armaduras_a_venda[$value]['nome']).'</span><br>';
			echo 'Tipo: '.($armaduras_a_venda[$value]['tipo']).'<br>';
			echo 'Poder: '.($armaduras_a_venda[$value]['poder']).'<br>';
			echo 'Bônus: '.($armaduras_a_venda[$value]['bonus']).'<br>';
			echo 'Redução de dano: '.($armaduras_a_venda[$value]['rd']).'<br>';
			echo 'Mágica: '.($armaduras_a_venda[$value]['magica']).'<br>';
			echo 'Preço: '.number_format(($armaduras_a_venda[$value]['preco'])).' PO<br>';
			echo '</div>';
		}
	}

	$escudos1 = array_rand($escudos_a_venda, mt_rand(1,2));
	if (!is_array($escudos1)) {
			echo '<div class="iventario">';
			echo '<span class="nome">'.($escudos_a_venda[$escudos1]['nome']).'</span><br>';
			echo 'Tipo: '.($escudos_a_venda[$escudos1]['tipo']).'<br>';
			echo 'Poder: '.($escudos_a_venda[$escudos1]['poder']).'<br>';
			echo 'Bônus: '.($escudos_a_venda[$escudos1]['bonus']).'<br>';
			echo 'Redução de dano: '.($escudos_a_venda[$escudos1]['rd']).'<br>';
			echo 'Mágica: '.($escudos_a_venda[$escudos1]['magica']).'<br>';
			echo 'Preço: '.number_format(($escudos_a_venda[$escudos1]['preco'])).' PO<br>';
			echo '</div>';
	} else {
		foreach ($escudos1 as $key => $value) {
			echo '<div class="iventario">';
			echo '<span class="nome">'.($escudos_a_venda[$value]['nome']).'</span><br>';
			echo 'Tipo: '.($escudos_a_venda[$value]['tipo']).'<br>';
			echo 'Poder: '.($escudos_a_venda[$value]['poder']).'<br>';
			echo 'Bônus: '.($escudos_a_venda[$value]['bonus']).'<br>';
			echo 'Redução de dano: '.($escudos_a_venda[$value]['rd']).'<br>';
			echo 'Mágica: '.($escudos_a_venda[$value]['magica']).'<br>';
			echo 'Preço: '.number_format(($escudos_a_venda[$value]['preco'])).' PO<br>';
			echo '</div>';
		}
	}
?>
</pre>
</body>
</html>