<?php 

$armas_duas_maos = simplexml_load_file('itens/arma_de_duas_maos.xml');
$armas_uma_mao = simplexml_load_file('itens/arma_de_uma_mao.xml');
$armaduras = simplexml_load_file('itens/armaduras.xml');
$escudos = simplexml_load_file('itens/escudos.xml');

$tipo_de_dano = ['d4','d6','d8','2d4','d10','d12','2d6','2d8'];
$tipo_de_bonus_leve = [1,1,2,2,3,3,4,4,5,6];
$tipo_de_bonus_medio = [1,1,2,2,3,4,5,6,7,7];
$tipo_de_bonus_pesado = [6,6,6,7,7,7,8,8,9,10];

$tipo_de_bonus_leve_escudo = [1,1,1,1,2,2,2,3,3,4];
$tipo_de_bonus_medio_escudo = [1,1,2,2,2,3,3,3,4,5];
$tipo_de_bonus_pesado_escudo = [1,2,2,3,3,4,4,5,6,7];

$variante = [mt_rand(0,1),mt_rand(1,2),mt_rand(2,3),mt_rand(3,4),mt_rand(4,5)];
$magica = [0,1,2,3,4,5];
$tipo_armadura = ['Leve','Média','Pesada'];
$tipo_escudo = ['Leve','Pesado','De corpo'];
$tipo_e_bonus = ['Leve'=>$tipo_de_bonus_leve, 'Média'=>$tipo_de_bonus_medio, 'Pesada'=>$tipo_de_bonus_pesado];
$tipo_e_bonus_escudo = ['Leve'=>$tipo_de_bonus_leve_escudo, 'Pesado'=>$tipo_de_bonus_medio_escudo, 'De corpo'=>$tipo_de_bonus_pesado_escudo];

function ajustar_bonus($equipamento_defensivo, $tipo){
	global $tipo_e_bonus;
	global $tipo_e_bonus_escudo;
	global $variante;
	global $magica;
	global $tipo_armadura;
	global $tipo_escudo;

	$armaduras = [];
	foreach ($equipamento_defensivo as $key => $value) {
		$nivel = mt_rand(1,21);
		if ($nivel < 5) {
			if ($tipo == 1) {
				$indice = mt_rand(0,1);
				$tipo = $tipo_escudo[mt_rand(0,2)];
				$tipo_bonus = $tipo_e_bonus_escudo[$tipo][$indice];
			} else {
				$indice = mt_rand(2,3);
				$tipo = $tipo_armadura[mt_rand(0,2)];
				$tipo_bonus = $tipo_e_bonus[$tipo][$indice];
			}

			$armaduras[] = [
				'poder'=>'Baixo - Nível 5 ou menor',
				'nome' => $value,
				'bonus' => $tipo_bonus,
				'rd' => $variante[0],
				'magica' => $magica[0],
				'tipo' => $tipo,
				'preco' => ($nivel+$indice)*1000
			];
		} elseif ($nivel >= 5 and $nivel < 10){
			if ($tipo == 1) {
				$indice = mt_rand(1,2);
				$tipo = $tipo_escudo[mt_rand(0,2)];
				$tipo_bonus = $tipo_e_bonus_escudo[$tipo][$indice];
			} else {
				$indice = mt_rand(3,4);
				$tipo = $tipo_armadura[mt_rand(0,2)];
				$tipo_bonus = $tipo_e_bonus[$tipo][$indice];
			}

			$indice_variante = mt_rand(0,1);
			$indice_magico = mt_rand(1,2);
			$armaduras[] = [
				'poder'=>'Médio - Entre nível 5 e 9',
				'nome' => $value,
				'bonus' => $tipo_bonus,
				'rd' => $variante[$indice_variante],
				'magica' => $magica[$indice_magico],
				'tipo' => $tipo,
				'preco'=> (($nivel+$indice+$indice_variante)*$indice_magico)*1000

			];
		} elseif ($nivel >= 10 and $nivel < 15){
			if ($tipo == 1) {
				$indice = mt_rand(3,4);
				$tipo = $tipo_escudo[mt_rand(0,2)];
				$tipo_bonus = $tipo_e_bonus_escudo[$tipo][$indice];
			} else {
				$indice = mt_rand(5,6);
				$tipo = $tipo_armadura[mt_rand(0,2)];
				$tipo_bonus = $tipo_e_bonus[$tipo][$indice];
			}

			$indice_variante = mt_rand(2,3);
			$indice_magico = mt_rand(2,3);
			$armaduras[] = [
				'poder'=>'Forte - Entre nível 10 e 14',
				'nome' => $value,
				'bonus' => $tipo_bonus,
				'rd' => $variante[$indice_variante],
				'magica' => $magica[$indice_magico],
				'tipo' => $tipo,
				'preco'=> (($nivel+$indice+$indice_variante)*$indice_magico)*1000
			];
		} elseif ($nivel >= 15){
			if ($tipo == 1) {
				$indice = 5;
				$tipo = $tipo_escudo[mt_rand(0,2)];
				$tipo_bonus = $tipo_e_bonus_escudo[$tipo][$indice];
			} else {
				$indice = mt_rand(7,9);
				$tipo = $tipo_armadura[mt_rand(0,2)];
				$tipo_bonus = $tipo_e_bonus[$tipo][$indice];
			}

			$indice_variante = mt_rand(3,4);
			$indice_magico = mt_rand(3,5);
			$armaduras[] = [
				'poder'=>'Poderoso - Nível 15 ou maior',
				'nome' => $value,
				'bonus' => $tipo_bonus,
				'rd' => $variante[$indice_variante],
				'magica' => $magica[$indice_magico],
				'tipo' => $tipo,
				'preco'=> (($nivel+$indice+$indice_variante)*$indice_magico)*1000
			];
		}
	}

	shuffle($armaduras);
	return $armaduras;
}

function ajustar_armas($armas_lista, $tipo){
	global $tipo_de_dano;
	global $variante;
	global $magica;

	$armas = [];
	foreach ($armas_lista as $key => $value) {
		$nivel = mt_rand(1,21);
		if ($nivel < 5) {
			if ($tipo == 1) {
				$indice = mt_rand(0,2);
				$tipo_dano = $tipo_de_dano[$indice];
			} else {
				$indice = mt_rand(2,4);
				$tipo_dano = $tipo_de_dano[$indice];
			}

			$armas[] = [
				'poder'=>'Baixo - Nível 5 ou menor',
				'nome' => $value,
				'dano' => $tipo_dano,
				'variante' => $variante[0],
				'magica' => $magica[0],
				'preco' => ($nivel+$indice)*1000
			];
		} elseif ($nivel >= 5 and $nivel < 10){
			if ($tipo == 1) {
				$indice = mt_rand(1,3);
				$tipo_dano = $tipo_de_dano[$indice];
			} else {
				$indice = mt_rand(3,5);
				$tipo_dano = $tipo_de_dano[$indice];
			}

			$indice_variante = mt_rand(0,1);
			$indice_magico = mt_rand(1,2);
			$armas[] = [
				'poder'=>'Médio - Entre nível 5 e 9',
				'nome' => $value,
				'dano' => $tipo_dano,
				'variante' => $variante[$indice_variante],
				'magica' => $magica[$indice_magico],
				'preco'=> (($nivel+$indice+$indice_variante)*$indice_magico)*1000

			];
		} elseif ($nivel >= 10 and $nivel < 15){
			if ($tipo == 1) {
				$indice = mt_rand(2,4);
				$tipo_dano = $tipo_de_dano[$indice];
			} else {
				$indice = mt_rand(4,6);
				$tipo_dano = $tipo_de_dano[$indice];
			}

			$indice_variante = mt_rand(2,3);
			$indice_magico = mt_rand(2,3);
			$armas[] = [
				'poder'=>'Forte - Entre nível 10 e 14',
				'nome' => $value,
				'dano' => $tipo_dano,
				'variante' => $variante[$indice_variante],
				'magica' => $magica[$indice_magico],
				'preco'=> (($nivel+$indice+$indice_variante)*$indice_magico)*1000
			];
		} elseif ($nivel >= 15){
			if ($tipo == 1) {
				$indice = mt_rand(3,4);
				$tipo_dano = $tipo_de_dano[$indice];
			} else {
				$indice = mt_rand(5,7);
				$tipo_dano = $tipo_de_dano[$indice];
			}

			$indice_variante = mt_rand(3,4);
			$indice_magico = mt_rand(3,5);
			$armas[] = [
				'poder'=>'Poderoso - Nível 15 ou maior',
				'nome' => $value,
				'dano' => $tipo_dano,
				'variante' => $variante[$indice_variante],
				'magica' => $magica[$indice_magico],
				'preco'=> (($nivel+$indice+$indice_variante)*$indice_magico)*1000
			];
		}
	}

	shuffle($armas);
	return $armas;
}


$armas_uma_mao = (array)$armas_uma_mao->nome;
$armas_duas_maos = (array)$armas_duas_maos->nome;
$armaduras = (array)$armaduras->nome;
$escudos = (array)$escudos->nome;

$armas_a_venda1 = ajustar_armas($armas_uma_mao, 1);
$armas_a_venda2 = ajustar_armas($armas_duas_maos, 0);
$escudos_a_venda = ajustar_bonus($escudos, 1);
$armaduras_a_venda = ajustar_bonus($armaduras, 0);
?>