<?php  

require_once 'define-urls.php';
session_start();

if (!isset($_SESSION['login']) && !isset($_SESSION['liberado'])){
	header("Location: ".URL_BASE_ORIGINAL."?status=pagina-restrita");
} else if (isset($_SESSION['usuarios_logados']) && $_SESSION['usuarios_logados'] < 0) {
	header("Location: ".URL_BASE_ORIGINAL."?status=pagina-restrita");
}